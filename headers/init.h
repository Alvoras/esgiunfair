#ifndef INIT_H_INCLUDED
#define INIT_H_INCLUDED

/**
 * Main init function
 * @param  isFullscreen 1 to create a fullscreen window, 0 otherwise
 * @return              Return 1 if successful
 */
int init(int isFullscreen);
/**
 * Init the _mainChar
 * @param level Level id
 */
 void initCharacter(int level, int saveId);
/**
 * Init the _boss
 * @param level Level id
 */
void initBoss(int level);
/**
 * Init all the scenery elements
 * @param level Level id
 */
void initScenery(int level);

void initMainMenu();
void initInGameMenu();
void initParamMenu();
void initLoadCharMenu();
void initGameModeMenu();
void initMultiplayerMenu();
void initLocalMultiModeMenu();
void initConfirmBox();
void initPlayOptionsBox();
void initKeymap(int saveId);
void initInputTextMenu(char ask[51]);
void initPlayerMenu();
void refreshDisplayStr(MenuStruct *menuArray, int arrayId, char *str);
void initDialogText(int level);
void initDialog(int level);
void initDialogSprite();
void initGameParams();
void initSoundMenu();

#endif // INIT_H_INCLUDED
