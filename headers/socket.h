#ifndef SOCKET_H
#define SOCKET_H

#ifdef WIN32

#include <winsock2.h>

#elif defined (linux)

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

#else

#error Not defined for this platform

#endif

#define CRLF      "\r\n"
#define PORT      2811
#define MAX_CLIENTS  10

#define BUF_SIZE  1024

typedef struct
{
        SOCKET socket;
        char name[BUF_SIZE];
}Client;

#endif // SERVER_H_INCLUDED
