#ifndef CHARACTER_H_INCLUDED
#define CHARACTER_H_INCLUDED

/**
 * Start the upward jump movement
 */
void startJump();
/**
 * Start the downward landing movement
 */
void endJump();
/**
 * Make the character move left
 */
void moveLeft();
/**
 * Make the character move right
 */
void moveRight();
/**
 * Stops the lateral movement of the character
 */
void endMoveXAxis();
/**
 * Makes the character move quickly in a straight line
 * @param Teleportation If teleportation upgrade has been acquired. Cause instant movement
 * @param Left or right
 */
void dash(int teleportation, int mode);
/**
 * Updates all the variables that define the current state of the char sprite
 */
void updateCharPhysics();
/**
 * Moves the char's sprite according to variables set in the handleEvents function
 */
void mainCharAction();

/**
 * Verif hitbox of sprite with bullet of boss
 * @param rectangle of bullet
 */
int isPlayerHit(SDL_Rect rect);

/**
 * Verif hitbox with coord of specialAttack
 */
int isPlayerHitSpecial(SpecialAttack *ability);

/**
 * Verif hitbox with coord of specialAttack
 * @param str = amount of damage apply to mainChar
 * @return 1 = dead | 0 = alive
 */
int damagePlayer(int strength);

#endif // CHARACTER_H_INCLUDED
