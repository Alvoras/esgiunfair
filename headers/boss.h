#ifndef BOSS_H_INCLUDED
#define BOSS_H_INCLUDED

/**
 * Called when we need to apply damage to the boss
 * @param shot         Shot tht gives us information like the amount of damage to apply
 * @param shotPosition Position of the shot to remove in the ShotList linked list
 */
void damageBoss(Shot *shot, int shotPosition);

/**
 * Called when we need to apply damage to the boss
 * @param strength => amount of damage to apply
 */
void damageBossSpecial(int strength);

/**
 * Temporary hitbox function
 * @param  rect SDL_Rect to check the position of
 * @return      1 if true, otherwise 0
 */
int isBossHit(SDL_Rect *rect);

/**
 * Temporary hitbox function
 * @param  check ability and calcul coord
 * @return      1 if true, otherwise 0
 */
int isBossHitSpecial(SpecialAttack *ability);

/**
 * Check if the boss is set as dead
 * @return 1 if true, otherwise 0
 */
int isBossDead();

/**
 * call every moment and throw special attack when he can
 * @param attack
 */
void bossAttack(SpecialAttack *attack);

/**
 * Updates all the variables that define the current state of the boss sprite
 */
void updateBossPhysics();
#endif // BOSS_H_INCLUDED
