#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#define SCREEN_WIDTH 850
#define SCREEN_HEIGHT 480
#define FLOOR_PADDING 50

#define ARCADE_SAVE 4

#define FPS 60
#define FRAME_DELAY 1000/FPS // ms

#define SERVER_WAIT_TIMEOUT 4000

#define LOCALHOST "127.0.0.1"

#define SHOT_PER_SECOND 8
#define SHOT_DELAY 1000/SHOT_PER_SECOND
#define SPECIAL_SHOT_DELAY 1000 // 1 sec cooldown
#define SHOT_ANIM_DELAY 120
#define SHOT_VEL 10
#define SHOT_STRENGTH 10

#define CHAR_DASH_DELAY 1000 // ms
#define CHAR_ANIM_DELAY 120 // ms
#define CHAR_SPECIAL_ANIM_DELAY 85 // ms
#define CHAR_CHARGES_DELAY 30000 // ms -- 30 seconds
#define CHAR_H 48
#define CHAR_W 48
#define JUMP_HEIGHT CHAR_H*3
#define MAX_CHARGES 5
#define CHAR_HEART_SPRITE_PATH "assets/hud/heart.png"
#define CHAR_MANA_SPRITE_PATH "assets/hud/mana.png"

#define BOSS_ANIM_DELAY 120 // ms
#define BOSS_H 120
#define BOSS_W 120

#define GROUND_SHADOW_SPRITE_PATH "assets/ground_shadow.png"

#define MUSIC_PATH "assets/sounds/music.ogg"
#define BAR_SLIDER_SPRITE "assets/menu/slider_bar.png"
#define BALL_SLIDER_SPRITE "assets/menu/slider_handle.png"
#define SLIDER_BALL_WIDTH 35
#define BASIC_FONT "assets/menu/basic_font.ttf"
#define FONT_WIDTH 26
#define TEXT_FONT_WIDTH 10
#define ANNOUCEMENT_FONT "assets/fonts/pixeliza.ttf"
#define DIALOG_BOX_SPRITE_PATH "assets/hud/dialog_box.png"
#define TEXT_FONT "assets/fonts/grand9kpixel.ttf"
#define TEXT_DELAY 3000
#define TEXT_MARGIN 35
#define TEXT_WINDOW_MARGIN 0
#define TEXT_WINDOW_WIDTH SCREEN_WIDTH-TEXT_WINDOW_MARGIN*2
#define TEXT_WINDOW_HEIGHT 150
#define TEXT_MAX_DISPLAY_LINES 2
#define TEXT_MAX_LETTERS_PER_LINE (TEXT_WINDOW_WIDTH-TEXT_MARGIN*2)/TEXT_FONT_WIDTH
#define TEXT_MAX_LETTERS_PER_DISPLAY TEXT_MAX_LETTERS_PER_LINE*TEXT_MAX_DISPLAY_LINES

#define LOGO "assets/logo3.png"
#define BACKGROUND_MENU "assets/menu/background.png"
#define BACKGROUND_GAME_MODE_MENU "assets/menu/background.png"
#define BACKGROUND_MULTIPLAYER_MENU "assets/menu/background_load_char.png"
#define BACKGROUND_LOAD_CHAR_MENU "assets/menu/background_load_char.png"
#define BACKGROUND_CONFIRM_BOX "assets/menu/background.png"
#define BACKGROUND_PLAY_OPTIONS_BOX "assets/menu/background.png"
#define BACKGROUND_INPUT_TEXT_MENU "assets/menu/background_load_char.png"

#define BLACKSCREEN_PATH "assets/blackscreen.png"
#define MAX_OPACITY 256

#define PARAM_FILE_PATH "config/params"

// Used to cast void* to MainCharacter* structure
#define MAINCHAR_CAST(var) ((MainCharacter*)var)
#define BOSS_CAST(var) ((Boss*)var)

enum ServerOpCodes {
        // Won't be used
        ZERO,
        //Since we're using atoi(), we must ban 0
        SRV_BOSS_JUMP,
        SRV_BOSS_LEFT,
        SRV_BOSS_RIGHT
};

enum KeyPressSurface {
        KEY_PRESS_SURFACE_DEFAULT,
        KEY_PRESS_SURFACE_UP,
        KEY_PRESS_SURFACE_DOWN,
        KEY_PRESS_SURFACE_LEFT,
        KEY_PRESS_SURFACE_RIGHT,
        KEY_PRESS_SURFACE_TOTAL
};

enum Directions {
        DIRECTION_UP,
        DIRECTION_DOWN,
        DIRECTION_LEFT,
        DIRECTION_RIGHT,
        DIRECTION_TOTAL
};

enum Mouse {
        MOUSE_LEFT,
        MOUSE_RIGHT,
        MOUSE_MIDDLE,
        MOUSE_X,
        MOUSE_Y,
        MOUSE_TOTAL
};

enum LifeBarMode {
        LIFEBAR_VERTICAL,
        LIFEBAR_HORIZONTAL
};

enum Dialog {
        DIALOG_MODE_TEXT,
        DIALOG_MODE_TEXT_TITLE
};

enum Textures {
        CHAR_SPRITE,
        BOSS_SPRITE,
        SHOT_SPRITE,
        SCENERY_SPRITE,
        BACKGROUND_SPRITE,
        LIFEBAR_HOLDER_SPRITE,
        LIFEBAR_BOSS_SPRITE,
        LIFEBAR_CHAR_SPRITE,
        GROUND_SHADOW,
        DIALOG_TEXT_TITLE,
        DIALOG_TEXT,
        DIALOG_BOX,
        CHAR_HEART,
        CHAR_MANA_ICON,
        BLACKSCREEN,
        TEXTURES_TOTAL
};

enum FADE {
        FADE_IN,
        FADE_OUT
};

enum BossStates {
        BOSS_IDLE,
        BOSS_HIT,
        BOSS_DEATH,
        BOSS_IDLE_BLANK,
        BOSS_HIT_BLANK,
        BOSS_STATES_TOTAL
};

enum CharStates {
        CHAR_IDLE,
        CHAR_JUMP,
        CHAR_RUN_RIGHT,
        CHAR_RUN_LEFT,
        CHAR_BOW,
        CHAR_SPECIAL,
        CHAR_FALL,
        CHAR_DASH_LEFT,
        CHAR_DASH_RIGHT,
        CHAR_IDLE_BLANK,
        CHAR_JUMP_BLANK,
        CHAR_RUN_RIGHT_BLANK,
        CHAR_RUN_LEFT_BLANK,
        CHAR_SHOOT,
        CHAR_STATES_TOTAL
};

enum ShotStates {
        FIRED,
        TRAVELLING,
        HIT,
        FIRED_BLANK,
        TRAVELLING_BLANK,
        HIT_BLANK,
        SHOT_STATES_TOTAL
};

enum LoadTargetId {
        LOAD_CHARACTER,
        LOAD_BOSS
};
enum ShotListId {
        CHAR_SHOT_LIST,
        BOSS_SHOT_LIST
};

enum Dash {
        DASH_LEFT,
        DASH_RIGHT
};

enum SaveFile {
        LOAD_PASSIVE,
        LOAD_SPECS,
        LOAD_LEVEL,
        LOAD_SAVE,
        LOAD_KEYMAP,
        LOAD_KEYMAP_CUSTOM,
        NEW_FILE
};

enum TypeAbility {
        SHOT,
        RAY,
        RAIN
};

enum EffectAbility {
        NONE,
        POISONING,
        WEAKENING,
        PARALIZING,
        SLOWING
};

enum Ability {
        SPECIALCHAR,
        BASICBOSS,
        BASICRAY,
        PARALIZINGSHOT,
        SLOWINGSHOT,
        WEAKENINGSHOT,
        POISONINGSHOT,
        RAININGSHOT
};

enum Sounds {
        SOUND_CHAR_HIT,
        SOUND_CHAR_SHOOT,
        SOUND_CHAR_JUMP,
        SOUND_CHAR_DASH,
        SOUND_CHAR_SPECIAL,
        SOUND_CHAR_DEATH,
        SOUND_BOSS_HIT,
        SOUND_BOSS_LASER,
        SOUND_BOSS_SHOOT,
        SOUND_BOSS_DEATH,
        SOUND_TOTAL
};

// THE ORDER MUST MATCH THE ONE IN THE PARAM FILE
enum Params {
        CONFIG_FULLSCREEN,
        CONFIG_MUSIC_PLAY,
        CONFIG_MUSIC_VOLUME,
        CONFIG_SOUND_EFFECTS_PLAY,
        CONFIG_SOUND_EFFECTS_VOLUME,
        CONFIG_MAIN_VOLUME_PLAY,
        CONFIG_MAIN_VOLUME,
        CONFIG_TOTAL
};

enum Music {
        SOUND,
        EFFECTS,
        MUSIC
};

typedef struct Speech {
        char *text;
        int hasTalked;
        int totalParts;
        int cnt;
        unsigned long timer;
} Speech;


typedef struct Keymap {
        int up;
        int down;
        int left;
        int right;
        int shoot;
        int special;
        int parry;
        int menu;
        int dashLeft;
        int dashRight;
} Keymap;

typedef struct {
        char *address;
        char *name;
} ClientSocketInfo;

typedef struct AnimationData {
        int frame;
        int state;
        int blank_state;
        unsigned long timer;
        int animationLock;
} AnimationData;

typedef struct ShotAnimation {
        int frame;
        int state;
        int blank_state;
        SDL_Rect *spriteRects[SHOT_STATES_TOTAL];
        int frameQty[SHOT_STATES_TOTAL];
        int spriteWidth[SHOT_STATES_TOTAL];
        int spriteHeight[SHOT_STATES_TOTAL];
        SDL_Texture *spriteSheets[SHOT_STATES_TOTAL];
        unsigned long timer;
} ShotAnimation;

typedef struct Shot {
        float vectorX;
        float vectorY;
        int vel;
        int strength;
        int state;
        int state_blank;
        int frame;
        SDL_Rect dstRect;
        SDL_Rect spriteRect;    //With mask, verified pixel for hitbox
        unsigned long timer;
        struct Shot *next;
} Shot;

typedef struct {
        Shot *first;
        unsigned long timer;
        ShotAnimation *animation;
} ShotList;

typedef struct Passive {
        int hasTeleportation;

} Passive;

typedef struct Specs {
        int intelligence;
        int strength;
        int agility;
        int vitality;
        int isParalized;
        int timerParalized;
        int isWeakened;
        int timerWeakened;
        int isSlowed;
        int timerSlowed;
        int isPoisoned;
        int timerPoisoned;
        int isInvulnerable;
        int timerInvulnerable;
        int frameInvulnerable;
        int timerFrameInvulnerable;
        int cooldownFrameInvulnerable;
} Specs;

typedef struct SpecialAttack {
        void (*specialAbility)(struct SpecialAttack*);
        SDL_Rect *spriteRectSrc;
        SDL_Rect spriteRectDst;
        SDL_Texture *sprite;
        int strength;
        int duration;
        int type;
        int effects;
        int frame;
        int stateFrame;
        int timerFrame;
        int frameCooldown;
        int cooldown;
        int timer;
        int isInversed;
        int isActivated;
        Shot *nextShot;
        struct SpecialAttack *next;
} SpecialAttack;

typedef struct WriteBuffer {
        int level;
        Specs specs;
        Passive passive;
} WriteBuffer;

typedef struct {
        Speech speech;
        float velX;
        float velY;
        float gravityX;
        float gravityY;
        int isJumping;
        int isFalling;
        int isDashing;
        long int dashTimer;
        int maxJumps;
        int currentJump;
        int initialJumpPos;
        int direction[DIRECTION_TOTAL];
        int hp;
        int strength;
        int charges;
        unsigned long chargesTimer;
        unsigned long specialCooldownTimer;
        int frame;
        int state;
        int state_blank;
        int animationLock;
        SDL_Rect *spriteRects[CHAR_STATES_TOTAL];
        int frameQty[CHAR_STATES_TOTAL];
        int spriteWidth[CHAR_STATES_TOTAL];
        int spriteHeight[CHAR_STATES_TOTAL];
        SDL_Texture *spriteSheets[CHAR_STATES_TOTAL];
        SDL_Rect dstRect;
        Passive passive;
        Specs specs;
        unsigned long timer;
        ShotList *shotList;
        SpecialAttack *specialAttack;
} MainCharacter;

typedef struct {
        Speech speech;
        float velX;
        float velY;
        float gravityX;
        float gravityY;
        int hp;
        int strength;
        int frame;
        int state;
        int state_blank;
        SDL_Rect *spriteRects[BOSS_STATES_TOTAL];
        int frameQty[BOSS_STATES_TOTAL];
        int spriteWidth[BOSS_STATES_TOTAL];
        int spriteHeight[CHAR_STATES_TOTAL];
        SDL_Texture *spriteSheets[BOSS_STATES_TOTAL];
        SDL_Rect dstRect;
        unsigned long timer;
        ShotList *shotList;
        SpecialAttack *specialAttack;
} Boss;

typedef struct {
        SDL_Texture *sprite;
        SDL_Rect srcRect;
        SDL_Rect dstRect;
        int w;
        int h;
} LifeBar;

typedef struct Platforms {
        int nbPlatforms;
        SDL_Texture *texture;
        SDL_Rect srcRect;
        SDL_Rect dstRect;
} Platforms;

typedef struct {
        int up;
        int down;
        int right;
        int left;
        int sum;
} CalculCoord;

#endif // CONFIG_H_INCLUDED
