#ifndef LOAD_H_INCLUDED
#define LOAD_H_INCLUDED

/**
 * Create a SDL_Texture from a path
 * @param  path Path to load the texture from
 * @return      SDL_Texture loaded
 */
SDL_Texture *loadTexture(char *path);
/**
 * Load the sprites for the _mainChar structure
 * @param configPath Path to the config file
 */
void loadCharSpriteSheet(char *configPath);
/**
 * Load all the acquired power upade
 * @param level Level concerned
 */
void loadCharPassive(int level);
/**
 * Load the sprites for the _boss structure
 * @param configPath Path to the config file
 */
void loadBossSpriteSheet(char *configPath);
/**
 * Load the sprites for the animation structure
 * @param configPath Path to the config file
 */
void loadAttackSpriteSheet(char *configPath, int loadTargetId);
/**
 * Load a spritesheet depending of the target id
 * @param loadTargetId The target can be boss or main char
 * @param level        Level id
 */
void loadSpriteSheet(int loadTargetId, int level);
/**
 * Load the background texture into the _textures array
 */
void renderBackground();
/**
 * Load the life bar scenery elements
 */
void loadLifeBar(LifeBar lifeBar, LifeBar lifeBarHolder, float hp, int mode);
/**
 * Load the scenery's platforms
 */
void loadPlatforms();
/**
 * Load the designated level
 * @param level Level id to be loaded
 */
void loadLevel(int level, int saveId);
/**
 * Load all the file used to configure the game to be launched
 * @param saveId Id of the save file to be loaded (1,2,3)
 */
void loadGame(int saveId, ...);
/**
 * Convert the given SDL keycode into the corresponding char
 * @param  keycode Char to convert
 * @return   Char
 */
char convertFomSDLKeyCode(int keycode);
/**
 * Convert the given string into the corresponding SDL keycode
 * @param  c String to convert
 * @return   SDL keycode
 */
int convertToSDLKeyCode(char *c);
/**
 * Loads the keymap file into the program
 */
void loadKeymap(Keymap  *keymap, int saveId);
void loadCharSpecs(int saveId);
char *loadPlayerName(int saveId);
void loadDialogBox();
void loadMusic();
void loadSoundEffects(int flag, int level);
void loadGameParams();
void loadBlackScreen();

#endif // LOAD_H_INCLUDED
