#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

enum InGameMenu {
        IG_BACKGROUND,
        IG_LOGO,
        IG_CONTINUE,
        IG_BACK_TO_MENU,
        IG_PARAM,
        IG_LEAVE,
        IG_OPENING_CHEVRON,
        IG_CLOSING_CHEVRON,
        IG_TOTAL_MENU
};

enum MainMenu {
        MAIN_BACKGROUND,
        MAIN_LOGO,
        MAIN_PLAY,
        MAIN_PARAM,
        MAIN_LEAVE,
        MAIN_OPENING_CHEVRON,
        MAIN_CLOSING_CHEVRON,
        MAIN_TOTAL_MENU
};

enum LoadCharMenu {
        LOAD_CHAR_BACKGROUND,
        LOAD_CHAR_SLOT_1,
        LOAD_CHAR_SLOT_2,
        LOAD_CHAR_SLOT_3,
        LOAD_CHAR_LEAVE,
        LOAD_CHAR_CHEVRON,
        LOAD_CHAR_TOTAL
};

enum GameModeMenu {
        GAME_MODE_BACKGROUND,
        GAME_MODE_AVENTURE,
        GAME_MODE_MULTIPLAYER,
        GAME_MODE_ARCADE,
        GAME_MODE_LEAVE,
        GAME_MODE_OPENING_CHEVRON,
        GAME_MODE_CLOSING_CHEVRON,
        GAME_MODE_TOTAL
};

enum MultiplayerMenu {
        MULTI_BACKGROUND,
        MULTI_LOCAL,
        MULTI_INTERNET,
        MULTI_LEAVE,
        MULTI_OPENING_CHEVRON,
        MULTI_CLOSING_CHEVRON,
        MULTI_TOTAL
};

enum localMultiModeMenu {
        LOCAL_MULTI_BACKGROUND,
        LOCAL_MULTI_HOST,
        LOCAL_MULTI_CLIENT,
        LOCAL_MULTI_LEAVE,
        LOCAL_MULTI_OPENING_CHEVRON,
        LOCAL_MULTI_CLOSING_CHEVRON,
        LOCAL_MULTI_TOTAL
};

enum ParamMenu {
        PARAM_BACKGROUND,
        PARAM_LOGO,
        PARAM_FULLSCREEN,
        PARAM_SOUND,
        PARAM_LEAVE,
        PARAM_FULLSCREEN_PICTURE,
        PARAM_OPENING_CHEVRON,
        PARAM_CLOSING_CHEVRON,
        PARAM_TOTAL_MENU
};

enum SoundMenu {
        SOUND_MENU_BACKGROUND,
        SOUND_MENU_MUSIC_PLAY,
        SOUND_MENU_SOUND_EFFECTS_PLAY,
        SOUND_MENU_MAIN_VOLUME_PLAY,
        SOUND_MENU_LEAVE,
        SOUND_MENU_MUSIC_SLIDER_BAR,
        SOUND_MENU_MUSIC_SLIDER_BALL,
        SOUND_MENU_EFFECTS_SLIDER_BAR,
        SOUND_MENU_EFFECTS_SLIDER_BALL,
        SOUND_MENU_MAIN_VOLUME_SLIDER_BAR,
        SOUND_MENU_MAIN_VOLUME_SLIDER_BALL,
        SOUND_MENU_MUSIC_VOLUME,
        SOUND_MENU_SOUND_EFFECTS_VOLUME,
        SOUND_MENU_MUSIC_MAIN_VOLUME,
        SOUND_MENU_MUSIC_PLAY_PICTURE,
        SOUND_MENU_SOUND_EFFECTS_PLAY_PICTURE,
        SOUND_MENU_MAIN_VOLUME_PLAY_PICTURE,
        SOUND_MENU_MUSIC_VOLUME_VALUE,
        SOUND_MENU_EFFECTS_VOLUME_VALUE,
        SOUND_MENU_MAIN_VOLUME_VALUE,
        SOUND_MENU_CHEVRON,
        SOUND_MENU_TOTAL
};

enum ConfirmBox {
        CONFIRM_BACKGROUND,
        CONFIRM_ASK,
        CONFIRM_YES,
        CONFIRM_NO,
        CONFIRM_CHEVRON,
        CONFIRM_TOTAL
};

enum PlayOptions {
        PLAY_BACKGROUND,
        PLAY_CONTINUE,
        PLAY_RESET,
        PLAY_DELETE,
        PLAY_LEAVE,
        PLAY_CHEVRON,
        PLAY_TOTAL
};

enum InputTextMenu {
        INPUT_BACKGROUND,
        INPUT_ASK,
        INPUT_TEXT,
        INPUT_LEAVE,
        INPUT_VALIDATE,
        INPUT_CHEVRON,
        INPUT_TEXT_NAME_MODE,
        INPUT_TEXT_IP_MODE,
        INPUT_TEXT_SELECT_LEVEL_MODE,
        INPUT_TOTAL
};

enum PlayerDeadMenu {
        PLAYER_BACKGROUND,
        PLAYER_LOGO,
        PLAYER_RETRY,
        PLAYER_BACK_TO_MENU,
        PLAYER_LEAVE,
        PLAYER_OPENING_CHEVRON,
        PLAYER_CLOSING_CHEVRON,
        PLAYER_TOTAL_MENU
};

enum Loops {
        MAIN_MENU_LOOP,
        GAME_MODE_LOOP,
        LOAD_CHAR_LOOP,
        MULTIPLAYER_LOOP,
        LOCAL_MULTI_LOOP,
        INGAME_MENU_LOOP,
        PARAM_MENU_LOOP,
        CONFIRM_LOOP,
        PLAY_OPTIONS_LOOP,
        INPUT_TEXT_LOOP,
        GAME_LOOP,
        PROGRAM_LOOP,
        DIALOG_LOOP,
        PLAYER_DEAD_MENU_LOOP,
        BOSS_DEAD_MENU_LOOP,
        SOUND_MENU_LOOP,
        LOOP_TOTAL
};

typedef struct MenuStruct {
        SDL_Texture *texture;
        SDL_Rect size;
        int isVisible;
} MenuStruct;

void mainMenu();
void changeMainPosCursor(int posCursor);

void inGameMenu();
void changeInGamePosCursor(int posCursor);

void paramMenu();
void changeParamPosCursor(int posCursor);

void loadCharMenu();
void changeLoadCharPosCursor(int posCursor);

void gameModeMenu();
void changeGameModePosCursor(int posCursor);

void multiplayerMenu();
void changeMultiplayerPosCursor(int posCursor);

void localMultiModeMenu();
void changeLocalMultiModeCursor(int posCursor);

int confirmBox();
void changeConfirmBoxPosCursor(int posCursor);

void playOptionsBox(int currentSaveId);
void changePlayOptionsBoxCursor(int posCursor);

char *inputTextMenu(int mode);
void changeInputTextMenuCursor(int posCursor);

void playerDeadMenu();
void changePlayerPosCursor(int posCursor);

void addMenuOption(char *message, int x, int y, int w, int h, SDL_Color color, MenuStruct *menu, int param);
void renderMenu(MenuStruct menu[], int totalElements);
void addMenuTexture(int x, int y, int w, int h, MenuStruct *menu, char *path, int flag);
void soundMenu();
void changeSoundMenuCursor(int posCursor);
void renderBlackscreenMenu(MenuStruct menu[], int totalElements);
int isHovering(MenuStruct item, int x, int y);

//void printText(char *message, int x, int y, int w, int h, SDL_Color color, int param);

#endif // MENU_H_INCLUDED
