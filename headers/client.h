#ifndef CLIENT_H
#define CLIENT_H

/**
 * Handle the WSAStartup function needed if we're running on Windows
 */
 void initClient();
 /**
  * Initialise the ClientSocketInfo strcture
  * @param clientSocketInfo [description]
  */
  void initSocketInfo(char *ip);
 /**
  * Generate the socket and start a connection with the server
  * @param  address The IP address to connect to
  * @return         Returns socket fd
  */
 int initClientConnection(char *address);
 /**
  * Thread function that handle the data to send and to receive from the server
  * @param  threadArgs Pinter to arguments structure sent by pthread_create
  * @return            Returns NULL to make the compiler happy
  */
 void *clientLoop(void *threadArgs);
 /**
  * Closes the socket
  * @param sock Socket's fd to close
  */
  void endClientConnection(SOCKET socket);
 /**
  * Read the data sent by the server and give it to the clientSocket function to handle
  * @param  sock   Socket fd to listen to
  * @param  buffer 1024 bytes buffer to write data to
  * @return        The number if bytes received
  */
 int readServer(SOCKET sock, char *buffer);
 /**
  * Send the given buffer to the given socket
  * @param sock   Socket to send the buffer to
  * @param buffer Buffer to send
  */
 void writeServer(SOCKET sock, char *buffer);

/**
 * Main client function
 */
 void *clientGameSocket(void *threadArgs);
 void clientCleanUp();

 #endif // CLIENT_H_INCLUDED
