#ifndef LINKED_LIST_INCLUDED
#define LINKED_LIST_INCLUDED

/**
 * Initialise a ShotList in order to store Shot elements
 * @return Returns the ShotList structure generated
 */
ShotList *initShotList();
/**
 * Generate a Shot element ready to be added to a Shot linked list
 * @param _shot      Shot element to be initialised
 * @param shotListId Which ShotList the Shot will be added to
 */
void initNewShot(Shot *shot, int shotListId);
/**
 * Check if the character is ready to generate a new Shot and if true, initalise a new Shot and add it to the given linked list
 * @param shotListId Which ShotList the Shot will be added to
 */
void addShot(int shotListId);
/**
 * Remove a Shot element from the given Shot linked list
 * @param pos        Position of the element in the list to be removed
 * @param shotListId Which ShotList the Shot will be added to
 */
void removeShot(int pos, int shotListId);

int comparePixel(int x, int y, int r, int g, int b);

#endif // LINKED_LIST_INCLUDED
