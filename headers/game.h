#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED


void handleEvents(SDL_Event e);
/**
 * Handle the events sent by the server
 */
void handleServerEvents();

int updateProjectilePhysics();
/**
 * Main update function that calls all the update sub-function
 */
int update();
int isDialogDone();
/**
 * Watch if the given SDL_Rect is outside the game's window
 * @param  rect SDL_Rect to monitor
 * @return      Return if the rect is outside, 0 otherwise
 */
int isOutside(SDL_Rect *rect);
/**
 * Handle the animation for the given Shot element
 * @param shot Shot element to be animated
 */
void animateShot(Shot *shot);
/**
 * As needed for every animation : reset timer, increase current frame
 * @param timer Timer to be reseted
 * @param frame Frame to be increased
 * @param delay Delay used to look if enough time has passed to allow animation to update
 */
void updateAnimation(unsigned long *timer, int *frame, int delay);
/**
 * Update the position of the characters own shadows
 */
void updateShadow();
/**
 * Save the current state of the character
 * @param levelId Current level
 * @param saveId  Save slot
 * @param specs   Char's characteristics
 * @param passive Char's passive
 */
 int save(int levelId, int saveId, Specs specs, Passive passive);
/**
 * Reset the given slot's save file
 * @param saveId Slot to reset
 */
 int resetSave(int saveId);

 void dialog();
 void handleEventsLoop();
 char *serializeEvent(SDL_Event e);

 /**
 * Compare 2 structs CalculCoord
 * @param struct point A
 * @param struct Point B
 * @return 1 = out of screen, 0 = inside
 */
int calculCoordBin(CalculCoord codeA, CalculCoord codeB);

/**
 * Coordinate calculation x, y on rectangle xMin yMin xMax yMax
 * @param coord x y of 1 point
 * @param coord xMin && yMin
 * @param coord xMax && yMax
 * @return Struct CalculCoord with the point too high or too low
 */
CalculCoord calculCoord(int x, int y, int xMinScreen, int yMinScreen, int xMaxScreen, int yMaxScreen);
#endif // GAME_H_INCLUDED
