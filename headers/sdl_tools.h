#ifndef SDL_TOOLS_H_INCLUDED
#define SDL_TOOLS_H_INCLUDED
/**
 * Tell how many lines there is in a file
 * @param  fp Pointer to the file to eval
 * @return    Line quantity
 */
unsigned int getLineQty(FILE *fp);
/**
 * Generate the path where the asset is located with the given arguments
 * @param  item      char, boss
 * @param  assetType Is used when we're loading something else than root item sprites (e.g attacks sprites, specs)
 * @param  asset     sprites, specs...
 * @param  level     The level to load
 * @return           The generated path is returned as a string
 */
char *createLevelStr(char *item, char *assetType, char *asset, int level);
/**
 * Generate the path where the given save's confg file is located
 * @param  toLoad Which file to load
 * @param  saveId Save to load (1,2,3)
 * @return        Returns the generated path
 */
char *createSaveStr(int toLoad, int saveId);
/**
 * Close the main SDL library and the extensions
 */
SDL_Texture *createDialogText(char *str, SDL_Color color);
void renderDialog(int x, int y, int w, int h, int isCentered);
void closeSDL();
/**
 * Apply the given sprite to the window
 * @param charTexture The texture to render
 * @param charSrcRect The SDL_Rect that gives us which part of the image we are loading
 * @param charDstRect The SDL_Rect on which we apply the texture to
 */
void renderSprite(SDL_Texture *charTexture, SDL_Rect *charSrcRect, SDL_Rect *charDstRect);
/**
 * Render all the ShotList
 */
void renderProjectiles();
/**
 * Main render function that calls all then rendering sub-function
 */
void render();
void renderVictory();
void renderDefeat();
void renderCenterText(char *str, TTF_Font *font, int fontWidth);
int renderWaitingScreen();
void renderBossHud();
void playSound(int flag);
void playMusic();
int comparePixel(int x, int y, int r, int g, int b);
void freePointer();
void stopSound(int channel);
void setParam(int flag, int value);
void applySoundParams();
void toggleSound(int flag);
void renderBlackscreen();
void fadeBlackscreen(int mode);
void doFadeBlackscreen(int mode);
void gameBlackScreen(int mode);
void applyBlackscreenAlpha();
void toggleBlackscreenFade();
void setBlackscreenTimer(int sec, long millisec);

#endif // SDL_TOOLS_H_INCLUDED
