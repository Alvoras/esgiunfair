#ifndef ABILITY_H_INCLUDED
#define ABILITY_H_INCLUDED

SpecialAttack *loadSpecialAttack(int level, int LoadTargetId);

SpecialAttack* initSpecialAttack(int special, int cooldown, int duration, int maxFrame, int frameCooldown, int strength, int type, int effect);
SpecialAttack *initAnimationAttack(SpecialAttack *attack, char* pathTexture, int xSprite, int ySprite, int widthSprite, int heightSprite, int mulX, int mulY);
void* chooseSpecialAbility(int ability);

void charSpecial(SpecialAttack *attack);
void electricalBullet(SpecialAttack *attack);
void basicalRay(SpecialAttack *attack);
void paralizingShot(SpecialAttack *attack);
void poisoningShot(SpecialAttack *attack);
void weakeningShot(SpecialAttack *attack);
void slowingShot(SpecialAttack *attack);
void rainingShot(SpecialAttack *attack);

Shot *calculVector(Shot *shot, SDL_Rect shotRect);

void removeShotSpecial(SpecialAttack *attack, int pos);
void updateSpecialAbility(SpecialAttack *attack);
void renderAbility();
void renderShotList(SpecialAttack *attack);

#endif // ABILITY_H_INCLUDED
