#ifndef SERVER_H
#define SERVER_H

void *gameServer();
void initServer(void);
void serverCleanUp(void);
void serverLoop(void);
int initServerConnection(void);
void endServerConnection(SOCKET sock);
int readClient(SOCKET sock, char *buffer);
void clearClients(Client *clients, int crtSocketQty);
void writeClient(SOCKET sock, char *buffer);
void broadcastMessage(Client *clients, Client sender, int crtSocketQty, char *buffer);
void removeClient(Client *clients, int to_remove, int *crtSocketQty);

#endif // SERVER_H_INCLUDED
