#ifndef UTILITY_H_INCLUDED
#define UTILITY_H_INCLUDED

enum LoggerMode{
  LOGGER_INFO,
  LOGGER_ERR,
  LOGGER_OK,
  LOGGER_FAIL,
  LOGGER_SRV,
  LOGGER_CLT
};

enum EnvOptions{
  ENV_DEV,
  ENV_DEBUG,
  ENV_PROD
};

enum PushType{
  PUSH_INT,
  PUSH_LONG,
  PUSH_FLOAT,
  PUSH_DOUBLE,
  PUSH_CHAR,
};

enum Sleep{
  SLEEP_MICRO,
  SLEEP_SECOND
};

/**
 * Prints the given string with LOG - to the console
 * @param str String to print
 */
 void logger(char *str, int mode, ...);
/**
 * Prints >>debug<< in the console ; used to find the line that's crashing the program
 */
void dbg();
/**
 * Handle the events spawned from mouse and keyboard
 * @param quit This function can set quit = 1 to end the main game loop
 */

unsigned long filelen(FILE *fp);
void *smalloc(int size, int qty);
void *srealloc(void *ptr, int size);
FILE *sfopen(char* path, char* mode);
int exists(char *path);
void crossUnlink(char *path);
void *push(void *array, void *element, int mode);
void popStr(char *str, int pos, int flush);
void appendStr(char *path, char *str);
float getPercentFromVal(float pos, float maxVal);
float getValFromPercent(float percent, float maxVal);
char **splitStr(char *str, char *delimiter, ...);
char *joinStr(char **str, char *delimiter);
char *keyValJoin(char **array);
void signalHandler(int signo);
void crossSleep(int sec, unsigned long millisec);

#endif // UTILITY_H_INCLUDED
