#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../headers/config.h"
#include "../headers/utility.h"
#include "../headers/sdl_tools.h"
#include "../headers/linked_list.h"
#include "../headers/game.h"

extern SDL_Texture *_textures[TEXTURES_TOTAL];

extern int _mouse[MOUSE_TOTAL];

extern MainCharacter _mainChar;
extern Boss _boss;

ShotList *initShotList() {
        ShotList *list;
        Shot *    shot;

        list = malloc(sizeof(ShotList));
        shot = malloc(sizeof(Shot));

        if (list == NULL || shot == NULL) {
                printf("ShotList malloc failed");
                exit(EXIT_FAILURE);
        }

        shot->next = NULL;

        list->first     = shot;
        list->animation = malloc(sizeof(ShotAnimation));

        return(list);
}

void initNewShot(Shot *shot, int shotListId) {
        SDL_Rect *shotRect = NULL;
        void *structBuffer = NULL;
        float norm;

        shotRect = smalloc(sizeof(SDL_Rect), 1);

        switch (shotListId) {
        case CHAR_SHOT_LIST:
                structBuffer = &_mainChar;
                break;

        case BOSS_SHOT_LIST:
                structBuffer = &_boss;
                break;
        }

        shotRect->x     = MAINCHAR_CAST(structBuffer)->dstRect.x + CHAR_W / 2;
        shotRect->y     = MAINCHAR_CAST(structBuffer)->dstRect.y + CHAR_H / 3;
        shotRect->w     = MAINCHAR_CAST(structBuffer)->shotList->animation->spriteWidth[FIRED];
        shotRect->h     = MAINCHAR_CAST(structBuffer)->shotList->animation->spriteHeight[FIRED];

        if((_mouse[MOUSE_X] - shotRect->x) > 0 && (_mouse[MOUSE_Y] - shotRect->y) < 0)
        {
                // Higher Y && Higher X
                norm = sqrt(pow((_mouse[MOUSE_X] - shotRect->x),2) + pow((shotRect->y - _mouse[MOUSE_Y]),2));
                shot->vectorX = (_mouse[MOUSE_X] - shotRect->x)/norm;
                shot->vectorY = -(shotRect->y - _mouse[MOUSE_Y])/norm;
        }
        else if((_mouse[MOUSE_X] - shotRect->x) < 0 && (_mouse[MOUSE_Y] - shotRect->y) < 0)
        {
                // Higher Y && Lower X
                norm = sqrt( pow((shotRect->x - _mouse[MOUSE_X]),2) + pow((shotRect->y - _mouse[MOUSE_Y]),2));
                shot->vectorX = -(shotRect->x - _mouse[MOUSE_X])/norm;
                shot->vectorY = -(shotRect->y - _mouse[MOUSE_Y])/norm;
        }
        else if((_mouse[MOUSE_X] - shotRect->x) < 0 && (_mouse[MOUSE_Y] - shotRect->y) > 0)
        {
                // Lower Y && Lower X
                norm = sqrt(pow((_mouse[MOUSE_X] - shotRect->x),2) + pow((_mouse[MOUSE_Y] - shotRect->y),2));
                shot->vectorX = (_mouse[MOUSE_X] - shotRect->x)/norm;
                shot->vectorY = (_mouse[MOUSE_Y] - shotRect->y)/norm;
        }
        else
        {
                // Lower Y && Higher X
                norm = sqrt(pow((shotRect->x - _mouse[MOUSE_X]),2) + pow((_mouse[MOUSE_Y] - shotRect->y),2));
                shot->vectorX = -(shotRect->x - _mouse[MOUSE_X])/norm;
                shot->vectorY = (_mouse[MOUSE_Y] - shotRect->y)/norm;
        }

        shot->vel      = SHOT_VEL;
        shot->strength = SHOT_STRENGTH;
        shot->state   = FIRED;
        shot->state_blank = FIRED_BLANK;
        shot->frame   = 0;
        shot->dstRect = *shotRect;
        shot->timer   = SDL_GetTicks();
        shot->next    = NULL;
}

void addShot(int shotListId) {
        Shot *shot = smalloc(sizeof(Shot), 1);
        void *structBuffer = NULL;

        switch (shotListId) {
        case CHAR_SHOT_LIST:
                structBuffer = &_mainChar;
                break;

        case BOSS_SHOT_LIST:
                structBuffer = &_boss;
                break;
        }

        // Cooldown
        if (MAINCHAR_CAST(structBuffer)->shotList->timer != 0 && (SDL_GetTicks() - MAINCHAR_CAST(structBuffer)->shotList->timer) < SHOT_DELAY) {
                return;
        }

        MAINCHAR_CAST(structBuffer)->shotList->timer = SDL_GetTicks();

        if (MAINCHAR_CAST(structBuffer)->shotList == NULL) {
                printf("Shot malloc failed or MAINCHAR_CAST(structBuffer)->shotList is null");
                exit(EXIT_FAILURE);
        }

        initNewShot(shot, shotListId);

        shot->next = MAINCHAR_CAST(structBuffer)->shotList->first;
        MAINCHAR_CAST(structBuffer)->shotList->first = shot;

}

void removeShot(int pos, int shotListId) {
        Shot *crtBuffer = NULL;
        Shot *previousBuffer = NULL;
        void *structBuffer = NULL;
        int i = 0;

        switch (shotListId) {
        case CHAR_SHOT_LIST:
                structBuffer = &_mainChar;

                break;
        case BOSS_SHOT_LIST:
                structBuffer = &_boss;
                break;
        }


        crtBuffer      = MAINCHAR_CAST(structBuffer)->shotList->first;

        if (MAINCHAR_CAST(structBuffer)->shotList->first == NULL) {
                return;
        }

        // If pos is 0, we don't need to iterate through the list
        if (pos == 0) {
                MAINCHAR_CAST(structBuffer)->shotList->first = crtBuffer->next;

                if(crtBuffer != NULL) {
                        free(crtBuffer);
                }
                return;
        }

        // Iterate trough the list to get ourselves to the right position
        for (i = 0; crtBuffer != NULL && i < pos; i++) {
                previousBuffer = crtBuffer;
                crtBuffer      = crtBuffer->next;
        }

        if (crtBuffer == NULL) {
                return;
        }

        previousBuffer->next = crtBuffer->next;

        if(crtBuffer != NULL) {
                free(crtBuffer);
        }
}
