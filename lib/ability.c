#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../headers/config.h"
#include "../headers/load.h"
#include "../headers/utility.h"
#include "../headers/load.h"
#include "../headers/menu.h"
#include "../headers/init.h"
#include "../headers/game.h"
#include "../headers/linked_list.h"
#include "../headers/sdl_tools.h"
#include "../headers/game.h"
#include "../headers/character.h"
#include "../headers/boss.h"
#include "../headers/ability.h"

extern int _mouse[MOUSE_TOTAL];

extern Boss          _boss;
extern LifeBar       _lifeBar;
extern SDL_Renderer *_renderer;
extern MainCharacter _mainChar;

SpecialAttack *loadSpecialAttack(int level, int LoadTargetId)
{
    int i = 0;
    char *characterSpritesPath = "character/";
    char *bossSpritesPath = "boss/";
    char *special = "special";
    char *attacksAsset    = "attacks/";
    FILE *config = NULL;

    SpecialAttack *firstAbility = NULL;
    SpecialAttack *attack = NULL;

    //Int for attack Specs
    int ability, cooldown, duration, maxFrame, frameCooldown, strength, type, effect;

    //Use for texture of attack
    char *pathSprite = malloc(sizeof(char) * 150);
    int x, y, w, h, mulX, mulY;

    char *path = NULL;
    path = smalloc(sizeof(char), 150);

    switch(LoadTargetId)
    {
        case LOAD_BOSS:
            path = createLevelStr(bossSpritesPath, attacksAsset, special, level);
            break;

        case LOAD_CHARACTER:
            path = createLevelStr(characterSpritesPath, attacksAsset, special, level);
            break;
    }

    config = sfopen(path, "rb");
    while(fscanf(config, "%d %d %d %d %d %d %d %d %s %d %d %d %d %d %d", &ability, &cooldown, &duration, &maxFrame, &frameCooldown, &strength, &type, &effect, pathSprite, &x, &y, &w, &h, &mulX, &mulY) != EOF)
    {
        i++;
        attack = initSpecialAttack(ability, cooldown, duration, maxFrame, frameCooldown, strength, type, effect);
        attack = initAnimationAttack(attack, pathSprite,x, y, w, h, mulX, mulY);
        attack->next = firstAbility;
        firstAbility = attack;
    }

    free(path);
    free(pathSprite);
    fclose(config);
    return attack;
}

//================================= INIT ABILITY=============================================
SpecialAttack* initSpecialAttack(int special, int cooldown, int duration, int maxFrame, int frameCooldown, int strength, int type, int effect)
{
    SpecialAttack *attack = smalloc(sizeof(SpecialAttack), 1);
    attack->specialAbility = chooseSpecialAbility(special);

    attack->cooldown = cooldown;
    attack->duration = duration;
    attack->stateFrame = 0;
    attack->frame = maxFrame;
    attack->timerFrame = 0;
    attack->frameCooldown = frameCooldown;

    attack->timer = 0;  //timer compare with cooldown and duration
    attack->isInversed = 0; //If texture inversed
    attack->strength = strength;    //damage deal by attack
    attack->type = type;     //Type of attack
    attack->effects= effect;
    attack->nextShot = NULL;    //Use if type == SHOT or RAIN

    attack->next = NULL;

    return attack;
}

SpecialAttack *initAnimationAttack(SpecialAttack *attack, char* pathTexture, int xSprite, int ySprite, int widthSprite, int heightSprite, int mulX, int mulY)
{
    int i =0;

    attack->sprite = loadTexture(pathTexture);
    attack->spriteRectSrc = smalloc(sizeof(SDL_Rect), attack->frame);   //Attack->frame init W/ initSpecialAttack

    for(i = 0; i < attack->frame; i++)
    {
        attack->spriteRectSrc[i].x = xSprite + (i * mulX);
        attack->spriteRectSrc[i].y = ySprite + (i * mulY);
        attack->spriteRectSrc[i].h = heightSprite;
        attack->spriteRectSrc[i].w = widthSprite;
    }

    return attack;
}

void* chooseSpecialAbility(int ability)
{
    switch(ability)
    {
        case SPECIALCHAR:
            return charSpecial;
        case BASICBOSS:
            return electricalBullet;
        case BASICRAY:
            return basicalRay;
        case PARALIZINGSHOT:
           return paralizingShot;
        case SLOWINGSHOT:
            return slowingShot;
        case WEAKENINGSHOT:
            return weakeningShot;
        case POISONINGSHOT:
            return poisoningShot;
        case RAININGSHOT:
            return rainingShot;
        default:
            printf("Ability not found\n");
            return NULL;
            break;
    }
}
//================================= END INIT ABILITY==========================================

//================================= START ABILITY=============================================
void charSpecial(SpecialAttack *attack)
{
    _mainChar.animationLock = 1;
    _mainChar.state = CHAR_SPECIAL;
    _mainChar.charges--;
    _mainChar.frame = 0;

    _mainChar.specialAttack->spriteRectDst.y = _mainChar.dstRect.y;
    _mainChar.specialAttack->spriteRectDst.h = 50;

    if(_mouse[MOUSE_X] < _mainChar.dstRect.x)
    {
        printf("Ok");
        _mainChar.specialAttack->spriteRectDst.x = -10;
        _mainChar.specialAttack->spriteRectDst.w = 0 + _mainChar.dstRect.x;
        _mainChar.specialAttack->isInversed = 1;
    }
    else
    {
        _mainChar.specialAttack->spriteRectDst.x = _mainChar.dstRect.x + _mainChar.dstRect.w;
        _mainChar.specialAttack->spriteRectDst.w = SCREEN_WIDTH - _mainChar.dstRect.x;
    }
}

void electricalBullet(SpecialAttack *attack)
{
    Shot *shot = smalloc(sizeof(Shot), 1);
    SDL_Rect shotRect;

    shotRect.x = (_boss.dstRect.x + CHAR_W / 2);
    shotRect.y = (_boss.dstRect.y + CHAR_H / 3);
    shotRect.w = 50;
    shotRect.h = 50;

    shot = calculVector(shot, shotRect);

    shot->dstRect = shotRect;
    shot->vel = 10;
    shot->strength = 20;

    shot->next = attack->nextShot;
    attack->nextShot = shot;
}

void basicalRay(SpecialAttack *attack)
{
    attack->spriteRectDst.y = _mainChar.dstRect.y;
    attack->spriteRectDst.h = 50;

    if(_boss.dstRect.x >= _mainChar.dstRect.x)
    {
        attack->spriteRectDst.x = -10;
        attack->spriteRectDst.w = 0 + _boss.dstRect.x;
        attack->isInversed = 1;
    }
    else
    {
        attack->spriteRectDst.x = _boss.dstRect.x + _boss.dstRect.w;
        attack->spriteRectDst.w = SCREEN_WIDTH - _boss.dstRect.x;
    }
}

void paralizingShot(SpecialAttack *attack)
{
    Shot *shot = smalloc(sizeof(Shot), 1);
    SDL_Rect shotRect;

    shotRect.x = (_boss.dstRect.x + CHAR_W / 2);
    shotRect.y = (_boss.dstRect.y + CHAR_H / 3);
    shotRect.w = 30;
    shotRect.h = 30;

    shot = calculVector(shot, shotRect);

    shot->dstRect = shotRect;
    shot->vel = 15;
    shot->strength = 10;

    shot->next = attack->nextShot;
    attack->nextShot = shot;
}

void slowingShot(SpecialAttack *attack)
{
    Shot *shot = smalloc(sizeof(Shot), 1);
    SDL_Rect shotRect;

    shotRect.x = (_boss.dstRect.x + CHAR_W / 2);
    shotRect.y = (_boss.dstRect.y + CHAR_H / 3);
    shotRect.w = 50;
    shotRect.h = 50;

    shot = calculVector(shot, shotRect);

    shot->dstRect = shotRect;
    shot->vel = 15;
    shot->strength = 10;

    shot->next = attack->nextShot;
    attack->nextShot = shot;
}

void weakeningShot(SpecialAttack *attack)
{
    Shot *shot = smalloc(sizeof(Shot), 1);
    SDL_Rect shotRect;

    shotRect.x = (_boss.dstRect.x + CHAR_W / 2);
    shotRect.y = (_boss.dstRect.y + CHAR_H / 3);
    shotRect.w = 30;
    shotRect.h = 30;

    shot = calculVector(shot, shotRect);

    shot->dstRect = shotRect;
    shot->vel = 15;
    shot->strength = 10;

    shot->next = attack->nextShot;
    attack->nextShot = shot;
}

void poisoningShot(SpecialAttack *attack)
{
    Shot *shot = smalloc(sizeof(Shot), 1);
    SDL_Rect shotRect;

    shotRect.x = (_boss.dstRect.x + CHAR_W / 2);
    shotRect.y = (_boss.dstRect.y + CHAR_H / 3);
    shotRect.w = 100;
    shotRect.h = 50;

    shot = calculVector(shot, shotRect);

    shot->dstRect = shotRect;
    shot->vel = 10;
    shot->strength = 10;

    shot->next = attack->nextShot;
    attack->nextShot = shot;
}

void rainingShot(SpecialAttack *attack)
{
    int random = rand()%SCREEN_WIDTH;

    Shot *shot = smalloc(sizeof(Shot), 1);
    SDL_Rect shotRect;

    shotRect.x = random;
    shotRect.y = 5;
    shotRect.w = 50;
    shotRect.h = 50;

    shot->vectorX = 0;
    shot->vectorY = 1;

    shot->dstRect = shotRect;
    shot->vel = 10;
    shot->strength = 10;

    shot->next = attack->nextShot;
    attack->nextShot = shot;
}

Shot *calculVector(Shot *shot, SDL_Rect shotRect)
{
    float norm;

    if(((_mainChar.dstRect.x + CHAR_W / 2) - shotRect.x) > 0 && ((_mainChar.dstRect.y + CHAR_H / 3) - shotRect.y) < 0)
    {
        // Higher Y && Higher X
        norm = sqrt(pow(((_mainChar.dstRect.x + CHAR_W / 2) - shotRect.x),2) + pow((shotRect.y - (_mainChar.dstRect.y + CHAR_H / 3)),2));
        shot->vectorX = ((_mainChar.dstRect.x + CHAR_W / 2) - shotRect.x)/norm;
        shot->vectorY = -(shotRect.y - (_mainChar.dstRect.y + CHAR_H / 3))/norm;
    }
    else if(((_mainChar.dstRect.x + CHAR_W / 2) - shotRect.x) < 0 && ((_mainChar.dstRect.y + CHAR_H / 3) - shotRect.y) < 0)
    {
        // Higher Y && Lower X
        norm = sqrt( pow((shotRect.x -(_mainChar.dstRect.x + CHAR_W / 2)),2) + pow((shotRect.y - (_mainChar.dstRect.y + CHAR_H / 3)),2));
        shot->vectorX = -(shotRect.x - (_mainChar.dstRect.x + CHAR_W / 2))/norm;
        shot->vectorY = -(shotRect.y - (_mainChar.dstRect.y + CHAR_H / 3))/norm;
    }
    else if(((_mainChar.dstRect.x + CHAR_W / 2) - shotRect.x) < 0 && ((_mainChar.dstRect.y + CHAR_H / 3) - shotRect.y) > 0)
    {
        // Lower Y && Lower X
        norm = sqrt(pow(((_mainChar.dstRect.x + CHAR_W / 2) - shotRect.x),2) + pow(((_mainChar.dstRect.y + CHAR_H / 3) - shotRect.y),2));
        shot->vectorX = ((_mainChar.dstRect.x + CHAR_W / 2) - shotRect.x)/norm;
        shot->vectorY = ((_mainChar.dstRect.y + CHAR_H / 3) - shotRect.y)/norm;
        //printf("Vector X => %f | Vector Y => %f\n", shot->vectorX, shot->vectorY);
    }
    else
    {
        // Lower Y && Higher X
        norm = sqrt(pow((shotRect.x - (_mainChar.dstRect.x + CHAR_W / 2)),2) + pow(((_mainChar.dstRect.y + CHAR_H / 3) - shotRect.y),2));
        shot->vectorX = -(shotRect.x - (_mainChar.dstRect.x + CHAR_W / 2))/norm;
        shot->vectorY = ((_mainChar.dstRect.y + CHAR_H / 3) - shotRect.y)/norm;
    }

    return shot;
}
//================================= END ABILITY=============================================

void updateSpecialAbility(SpecialAttack *attack)
{
    if(SDL_GetTicks() - attack->timerFrame < attack->frameCooldown)return;

    while(attack != NULL)
    {
        attack->timerFrame = SDL_GetTicks();
        attack->stateFrame++;
        if(attack->stateFrame == attack->frame)attack->stateFrame = 0;

        attack = attack->next;
    }
}

void removeShotSpecial(SpecialAttack *attack, int pos)
{
    Shot *crtBuffer = NULL;
    Shot *previousBuffer = NULL;
    int   i = 0;

    crtBuffer = attack->nextShot;

    if (crtBuffer == NULL) {
        return;
    }
    if(pos == 0)
    {
        attack->nextShot = crtBuffer->next;
        free(crtBuffer);
        return;
    }

    for (i = 0; crtBuffer != NULL && i < pos; i++) {
        previousBuffer = crtBuffer;
        crtBuffer      = crtBuffer->next;
    }
    previousBuffer->next = crtBuffer->next;
    free(crtBuffer);
}

void renderAbility()
{
    SpecialAttack *attack = _boss.specialAttack;

    if(_mainChar.state == CHAR_SPECIAL)
    {
        if(!_mainChar.specialAttack->isInversed)SDL_RenderCopyEx(_renderer, _mainChar.specialAttack->sprite, &_mainChar.specialAttack->spriteRectSrc[_mainChar.specialAttack->stateFrame], &_mainChar.specialAttack->spriteRectDst, 180, NULL, SDL_FLIP_HORIZONTAL | SDL_FLIP_VERTICAL);
        else SDL_RenderCopyEx(_renderer, _mainChar.specialAttack->sprite, &_mainChar.specialAttack->spriteRectSrc[_mainChar.specialAttack->stateFrame], &_mainChar.specialAttack->spriteRectDst, 0, NULL, SDL_FLIP_HORIZONTAL);
    }

    while(attack != NULL)
    {

        if( attack->type == SHOT || attack->type == RAIN)
        {
            renderShotList(attack);
        }
        else if(attack->type == RAY)
        {
            if(attack->isActivated)
            {
                if(!attack->isInversed)SDL_RenderCopyEx(_renderer, attack->sprite, &attack->spriteRectSrc[attack->stateFrame], &attack->spriteRectDst, 180, NULL, SDL_FLIP_HORIZONTAL | SDL_FLIP_VERTICAL);
                else SDL_RenderCopyEx(_renderer, attack->sprite, &attack->spriteRectSrc[attack->stateFrame], &attack->spriteRectDst, 0, NULL, SDL_FLIP_HORIZONTAL);
            }
        }
        attack = attack->next;
    }

}

void renderShotList(SpecialAttack *attack)
{
    Shot *shot = attack->nextShot;

    while(shot != NULL)
    {
        SDL_RenderCopy(_renderer, attack->sprite, &attack->spriteRectSrc[attack->stateFrame], &shot->dstRect);
        shot = shot->next;
    }
}
