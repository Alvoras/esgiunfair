#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include "../headers/config.h"
#include "../headers/menu.h"
#include "../headers/init.h"
#include "../headers/utility.h"
#include "../headers/load.h"
#include "../headers/game.h"
#include "../headers/linked_list.h"
#include "../headers/sdl_tools.h"
#include "../headers/ability.h"

// extern : this var comes from another file
extern SDL_Window *  _mainWindow;
extern SDL_Renderer *_renderer;

extern MainCharacter _mainChar;
extern Boss          _boss;
extern LifeBar       _bossLifeBar;
extern LifeBar       _lifeBarHolder;

extern SDL_Rect _shadowRect;

extern int  _blackscreenSec;
extern long _blackscreenMillisec;
extern int  _blackscreenOpacity;
extern int  _fadeIn;
extern int  _fadeOut;

extern SDL_Texture *_textures[TEXTURES_TOTAL];

extern TTF_Font *_menuFont;
extern TTF_Font *_dialogFont;
extern TTF_Font *_announcementFont;

extern Mix_Chunk *_sounds[SOUND_TOTAL];
extern int        _laserChannel;
extern Mix_Music *_music;
extern int        _gameParams[CONFIG_TOTAL];
extern int        _isMusicPlaying;

extern SDL_Color color_white;

extern MenuStruct _localMultiModeMenuTextures[INPUT_TOTAL];

extern int _playingBoss;
extern int tWaitingForPlayer2;

extern int   tCloseClient;
extern int   tCloseServer;
extern char *tSerialDataToServer;
extern char *tSerialDataFromServer;

unsigned int getLineQty(FILE *fp) {
    int cnt = 0;

    char buf[250];

    while (fgets(buf, 250, fp) != NULL) {
        cnt++;
    }

    rewind(fp);
    return cnt;
}

// Load image at specified path
SDL_Texture *loadTexture(char *path) {
    SDL_Surface *surfaceBuffer = IMG_Load(path);
    SDL_Texture *loadedSurface = NULL;

    if (surfaceBuffer == NULL) {
        logger("Failed to load surface ", LOGGER_ERR);
        printf("%s\n", IMG_GetError());
    }else{
        SDL_SetColorKey(surfaceBuffer, SDL_TRUE, SDL_MapRGB(surfaceBuffer->format, 0x00, 0x80, 0x80));

        loadedSurface = SDL_CreateTextureFromSurface(_renderer, surfaceBuffer);

        if (loadedSurface == NULL) {
            logger("Failed to load texture ", LOGGER_ERR);
            printf("%s\n", SDL_GetError());
        }

        SDL_FreeSurface(surfaceBuffer);
    }

    return loadedSurface;
}

char *createLevelStr(char *item, char *assetType, char *asset, int level) {
    char *str        = smalloc(sizeof(char), 50);
    char *levelsPath = "config/levels/%d/";

    sprintf(str, levelsPath, level);
    strcat(str, item);
    if (assetType != NULL) {
        strcat(str, assetType);
    }
    strcat(str, asset);
    return str;
}

char *createSaveStr(int toLoad, int saveId) {
    char *str = smalloc(sizeof(char), 50);

    char *levelsPath = "config/save/%d/";

    sprintf(str, levelsPath, saveId);

    switch (toLoad) {
    case LOAD_PASSIVE:
        strcat(str, "passive");
        break;

    case LOAD_SPECS:
        strcat(str, "specs");
        break;

    case LOAD_LEVEL:
        strcat(str, "level");
        break;

    case LOAD_KEYMAP:
        strcat(str, "keymap");
        break;

    case LOAD_KEYMAP_CUSTOM:
        strcat(str, "keymap_custom");
        break;

    case LOAD_SAVE:
        strcat(str, "save");
        break;

    case NEW_FILE:
        strcat(str, ".new");
        break;
    }

    return str;
}

void closeSDL() {
    //int i = 0;
    // TODO
    // DESTROY CHAR

    /*for (i = 0; i < BOSS_STATES_TOTAL; i++) {
     * SDL_DestroyTexture(_boss.textures[i]);
     * _boss.textures[i] = NULL;
     * }
     *
     * for (i = 0; i < CHAR_STATES_TOTAL; i++) {
     * SDL_DestroyTexture(_mainChar.textures[i]);
     * _mainChar.textures[i] = NULL;
     * }
     */
    // Destroy window
    SDL_DestroyWindow(_mainWindow);
    _mainWindow = NULL;

    // Quit SDL_Image
    IMG_Quit();

    // Quit SDL_TTF
    TTF_Quit();

    // Quit SDL_mixer
    Mix_CloseAudio();

    // Quit SDL subsystems
    SDL_Quit();

    free(tSerialDataToServer);
    free(tSerialDataFromServer);

    // Exit the server and thec client
    tCloseClient = 1;
    tCloseClient = 1;
}

SDL_Texture *createDialogText(char *str, SDL_Color color) {
    SDL_Surface *surfaceStr;
    SDL_Texture *textureStr;
    int          len    = 0;
    int          crtLen = 0;

    crtLen = strlen(str);

    char *displayStr = smalloc(sizeof(char), TEXT_MAX_LETTERS_PER_DISPLAY + 1);

    len = (crtLen < TEXT_MAX_LETTERS_PER_DISPLAY) ? crtLen : TEXT_MAX_LETTERS_PER_DISPLAY;

    strncpy(displayStr, str, len);

    displayStr[len - 1] = 0x00;

    surfaceStr = TTF_RenderUTF8_Blended(_dialogFont, displayStr, color);

    textureStr = SDL_CreateTextureFromSurface(_renderer, surfaceStr);

    SDL_FreeSurface(surfaceStr);
    free(displayStr);
    return textureStr;
}

void renderSprite(SDL_Texture *texture, SDL_Rect *charSrcRect, SDL_Rect *charDstRect) {
    SDL_RenderCopy(_renderer, texture, charSrcRect, charDstRect);
}

void renderProjectiles() {
    Shot *crtShot = _mainChar.shotList->first;

    while (crtShot != NULL) {
        renderSprite(_mainChar.shotList->animation->spriteSheets[crtShot->state], &_mainChar.shotList->animation->spriteRects[crtShot->state][crtShot->frame], &crtShot->dstRect);
        crtShot = crtShot->next;
    }
}

void renderDialog(int x, int y, int w, int h, int mode) {
    SDL_Rect displayRect;

    displayRect.y = y;
    displayRect.x = x;
    displayRect.w = w;
    displayRect.h = h;

    switch (mode) {
    case DIALOG_MODE_TEXT_TITLE:
        renderSprite(_textures[DIALOG_TEXT_TITLE], NULL, &displayRect);
        break;

    case DIALOG_MODE_TEXT:
        renderSprite(_textures[DIALOG_TEXT], NULL, &displayRect);
        break;
    }
}

void renderCenterText(char *str, TTF_Font *font, int fontWidth) {
    SDL_Surface *surfaceStr = NULL;
    SDL_Texture *textureStr = NULL;

    int len            = strlen(str);
    int strTotalLength = len * fontWidth;

    SDL_Rect rectStr = {
        (SCREEN_WIDTH / 2) - (strTotalLength / 2),
        (SCREEN_HEIGHT / 2) - 25,
        strTotalLength,
        50
    };

    surfaceStr = TTF_RenderUTF8_Blended(font, str, color_white);

    if (surfaceStr == NULL) {
        logger("", LOGGER_FAIL);
        SDL_GetError();
        exit(EXIT_FAILURE);
    }

    textureStr = SDL_CreateTextureFromSurface(_renderer, surfaceStr);

    if (textureStr == NULL) {
        logger("", LOGGER_FAIL);
        SDL_GetError();
        exit(EXIT_FAILURE);
    }

    renderSprite(textureStr, NULL, &rectStr);

    SDL_FreeSurface(surfaceStr);
}

int renderWaitingScreen() {
    long int     waitTimer         = SDL_GetTicks();
    SDL_Surface *surfaceBackground = NULL;
    SDL_Texture *textureBackground = NULL;

    char *displayStr = "En attente du joueur 2...";

    surfaceBackground = SDL_CreateRGBSurface(0, SCREEN_WIDTH, SCREEN_HEIGHT, 32, 0, 0, 0, 0);

    if (surfaceBackground == NULL) {
        logger("", LOGGER_FAIL);
        SDL_GetError();
        exit(EXIT_FAILURE);
    }

    textureBackground = SDL_CreateTextureFromSurface(_renderer, surfaceBackground);

    if (textureBackground == NULL) {
        logger("", LOGGER_FAIL);
        SDL_GetError();
        exit(EXIT_FAILURE);
    }

    // BUG
    // Not working ?
    //SDL_SetTextureAlphaMod(textureBackground, SDL_BLENDMODE_BLEND);
    //SDL_SetTextureAlphaMod(textureBackground, 50);

    //renderMenu(_localMultiModeMenuTextures, LOCAL_MULTI_TOTAL);
    renderSprite(textureBackground, NULL, NULL);
    renderCenterText(displayStr, _dialogFont, 16);

    SDL_RenderPresent(_renderer);

    while ((tWaitingForPlayer2)) {
        if (SDL_GetTicks() - waitTimer > 15000) {
            return 0;
        }
    }

    SDL_FreeSurface(surfaceBackground);

    return 1;
}

void renderBossHud() {
}

void renderCharSpecial() {
    int i = 0;

    SDL_Rect renderRect = { 0 };

    for (i = 0; i < _mainChar.charges; i++) {
        renderRect = (SDL_Rect){
            20 + (i * 23),
            SCREEN_HEIGHT - (25),
            20,
            20
        };

        renderSprite(_textures[CHAR_MANA_ICON], NULL, &renderRect);
    }
}

void renderCharHealth() {
    int i = 0;

    SDL_Rect renderRect = { 0 };

    for (i = 0; i < _mainChar.hp; i++) {
        renderRect = (SDL_Rect){
            20 + (i * 23),
            SCREEN_HEIGHT - (25 + 25),           // SCREEN_HEIGHT - (health bar height + margin top + special bar height)
            20,
            20
        };

        renderSprite(_textures[CHAR_HEART], NULL, &renderRect);
    }
}

void renderVictory() {
    //_mainChar.animationLock = 1;
    renderCenterText("Victoire !", _announcementFont, 30);
}

void renderDefeat() {
    //_mainChar.animationLock = 1;
    renderCenterText("Defaite !", _announcementFont, 30);
}

void render() {
    SDL_RenderClear(_renderer);

    renderBackground();
    loadLifeBar(_bossLifeBar, _lifeBarHolder, _boss.hp, LIFEBAR_HORIZONTAL);
    loadPlatforms();

    SDL_RenderCopy(_renderer, _textures[GROUND_SHADOW], NULL, &_shadowRect);
    renderSprite(_boss.spriteSheets[_boss.state], &_boss.spriteRects[_boss.state][_boss.frame], &_boss.dstRect);
    if ((_mainChar.specs.isInvulnerable && _mainChar.specs.frameInvulnerable == 1) || _mainChar.specs.isInvulnerable == 0) {
        renderSprite(_mainChar.spriteSheets[_mainChar.state], &_mainChar.spriteRects[_mainChar.state][_mainChar.frame], &_mainChar.dstRect);
    }
    renderProjectiles();
    renderAbility();

    if ((_playingBoss)) {
        renderBossHud();
    }
    renderCharHealth();
    renderCharSpecial();

    dialog();

    renderBlackscreen();

    // if victory
    // renderVictory();

    // if defeat
    // renderDefeat();

    SDL_RenderPresent(_renderer);
}

void toggleBlackscreenFade(int mode) {
    switch (mode) {
    case FADE_IN:
        _fadeIn = (_fadeIn) ? 0 : 1;
        break;

    case FADE_OUT:
        _fadeOut = (_fadeOut) ? 0 : 1;
        break;
    }
}

void setBlackscreenTimer(int sec, long millisec) {
    float floatingBuffer = 0.0;

    millisec /= MAX_OPACITY;

    // Since we pass the duration of the total animation, we divide this by the number of steps
    if (sec == 1) {
        floatingBuffer = (float)sec / MAX_OPACITY;
        millisec      += ceil(floatingBuffer * 100000);
        sec            = 0;
    }else{
        sec /= MAX_OPACITY;
    }

    _blackscreenSec      = sec;
    _blackscreenMillisec = millisec;
}

void renderBlackscreen() {
    if ((_fadeIn)) {
        fadeBlackscreen(FADE_IN);
    }else if ((_fadeOut)) {
        fadeBlackscreen(FADE_OUT);
    }

    // Apply new alpha
    applyBlackscreenAlpha();
}

void applyBlackscreenAlpha() {
    SDL_SetTextureAlphaMod(_textures[BLACKSCREEN], _blackscreenOpacity);

    renderSprite(_textures[BLACKSCREEN], NULL, NULL);
}

void gameBlackScreen(int mode) {
    switch (mode) {
    case FADE_IN:
        _fadeIn = 1;
        break;

    case FADE_OUT:
        _fadeIn = 1;
        break;
    }
}

void doFadeBlackscreen(int mode) {
    switch (mode) {
    case FADE_IN:
        while (_blackscreenOpacity < MAX_OPACITY) {
            _blackscreenOpacity++;
            applyBlackscreenAlpha();
            SDL_RenderPresent(_renderer);
            crossSleep(_blackscreenSec, _blackscreenMillisec);
        }
        _blackscreenOpacity = MAX_OPACITY;
        _fadeIn             = 0;
        break;

    case FADE_OUT:
        while (_blackscreenOpacity > 0) {
            _blackscreenOpacity--;
            applyBlackscreenAlpha();
            SDL_RenderPresent(_renderer);
            crossSleep(_blackscreenSec, _blackscreenMillisec);
        }
        _blackscreenOpacity = 0;
        _fadeOut            = 0;
        break;
    }
}

void fadeBlackscreen(int mode) {
    switch (mode) {
    case FADE_IN:
        if (_blackscreenOpacity < MAX_OPACITY) {
            _blackscreenOpacity++;
        }else{
            _blackscreenOpacity = MAX_OPACITY;
            _fadeIn             = 0;
        }
        break;

    case FADE_OUT:
        if (_blackscreenOpacity > 0) {
            _blackscreenOpacity--;
        }else{
            _blackscreenOpacity = 0;
            _fadeOut            = 0;
        }

        break;
    }
    crossSleep(_blackscreenSec, _blackscreenMillisec);
}

int comparePixel(int x, int y, int r, int g, int b) {
    unsigned int compareR, compareG, compareB, pixel;
    SDL_Rect     readPixel;

    readPixel.h = 1;
    readPixel.w = 1;
    readPixel.x = x;
    readPixel.y = y;

    SDL_RenderReadPixels(_renderer, &readPixel, SDL_PIXELFORMAT_RGB888, &pixel, 1);
    compareR = (pixel & 0x0000FF);
    compareG = (pixel & 0x00FF00) >> 8;
    compareB = (pixel & 0xFF0000) >> 16;

    if (r == compareR && g == compareG && b == compareB) {
        return 1;
    }
    return 0;
}

void playSound(int flag) {
    if (!(_gameParams[CONFIG_SOUND_EFFECTS_PLAY])) {
        return;
    }
    switch (flag) {
    case SOUND_CHAR_SPECIAL:
        if ((_laserChannel = Mix_PlayChannel(-1, _sounds[flag], 0)) == -1) {
            logger("Failed to play sound ", LOGGER_ERR);
            printf("%d : %s\n", flag, Mix_GetError());
        }

        break;

    default:
        if (Mix_PlayChannel(-1, _sounds[flag], 0) == -1) {
            logger("Failed to play sound ", LOGGER_ERR);
            printf("%d : %s\n", flag, Mix_GetError());
        }
    }
}

void stopSound(int channel) {
    // No return code to detect error from
    Mix_HaltChannel(channel);
}

void playMusic() {
    if (!(_gameParams[CONFIG_MUSIC_PLAY]) || !(_gameParams[CONFIG_MAIN_VOLUME_PLAY]) || (_isMusicPlaying)) {
        return;
    }
    // -1 plays the music forever
    if (Mix_PlayMusic(_music, -1) == -1) {
        logger("Failed to play background music ", LOGGER_ERR);
    }
    _isMusicPlaying = 1;
}

void setParam(int flag, int value) {
    int    i = 0;
    char   identifier[50] = { 0x00 };
    char   lookupStr[50]  = { 0x00 };
    int    crtValue       = -1;
    char * writeBuffer    = NULL;
    int    bufferLength   = -1;
    char   buffer[4]      = { 0x00 };
    int    fileLength     = -1;
    char * configContent  = NULL;
    char **configArray    = NULL;
    FILE * fp             = NULL;

    int counter = -1;

    int pos = -1;

    switch (flag) {
    case CONFIG_FULLSCREEN:
        strcpy(lookupStr, "fullscreen");
        break;

    case CONFIG_MUSIC_PLAY:
        strcpy(lookupStr, "music_play");
        break;

    case CONFIG_MUSIC_VOLUME:
        strcpy(lookupStr, "music_volume");
        break;

    case CONFIG_SOUND_EFFECTS_PLAY:
        strcpy(lookupStr, "sound_effects_play");
        break;

    case CONFIG_SOUND_EFFECTS_VOLUME:
        strcpy(lookupStr, "sound_effects_volume");
        break;

    case CONFIG_MAIN_VOLUME_PLAY:
        strcpy(lookupStr, "main_volume_play");
        break;

    case CONFIG_MAIN_VOLUME:
        strcpy(lookupStr, "main_volume");
        break;
    }

    fp = sfopen(PARAM_FILE_PATH, "r");

    for (i = 0; i < CONFIG_TOTAL; i++) {
        if (fscanf(fp, "%s %d", identifier, &crtValue)) {
            pos = ftell(fp);
            if (strcmp(identifier, lookupStr) == 0) {
                logger("Found the wanted config line ", LOGGER_INFO);
                printf("(%s)\n", identifier);
                break;
            }
        }
        if (i == CONFIG_TOTAL - 1) {
            logger("Failed to find the param to set\n", LOGGER_ERR);
            return;
        }
    }

    rewind(fp);

    fileLength = filelen(fp);

    configContent = smalloc(sizeof(char), fileLength);

    if (!(fread(configContent, sizeof(char), fileLength, fp))) {
        logger("Failed to read param file\n", LOGGER_ERR);
    }

    // Split the config file using space as a delimiter
    configArray = splitStr(configContent, "\n ", &counter);

    i = 0;

    configArray[counter] = NULL;

    // Find the pos of the wanted config key
    while (configArray[i] != NULL) {
        if (strcmp(configArray[i], lookupStr) == 0) {
            pos = i;
            printf("found %d\n", pos);
            break;
        }
        i++;
    }

    // Turn int to string
    sprintf(buffer, "%d", value);

    bufferLength = strlen(buffer);

    // Overwrite with new value
    strcpy(configArray[pos + 1], buffer);

    writeBuffer = smalloc(sizeof(char), (fileLength - 1) + bufferLength);

    // Join back the splitted config file
    writeBuffer = keyValJoin(configArray);

    fclose(fp);

    fp = NULL;

    fp = fopen(PARAM_FILE_PATH, "w");

    if (!(fwrite(writeBuffer, strlen(writeBuffer), 1, fp))) {
        logger("Failed to write param file\n", LOGGER_ERR);
    }

    if (writeBuffer != NULL) {
        free(writeBuffer);
    }
    fclose(fp);
}

void applySoundParams() {
    int i = 0;

    // Pause music is needed
    if (!(_gameParams[CONFIG_MUSIC_PLAY])) {
        if ((_isMusicPlaying)) {
            Mix_HaltMusic();
            _isMusicPlaying = 0;
        }
    }else{
        if (!(_isMusicPlaying)) {
            Mix_PlayMusic(_music, -1);
            _isMusicPlaying = 1;
        }
    }

    // Set music volume
    Mix_VolumeMusic(_gameParams[CONFIG_MUSIC_VOLUME]);

    // Set sound effecs volume
    if ((_gameParams[CONFIG_SOUND_EFFECTS_PLAY])) {
        for (i = 0; i < SOUND_TOTAL; i++) {
            if (_sounds[i] != NULL) {
                Mix_VolumeChunk(_sounds[i], _gameParams[CONFIG_SOUND_EFFECTS_VOLUME]);
            }
        }
    }else{
        for (i = 0; i < SOUND_TOTAL; i++) {
            if (_sounds[i] != NULL) {
                Mix_VolumeChunk(_sounds[i], 0);
            }
        }
    }

    // Set master volume
    if ((_gameParams[CONFIG_MAIN_VOLUME_PLAY])) {
        // Use -1 to apply the volume to all the channels
        Mix_Volume(-1, _gameParams[CONFIG_MAIN_VOLUME]);
        // Set the music's volume to the music volume * 0.[current main volume]
        // So for a 50% main volume and a 100% music volume, we get 100*0.50 = 50% actual music level
        Mix_VolumeMusic(_gameParams[CONFIG_MUSIC_VOLUME] * ((int)getPercentFromVal(_gameParams[CONFIG_MAIN_VOLUME], 128) / 100));
    }else{
        Mix_Volume(-1, 0);
        if ((_isMusicPlaying)) {
            Mix_HaltMusic();
            _isMusicPlaying = 0;
        }
    }
}

void toggleSound(int flag) {
    switch (flag) {
    case MUSIC:
        _gameParams[CONFIG_MUSIC_PLAY] = (_gameParams[CONFIG_MUSIC_PLAY]) ? 0 : 1;
        break;

    case EFFECTS:
        _gameParams[CONFIG_SOUND_EFFECTS_PLAY] = (_gameParams[CONFIG_SOUND_EFFECTS_PLAY]) ? 0 : 1;
        break;

    case SOUND:
        _gameParams[CONFIG_MAIN_VOLUME_PLAY] = (_gameParams[CONFIG_MAIN_VOLUME_PLAY]) ? 0 : 1;
        break;
    }
}

void freePointer() {
    int i = 0;

    printf("************ ALL FREE **************\n");
    //Free MAIN CHAR Pointer
    SpecialAttack *crtAttack  = _boss.specialAttack;
    SpecialAttack *prevAttack = _boss.specialAttack;
    Shot *         crtShot    = _mainChar.shotList->first;
    Shot *         prevShot   = NULL;

    if (_mainChar.specialAttack != NULL) {
        SDL_DestroyTexture(_mainChar.specialAttack->sprite);
        free(_mainChar.specialAttack->spriteRectSrc);
        _mainChar.specialAttack->spriteRectSrc = NULL;
        printf("%p\n", _mainChar.specialAttack);
        free(_mainChar.specialAttack);
        _mainChar.specialAttack = NULL;
    }
    while (crtShot != NULL) {
        prevShot = crtShot;
        crtShot  = crtShot->next;
        free(prevShot);
    }
    if (_mainChar.shotList != NULL) {
        free(_mainChar.shotList);
        _mainChar.shotList = NULL;
    }
    if (_mainChar.speech.text != NULL) {
        free(_mainChar.speech.text);
        _mainChar.speech.text = NULL;
    }
    //Free BOSS Pointer
    crtShot  = _boss.specialAttack->nextShot;
    prevShot = NULL;

    if (_boss.speech.text != NULL) {
        free(_boss.speech.text);
    }
    while (crtAttack != NULL) {
        while (crtShot != NULL) {
            prevShot = crtShot;
            crtShot  = crtShot->next;
            free(prevShot);
        }
        crtAttack->spriteRectSrc = NULL;
        SDL_DestroyTexture(crtAttack->sprite);
        free(crtAttack->spriteRectSrc);
        crtAttack->spriteRectSrc = NULL;

        prevAttack = crtAttack;
        crtAttack  = crtAttack->next;
        free(prevAttack);
    }

    // Free SDL_mixer
    for (i = 0; i < SOUND_TOTAL; i++) {
        Mix_FreeChunk(_sounds[i]);
    }

    Mix_FreeMusic(_music);

    printf("************ FREE SUCCESS **************\n");
    //verifFree();
}

void verifFree() {
    SpecialAttack *crtAttack = _boss.specialAttack;

    printf("Char special attaque : %p\n", _mainChar.specialAttack);
    while (_boss.specialAttack != NULL) {
        printf("Boss special attaque : %p\n", crtAttack);
        crtAttack = crtAttack->next;
    }
    printf("Boss special attack : %p\n", _boss.specialAttack);
}
