#include <stdlib.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../headers/config.h"
#include "../headers/sdl_tools.h"
#include "../headers/linked_list.h"
#include "../headers/game.h"
#include "../headers/character.h"
#include "../headers/boss.h"

// extern : this var comes from another file
extern MainCharacter _mainChar;
extern Boss          _boss;
extern LifeBar       _bossLifeBar;
extern SDL_Renderer *_renderer;
extern int _timerStartFight;

void updateBossPhysics() {
    if (_boss.velX == 0 && !(isBossDead())) {
        _boss.state = BOSS_IDLE;
        _boss.state_blank = BOSS_IDLE_BLANK;
    }

    if (SDL_GetTicks() - _boss.timer >= BOSS_ANIM_DELAY) {
        _boss.timer = SDL_GetTicks();

        _boss.frame++;
    }

    if ((isBossDead()) && _boss.frame == _boss.frameQty[BOSS_DEATH]) {
        exit(EXIT_SUCCESS);
    }

    if (_boss.frame >= _boss.frameQty[_boss.state] && !(isBossDead())) {
        _boss.frame = 1;
    }
}

int isBossDead() {
    int res = 0;

    if (_boss.state == BOSS_DEATH) {
        res = 1;
    }

    return(res);
}

void damageBoss(Shot *shot, int shotPosition) {
    if (!(isBossDead())) {
        _boss.state = BOSS_HIT;
        _boss.state_blank = BOSS_HIT_BLANK;
        playSound(SOUND_BOSS_HIT);
    }
    if(_mainChar.specs.isWeakened)_boss.hp -= shot->strength/2;
    else _boss.hp -= shot->strength;

    removeShot(shotPosition, CHAR_SHOT_LIST);

    if (_boss.hp <= 0 && !(isBossDead())) {
        _boss.state = BOSS_DEATH;
        _boss.frame = 0;
        playSound(SOUND_BOSS_DEATH);
    }
}

void damageBossSpecial(int strength)
{
    if (!(isBossDead())) {
        _boss.state = BOSS_HIT;
        _boss.state_blank = BOSS_HIT_BLANK;
    }
    if(_mainChar.specs.isWeakened) _boss.hp -= strength/2;
    else _boss.hp -= strength;

    if (_boss.hp <= 0 && !(isBossDead())) {
        _boss.state = BOSS_DEATH;
        _boss.frame = 0;
        //exit(EXIT_SUCCESS);
    }
}

int isBossHit(SDL_Rect *rect) {
    int res = 0;
    int i = 0;

    int posPixelx = rect->x;
    int posPixely = rect->y;

    if (posPixelx > (_boss.dstRect.x) && posPixely > (_boss.dstRect.y) && posPixelx < (_boss.dstRect.x + _boss.dstRect.w) && posPixely < (_boss.dstRect.y + _boss.dstRect.h)) {

        //printf("%d %d %d %d\n", (_boss.dstRect.x), (_boss.dstRect.y), (_boss.dstRect.x + _boss.dstRect.w), (_boss.dstRect.y + _boss.dstRect.h));
        renderSprite(_boss.spriteSheets[_boss.state_blank], &_boss.spriteRects[_boss.state][_boss.frame], &_boss.dstRect);
        for(i = 0; i < 5; i++)
        {
            if(comparePixel(posPixelx, posPixely, 255, 255, 255)) //Compare pixel blank with actual pixel
            {
                res = 1;
                break;
            }

            switch(i)
            {
                case 0:
                    posPixelx += rect->w;
                    break;
                case 1:
                    posPixely += rect->h;
                    break;
                case 2:
                    posPixelx -= rect->w;
                    break;
                case 3:
                    posPixelx = (rect->x + rect->w)/2;
                    posPixely = (rect->y + rect->h)/2;
                    break;
            }
        }
    }

    return(res);
}

int isBossHitSpecial(SpecialAttack *ability){
    int res = 0;

    CalculCoord codeA, codeB;
    codeA = calculCoord(_boss.dstRect.x, _boss.dstRect.y, ability->spriteRectDst.x, ability->spriteRectDst.y, (ability->spriteRectDst.x + ability->spriteRectDst.w), (ability->spriteRectDst.y + ability->spriteRectDst.h));
    codeB = calculCoord((_boss.dstRect.x + _boss.dstRect.w), (_boss.dstRect.y + _boss.dstRect.h), ability->spriteRectDst.x, ability->spriteRectDst.y, (ability->spriteRectDst.x + ability->spriteRectDst.w), (ability->spriteRectDst.y + ability->spriteRectDst.h));

    if(codeA.sum == 0 && codeB.sum == 0)res = 1;
    else if(calculCoordBin(codeA, codeB) == 0)res = 1;

    return(res);
}

void bossAttack(SpecialAttack *attack)
{
    while(attack != NULL)
    {
        if(((SDL_GetTicks() - attack->timer) > attack->cooldown))
        {
            playSound(SOUND_BOSS_SHOOT);
            attack->specialAbility(attack);
            attack->timer = SDL_GetTicks();
            attack->isActivated = 1;
        }
        else if(attack->duration < (SDL_GetTicks() - attack->timer))attack->isActivated = 0;
        attack = attack->next;

    }
}
