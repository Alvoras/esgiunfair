#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include "../headers/config.h"
#include "../headers/load.h"
#include "../headers/utility.h"
#include "../headers/load.h"
#include "../headers/menu.h"
#include "../headers/init.h"
#include "../headers/game.h"
#include "../headers/linked_list.h"
#include "../headers/sdl_tools.h"
#include "../headers/ability.h"

extern SDL_Window *  _mainWindow;
extern SDL_Renderer *_renderer;
extern TTF_Font *    _menuFont;
extern TTF_Font *    _dialogFont;
extern TTF_Font *    _announcementFont;

extern int _gameParams[CONFIG_TOTAL];

extern MainCharacter _mainChar;
extern Boss _boss;
extern LifeBar _bossLifeBar;
extern LifeBar _charLifeBar;
extern LifeBar _lifeBarHolder;
extern Platforms *   _platforms;

extern SDL_Rect _shadowRect;

extern SDL_Texture *_textures[TEXTURES_TOTAL];

extern MenuStruct _menuInGameTexture[IG_TOTAL_MENU];
extern MenuStruct _mainMenuTexture[MAIN_TOTAL_MENU];
extern MenuStruct _paramMenu[PARAM_TOTAL_MENU];
extern MenuStruct _loadCharMenuTextures[LOAD_CHAR_TOTAL];
extern MenuStruct _gameModeMenuTextures[GAME_MODE_TOTAL];
extern MenuStruct _multiplayerMenuTextures[MULTI_TOTAL];
extern MenuStruct _confirmBoxTextures[CONFIRM_TOTAL];
extern MenuStruct _playOptionsBoxTextures[PLAY_TOTAL];
extern MenuStruct _inputTextMenuTextures[INPUT_TOTAL];
extern MenuStruct _localMultiModeMenuTextures[LOCAL_MULTI_TOTAL];
extern MenuStruct _playerDeadMenu[PLAYER_TOTAL_MENU];
extern MenuStruct _soundMenuTextures[SOUND_MENU_TOTAL];

extern Keymap _keymap;

extern char *tSerialDataToServer;
extern char *tSerialDataFromServer;

SDL_Color color_white   = { 255, 255, 255 };
SDL_Color color_black   = { 0, 0, 0 };
SDL_Color color_red     = { 255, 0, 0 };
SDL_Color color_green   = { 0, 255, 0 };
SDL_Color color_blue    = { 0, 0, 255 };
SDL_Color color_cyan    = { 0, 255, 255 };
SDL_Color color_magenta = { 255, 0, 255 };
SDL_Color color_yellow  = { 255, 255, 0 };
SDL_Color color_grey    = { 255 / 2, 255 / 2, 255 / 2 };

int init(int isFullscreen) {
        // Init flag
        int screenFlags = 0;
        int imgFlags    = IMG_INIT_PNG;

        char *title = "ESGI Unfair";

        if ((isFullscreen)) {
                screenFlags = SDL_WINDOW_FULLSCREEN;
        }else {
                screenFlags = SDL_WINDOW_SHOWN;
        }

        srand(time(NULL));


        logger("Loading SDL2...\n", LOGGER_INFO);

        // Init SDL
        if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
                logger("", LOGGER_ERR);
                printf("%s\n", SDL_GetError());
                return 0;
        }
        logger("SDL2 initialized\n", LOGGER_OK);


        logger("Loading SDL_Image...\n", LOGGER_INFO);
        if (!(IMG_Init(imgFlags) & imgFlags)) {
                logger("", LOGGER_ERR);
                printf("%s\n", IMG_GetError());
                return 0;
        }
        logger("SDL_Image initialized\n", LOGGER_OK);

        logger("Loading SDL_ttf...\n", LOGGER_INFO);
        if (TTF_Init() == -1) {
                logger("", LOGGER_ERR);
                printf("%s\n", TTF_GetError());
                return 0;
        }

        logger("SDL_ttf initialized\n", LOGGER_OK);

        logger("Loading fonts...\n", LOGGER_INFO);
        _menuFont         = TTF_OpenFont(BASIC_FONT, 16);
        _dialogFont       = TTF_OpenFont(TEXT_FONT, 16);
        _announcementFont = TTF_OpenFont(ANNOUCEMENT_FONT, 16);

        if (_menuFont == NULL) {
                logger("", LOGGER_ERR);
                printf("%s\n", TTF_GetError());
                exit(EXIT_FAILURE);
        }

        if (_dialogFont == NULL) {
                logger("", LOGGER_ERR);
                printf("%s\n", TTF_GetError());
                exit(EXIT_FAILURE);
        }

        logger("Fonts loaded\n", LOGGER_OK);

        if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 4096) == -1) {
                logger("", LOGGER_ERR);
                printf("%s\n", TTF_GetError());
                exit(EXIT_FAILURE);
        }

        logger("SDL_mixer loaded\n", LOGGER_OK);

        loadMusic();

        // Create window
        _mainWindow = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, screenFlags);

        if (_mainWindow == NULL) {
                logger("", LOGGER_ERR);
                printf("%s\n", SDL_GetError());
                return 0;
        }

        // Create _renderer
        _renderer = SDL_CreateRenderer(_mainWindow, -1, 0);

        if (_renderer == NULL) {
                printf("ERR %s\n", SDL_GetError());
                return 0;
        }

        loadBlackScreen();

        logger("Loading main menu...\n", LOGGER_INFO);
        initMainMenu();
        logger("Main menu initialized\n", LOGGER_OK);

        logger("Loading sound menu...\n", LOGGER_INFO);
        initSoundMenu();
        logger("Sound menu initialized\n", LOGGER_OK);


        logger("Loading game mode menu...\n", LOGGER_INFO);
        initGameModeMenu();
        logger("Game mode menu initialized\n", LOGGER_OK);

        logger("Loading multiplayer menu...\n", LOGGER_INFO);
        initMultiplayerMenu();
        logger("Multiplayer menu initialized\n", LOGGER_OK);

        logger("Loading multiplayer menu...\n", LOGGER_INFO);
        initLocalMultiModeMenu();
        logger("Multiplayer menu initialized\n", LOGGER_OK);

        logger("Loading char selection menu...\n", LOGGER_INFO);
        initLoadCharMenu();
        logger("Char selection scene initialized\n", LOGGER_OK);

        logger("Loading name input menu...\n", LOGGER_INFO);
        initInputTextMenu("Nom du personnage :");
        logger("Name input menu initialized\n", LOGGER_OK);

        logger("Loading confirm box...\n", LOGGER_INFO);
        initConfirmBox();
        logger("Confirm box initialized\n", LOGGER_OK);

        logger("Loading play options box...\n", LOGGER_INFO);
        initPlayOptionsBox();
        logger("Play options box initialized\n", LOGGER_OK);

        logger("Loading in-game menu...\n", LOGGER_INFO);
        initInGameMenu();
        logger("In-game menu initialized\n", LOGGER_OK);

        logger("Loading parameter sub-menu...\n", LOGGER_INFO);
        initParamMenu();
        logger("Parameter sub-menu initialized\n", LOGGER_OK);

        logger("Loading parameter sub-menu...\n", LOGGER_INFO);
        initPlayerMenu();
        logger("Parameter sub-menu initialized\n", LOGGER_OK);

        // Enough space for e.type, e.keysym and e.button
        // Needed to send event data
        tSerialDataToServer   = smalloc((sizeof(int) * 2) + (sizeof(char)), 1);
        tSerialDataFromServer = smalloc((sizeof(int) * 2) + (sizeof(char)), 1);

        SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);

        return 1;
}

void initCharacter(int level, int saveId) {
        _mainChar = (MainCharacter){
                {                                     // Speech structure
                        NULL,                         // speech
                        0,                            // hasTalked
                        0,                            // totalParts
                        0,                            // cnt
                        0                             // timer
                },
                0,                                    // velX
                0,                                    // velY
                1.5,                                  // gravityX
                1.5,                                  // gravityY
                0,                                    // isJumping
                0,                                    // isFalling
                0,                                    // isDashing
                0,                                    // dashTimer
                2,                                    // maxJumps
                0,                                    // currentJump
                0,                                    // initialJumpPos
                { 0 },                                // direction[DIRECTION_TOTAL]
                5,                                    // hp
                50,                                   // strength
                1,                                    // charges
                0,                                    // chargesTimer
                0,                                    // specialCooldownTimer
                0,                                    // frame
                CHAR_IDLE,                            // state
                CHAR_IDLE_BLANK,                      // state_blank
                0,                                    // animationLock
                { NULL },                             // spriteRects
                { 0 },                                // frameQty
                { 0 },                                // spriteWidth
                { 0 },                                // spriteHeight
                { NULL },                             // spriteSheets
                {                                     // rect
                        0,                            // x
                        SCREEN_HEIGHT - (CHAR_H + FLOOR_PADDING), // y
                        CHAR_W,                       // w
                        CHAR_H                        // h
                },
                { 0 },                                // passive
                { 0 },                                // specs
                0,                                    // timer
                NULL,                                 // shotList
                NULL                                  // Special Attack
        };

        _mainChar.timer = SDL_GetTicks();

        _mainChar.shotList        = initShotList();
        _mainChar.shotList->first = NULL;
        _mainChar.shotList->timer = 0;

        _mainChar.specialAttack = smalloc(sizeof(SpecialAttack), 1);
        _mainChar.specialAttack = loadSpecialAttack(level, LOAD_CHARACTER);

        loadCharPassive(saveId);
        loadCharSpecs(saveId);
        loadSpriteSheet(LOAD_CHARACTER, level);
        loadSoundEffects(LOAD_CHARACTER, level);
}

void initBoss(int level) {
        _boss = (Boss){
                {                                     // Speech structure
                        NULL,                         // speech
                        0,                            // hasTalked
                        0,                            // totalParts
                        0,                            // cnt
                        0                             // timer
                },
                0,                                    // velX
                0,                                    // velY
                0,                                    // gravityX
                2,                                    // gravityY
                1000,                                 // hp
                20,                                   // strength
                0,                                    // frame
                BOSS_IDLE,                            // state
                BOSS_IDLE_BLANK,                      // state_blank
                { NULL },                             // spriteRects
                { 0 },                                // frameQty
                { 0 },                                // spriteWidth
                { 0 },                                // spriteHeight
                { NULL },                             // spriteSheets
                {                                     // rect
                        SCREEN_WIDTH - (BOSS_W + 20), // x
                        SCREEN_HEIGHT - (BOSS_H + FLOOR_PADDING), // y
                        BOSS_W,                       // w
                        BOSS_H                        // h
                },
                0,                                    // timer
                NULL,                                 // shotList
                NULL                                  // Special Attack
        };

        _boss.timer = SDL_GetTicks();

        _boss.shotList        = initShotList();
        _boss.shotList->first = NULL;
        _boss.shotList->timer = 0;

        _boss.specialAttack = malloc(sizeof(SpecialAttack));
        _boss.specialAttack = loadSpecialAttack(level, LOAD_BOSS);

        loadSpriteSheet(LOAD_BOSS, level);
        loadSoundEffects(LOAD_BOSS, level);
}

void initScenery(int level) {
        char *spriteConfigPath = NULL;
        FILE *spriteConfigFile = NULL;
        char *spritePath       = NULL;

        int spriteWidth  = 0;
        int spriteHeight = 0;

        int nbPlatforms = 0;
        int posX        = 0;
        int posY        = 0;
        int i           = 0;
        SDL_Texture *texturePlatforms;

        _bossLifeBar = (LifeBar){
                NULL,
                {
                        0,
                        0,
                        45,
                        SCREEN_WIDTH
                },
                {
                        0, // x
                        50, // y
                        SCREEN_WIDTH, // w
                        45 // h
                },
                0,
                0
        };

        _lifeBarHolder = (LifeBar){
                NULL,
                {
                        0,
                        0,
                        45,
                        SCREEN_WIDTH
                },
                {
                        0, // x
                        50, // y
                        SCREEN_WIDTH, // w
                        45 // h
                },
                0,
                0
        };

        // Load background
        spriteConfigPath = createLevelStr("scenery/", NULL, "background", level);
        spriteConfigFile = sfopen(spriteConfigPath, "rb");
        spritePath       = smalloc(sizeof(char), 150);

        fscanf(spriteConfigFile, "%s", spritePath);
        _textures[BACKGROUND_SPRITE] = loadTexture(spritePath);


        // Lifebar holder
        free(spritePath);
        spritePath       = smalloc(sizeof(char), 150);
        spriteConfigPath = createLevelStr("hud/", NULL, "lifebar", level);
        spriteConfigFile = sfopen(spriteConfigPath, "rb");

        fscanf(spriteConfigFile, "%d %d %s", &spriteWidth, &spriteHeight, spritePath);
        _textures[LIFEBAR_HOLDER_SPRITE] = loadTexture(spritePath);

        _lifeBarHolder.sprite = loadTexture(spritePath);
        _lifeBarHolder.w      = SCREEN_WIDTH;
        _lifeBarHolder.h      = spriteHeight;

        // Boss lifebar
        free(spritePath);
        spritePath = smalloc(sizeof(char), 150);

        fscanf(spriteConfigFile, "%d %d %s", &spriteWidth, &spriteHeight, spritePath);
        _textures[LIFEBAR_BOSS_SPRITE] = loadTexture(spritePath);

        _bossLifeBar.sprite = loadTexture(spritePath);
        _bossLifeBar.w      = SCREEN_WIDTH;
        _bossLifeBar.h      = spriteHeight;

        // Char lifebar
        free(spritePath);
        spritePath = smalloc(sizeof(char), 150);

        _textures[CHAR_HEART]     = loadTexture(CHAR_HEART_SPRITE_PATH);
        _textures[CHAR_MANA_ICON] = loadTexture(CHAR_MANA_SPRITE_PATH);
        fscanf(spriteConfigFile, "%d %d %s", &spriteWidth, &spriteHeight, spritePath);
        _textures[LIFEBAR_CHAR_SPRITE] = loadTexture(spritePath);

        _charLifeBar.sprite = loadTexture(spritePath);
        _charLifeBar.w      = 100;
        _charLifeBar.h      = 450;

        // Platforms
        free(spritePath);
        spritePath       = NULL;
        spritePath       = smalloc(sizeof(char), 150);
        spriteConfigPath = createLevelStr("scenery/", NULL, "platforms", level);
        spriteConfigFile = sfopen(spriteConfigPath, "rb");

        fscanf(spriteConfigFile, "%d %s %d %d", &nbPlatforms, spritePath, &spriteWidth, &spriteHeight);
        _platforms       = malloc(sizeof(Platforms) * nbPlatforms);
        texturePlatforms = loadTexture(spritePath);

        for (i = 0; i < nbPlatforms; i++) {
                fscanf(spriteConfigFile, "%d %d", &posX, &posY);
                _platforms[i].nbPlatforms = nbPlatforms;
                _platforms[i].srcRect.h   = spriteHeight;
                _platforms[i].srcRect.w   = spriteWidth;
                _platforms[i].srcRect.x   = 0;
                _platforms[i].srcRect.y   = 0;

                _platforms[i].dstRect.h = 25;
                _platforms[i].dstRect.w = 100;
                _platforms[i].dstRect.x = posX;
                _platforms[i].dstRect.y = posY;
                _platforms[i].texture   = texturePlatforms;
        }

        _textures[GROUND_SHADOW] = loadTexture(GROUND_SHADOW_SPRITE_PATH);
        free(spritePath);
        fclose(spriteConfigFile);
}

void initMainMenu() {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;
        mask       = loadTexture(BACKGROUND_MENU);
        _mainMenuTexture[MAIN_BACKGROUND].texture   = mask;
        _mainMenuTexture[MAIN_BACKGROUND].size      = maskRect;
        _mainMenuTexture[MAIN_BACKGROUND].isVisible = 1;

        _mainMenuTexture[MAIN_LOGO].texture   = loadTexture(LOGO);
        _mainMenuTexture[MAIN_LOGO].isVisible = 1;
        _mainMenuTexture[MAIN_LOGO].size.x    = SCREEN_WIDTH / 2 - 200;
        _mainMenuTexture[MAIN_LOGO].size.y    = 50;
        _mainMenuTexture[MAIN_LOGO].size.w    = 400;
        _mainMenuTexture[MAIN_LOGO].size.h    = 100;

        addMenuOption("Jouer", SCREEN_WIDTH / 2, 175, 200, 50, color_white, _mainMenuTexture, MAIN_PLAY);
        addMenuOption("Parametres", SCREEN_WIDTH / 2, 275, 250, 50, color_white, _mainMenuTexture, MAIN_PARAM);
        addMenuOption("Quitter", SCREEN_WIDTH / 2, 375, 200, 50, color_white, _mainMenuTexture, MAIN_LEAVE);
        addMenuOption("<", SCREEN_WIDTH - 50, 200, 25, 25, color_white, _mainMenuTexture, MAIN_OPENING_CHEVRON);
        addMenuOption(">", 50, 200, 25, 25, color_white, _mainMenuTexture, MAIN_CLOSING_CHEVRON);
}

void initInGameMenu() {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;

        mask = loadTexture(BACKGROUND_MENU);
        _menuInGameTexture[IG_BACKGROUND].texture   = mask;
        _menuInGameTexture[IG_BACKGROUND].size      = maskRect;
        _menuInGameTexture[IG_BACKGROUND].isVisible = 1;

        _menuInGameTexture[IG_LOGO].texture   = loadTexture(LOGO);
        _menuInGameTexture[IG_LOGO].isVisible = 1;
        _menuInGameTexture[IG_LOGO].size.x    = SCREEN_WIDTH / 2 - 200;
        _menuInGameTexture[IG_LOGO].size.y    = 50;
        _menuInGameTexture[IG_LOGO].size.w    = 400;
        _menuInGameTexture[IG_LOGO].size.h    = 100;

        addMenuOption("Continuer", SCREEN_WIDTH / 2, 150, 200, 50, color_white, _menuInGameTexture, IG_CONTINUE);
        addMenuOption("Revenir au menu principal", SCREEN_WIDTH / 2, 225, 500, 50, color_white, _menuInGameTexture, IG_BACK_TO_MENU);
        addMenuOption("Parametres", SCREEN_WIDTH / 2, 300, 250, 50, color_white, _menuInGameTexture, IG_PARAM);
        addMenuOption("Quitter", SCREEN_WIDTH / 2, 375, 200, 50, color_white, _menuInGameTexture, IG_LEAVE);
        addMenuOption("<", SCREEN_WIDTH - 50, 165, 25, 25, color_white, _menuInGameTexture, IG_OPENING_CHEVRON);
        addMenuOption(">", 50, 165, 25, 25, color_white, _menuInGameTexture, IG_CLOSING_CHEVRON);
}

void initParamMenu() {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        int isFullscreenOption = -1;

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;
        mask       = loadTexture(BACKGROUND_MENU);
        _paramMenu[PARAM_BACKGROUND].texture   = mask;
        _paramMenu[PARAM_BACKGROUND].size      = maskRect;
        _paramMenu[PARAM_BACKGROUND].isVisible = 1;

        _paramMenu[PARAM_LOGO].texture   = loadTexture(LOGO);
        _paramMenu[PARAM_LOGO].size.x    = SCREEN_WIDTH / 2 - 200;
        _paramMenu[PARAM_LOGO].size.y    = 50;
        _paramMenu[PARAM_LOGO].size.w    = 400;
        _paramMenu[PARAM_LOGO].size.h    = 100;
        _paramMenu[PARAM_LOGO].isVisible = 1;

        isFullscreenOption = ((_gameParams[CONFIG_FULLSCREEN])) ? 1 : 0;

        addMenuOption("FullScreen : Desactiver", SCREEN_WIDTH / 2, 150, SCREEN_WIDTH / 2 + 10, 50, color_white, _paramMenu, PARAM_FULLSCREEN);
        addMenuOption("Reglages son", SCREEN_WIDTH / 2, 225, SCREEN_WIDTH / 2, 50, color_white, _paramMenu, PARAM_SOUND);
        addMenuOption("Quitter parametres", SCREEN_WIDTH / 2, 375, SCREEN_WIDTH / 2, 50, color_white, _paramMenu, PARAM_LEAVE);
        addMenuOption("<", SCREEN_WIDTH - 50, 165, 25, 25, color_white, _paramMenu, PARAM_OPENING_CHEVRON);
        addMenuOption(">", 50, 165, 25, 25, color_white, _paramMenu, PARAM_CLOSING_CHEVRON);

        addMenuOption("FullScreen : Activer", SCREEN_WIDTH / 2, 150, SCREEN_WIDTH / 2, 50, color_white, _paramMenu, PARAM_FULLSCREEN_PICTURE);
        _paramMenu[PARAM_FULLSCREEN_PICTURE].isVisible = isFullscreenOption;
}

void initSoundMenu() {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        char musicVolume[4] = {0x00};
        char effectsVolume[4] = {0x00};
        char mainVolume[4] = {0x00};

        int iMusicVolume = ((float)_gameParams[CONFIG_MUSIC_VOLUME]/128)*100;
        int iEffectsVolume = ((float)_gameParams[CONFIG_SOUND_EFFECTS_VOLUME]/128)*100;
        int iMainVolume = ((float)_gameParams[CONFIG_MAIN_VOLUME]/128)*100;

        sprintf(musicVolume, "%d", iMusicVolume);
        sprintf(effectsVolume, "%d", iEffectsVolume);
        sprintf(mainVolume, "%d", iMainVolume);

        int musicSliderPos = (SCREEN_WIDTH / 4) + ((float)(SCREEN_WIDTH / 2)/100)*iMusicVolume;
        int effectsSliderPos = (SCREEN_WIDTH / 4) + ((float)(SCREEN_WIDTH / 2)/100)*iEffectsVolume;
        int mainSliderPos = (SCREEN_WIDTH / 4) + ((float)(SCREEN_WIDTH / 2)/100)*iMainVolume;

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;
        mask       = loadTexture(BACKGROUND_MENU);
        _soundMenuTextures[SOUND_MENU_BACKGROUND].texture   = mask;
        _soundMenuTextures[SOUND_MENU_BACKGROUND].size      = maskRect;
        _soundMenuTextures[SOUND_MENU_BACKGROUND].isVisible = 1;

        addMenuOption("Musique : Activer", SCREEN_WIDTH / 2, 50, SCREEN_WIDTH / 2 + 10, 50, color_white, _soundMenuTextures, SOUND_MENU_MUSIC_PLAY);
        addMenuTexture(SCREEN_WIDTH / 4, 110, SCREEN_WIDTH / 2, 35, _soundMenuTextures, BAR_SLIDER_SPRITE, SOUND_MENU_MUSIC_SLIDER_BAR);
        addMenuTexture(musicSliderPos, 110, SLIDER_BALL_WIDTH, SLIDER_BALL_WIDTH, _soundMenuTextures, BALL_SLIDER_SPRITE, SOUND_MENU_MUSIC_SLIDER_BALL);
        addMenuOption(musicVolume, SCREEN_WIDTH - SCREEN_WIDTH / 6, 100, 100, 50, color_white, _soundMenuTextures, SOUND_MENU_MUSIC_VOLUME_VALUE);

        addMenuOption("Effets : Activer", SCREEN_WIDTH / 2, 150, SCREEN_WIDTH / 2 + 10, 50, color_white, _soundMenuTextures, SOUND_MENU_SOUND_EFFECTS_PLAY);
        addMenuTexture(SCREEN_WIDTH / 4, 210, SCREEN_WIDTH / 2, 35, _soundMenuTextures, BAR_SLIDER_SPRITE, SOUND_MENU_EFFECTS_SLIDER_BAR);
        addMenuTexture(effectsSliderPos, 210, SLIDER_BALL_WIDTH, SLIDER_BALL_WIDTH, _soundMenuTextures, BALL_SLIDER_SPRITE, SOUND_MENU_EFFECTS_SLIDER_BALL);
        addMenuOption(effectsVolume, SCREEN_WIDTH - SCREEN_WIDTH / 6, 200, 100,50, color_white, _soundMenuTextures, SOUND_MENU_EFFECTS_VOLUME_VALUE);

        addMenuOption("Volume principal : Activer", SCREEN_WIDTH / 2, 250, SCREEN_WIDTH / 2 + 10, 50, color_white, _soundMenuTextures, SOUND_MENU_MAIN_VOLUME_PLAY);
        addMenuTexture(SCREEN_WIDTH / 4, 310, SCREEN_WIDTH / 2, 35, _soundMenuTextures, BAR_SLIDER_SPRITE, SOUND_MENU_MAIN_VOLUME_SLIDER_BAR);
        addMenuTexture(mainSliderPos, 310, SLIDER_BALL_WIDTH, SLIDER_BALL_WIDTH, _soundMenuTextures, BALL_SLIDER_SPRITE, SOUND_MENU_MAIN_VOLUME_SLIDER_BALL);
        addMenuOption(mainVolume, SCREEN_WIDTH - SCREEN_WIDTH / 6, 300, 100, 50, color_white, _soundMenuTextures, SOUND_MENU_MAIN_VOLUME_VALUE);

        addMenuOption("Musique : Desactiver", SCREEN_WIDTH / 2, 50, SCREEN_WIDTH / 2 + 10, 50, color_white, _soundMenuTextures, SOUND_MENU_MUSIC_PLAY_PICTURE);
        addMenuOption("Effets : Desactiver", SCREEN_WIDTH / 2, 150, SCREEN_WIDTH / 2, 50, color_white, _soundMenuTextures, SOUND_MENU_SOUND_EFFECTS_PLAY_PICTURE);
        addMenuOption("Volume principal : Desactiver", SCREEN_WIDTH / 2, 250, SCREEN_WIDTH / 2, 50, color_white, _soundMenuTextures, SOUND_MENU_MAIN_VOLUME_PLAY_PICTURE);

        addMenuOption("Retour", SCREEN_WIDTH / 2, 350, SCREEN_WIDTH / 2, 50, color_white, _soundMenuTextures, SOUND_MENU_LEAVE);
        addMenuOption(">", 50, 65, 25, 25, color_white, _soundMenuTextures, SOUND_MENU_CHEVRON);

        if ( !(_gameParams[CONFIG_MUSIC_PLAY]) ) {
                _soundMenuTextures[SOUND_MENU_MUSIC_PLAY_PICTURE].isVisible = 0;
        }else{
                _soundMenuTextures[SOUND_MENU_MUSIC_PLAY].isVisible = 0;
        }

        if ( !(_gameParams[CONFIG_SOUND_EFFECTS_PLAY]) ) {
                _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY_PICTURE].isVisible = 0;
        }else{
                _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].isVisible = 0;
        }

        if ( !(_gameParams[CONFIG_MAIN_VOLUME_PLAY]) ) {
                _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY_PICTURE].isVisible = 0;
        }else{
                _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].isVisible = 0;
        }
}

void initLoadCharMenu() {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        int i = 0;

        char *text       = smalloc(sizeof(char), 30);
        char *playerName = smalloc(sizeof(char), 51);

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;

        mask = loadTexture(BACKGROUND_LOAD_CHAR_MENU);

        _loadCharMenuTextures[LOAD_CHAR_BACKGROUND].texture   = mask;
        _loadCharMenuTextures[LOAD_CHAR_BACKGROUND].size      = maskRect;
        _loadCharMenuTextures[LOAD_CHAR_BACKGROUND].isVisible = 1;

        for (i = 0; i < 51; i++) {
                playerName[i] = '\0';
        }

        if ((exists("config/save/1/.new"))) {
                strcpy(text, "New game");
        }else{
                playerName = loadPlayerName(1);
                strcpy(text, playerName);
        }
        addMenuOption(text, SCREEN_WIDTH / 2, 175, 250, 50, color_white, _loadCharMenuTextures, LOAD_CHAR_SLOT_1);


        for (i = 0; i < 51; i++) {
                playerName[i] = '\0';
        }

        if ((exists("config/save/2/.new"))) {
                strcpy(text, "New game");
        }else{
                playerName = loadPlayerName(2);
                strcpy(text, playerName);
        }
        addMenuOption(text, SCREEN_WIDTH / 2, 225, 250, 50, color_white, _loadCharMenuTextures, LOAD_CHAR_SLOT_2);

        for (i = 0; i < 51; i++) {
                playerName[i] = '\0';
        }

        if ((exists("config/save/3/.new"))) {
                strcpy(text, "New game");
        }else{
                playerName = loadPlayerName(3);
                strcpy(text, playerName);
        }
        addMenuOption(text, SCREEN_WIDTH / 2, 275, 250, 50, color_white, _loadCharMenuTextures, LOAD_CHAR_SLOT_3);

        addMenuOption("Retour", SCREEN_WIDTH / 2, 375, 300, 50, color_white, _loadCharMenuTextures, LOAD_CHAR_LEAVE);
        addMenuOption(">", 50, 200, 25, 25, color_white, _loadCharMenuTextures, LOAD_CHAR_CHEVRON);
        free(text);
        free(playerName);
}

void initGameModeMenu() {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;
        mask       = loadTexture(BACKGROUND_GAME_MODE_MENU);
        _gameModeMenuTextures[GAME_MODE_BACKGROUND].texture   = mask;
        _gameModeMenuTextures[GAME_MODE_BACKGROUND].size      = maskRect;
        _gameModeMenuTextures[GAME_MODE_BACKGROUND].isVisible = 1;

        addMenuOption("Aventure", SCREEN_WIDTH / 2, 175, 250, 50, color_white, _gameModeMenuTextures, GAME_MODE_AVENTURE);
        addMenuOption("Multijoueur", SCREEN_WIDTH / 2, 225, 250, 50, color_white, _gameModeMenuTextures, GAME_MODE_MULTIPLAYER);
        addMenuOption("Arcade", SCREEN_WIDTH / 2, 275, 250, 50, color_white, _gameModeMenuTextures, GAME_MODE_ARCADE);
        addMenuOption("Retour", SCREEN_WIDTH / 2, 375, 300, 50, color_white, _gameModeMenuTextures, GAME_MODE_LEAVE);
        addMenuOption(">", 50, 200, 25, 25, color_white, _gameModeMenuTextures, GAME_MODE_OPENING_CHEVRON);
        addMenuOption("<", SCREEN_WIDTH - 50, 200, 25, 25, color_white, _gameModeMenuTextures, GAME_MODE_CLOSING_CHEVRON);
}

void initMultiplayerMenu() {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;
        mask       = loadTexture(BACKGROUND_MULTIPLAYER_MENU);
        _multiplayerMenuTextures[MULTI_BACKGROUND].texture   = mask;
        _multiplayerMenuTextures[MULTI_BACKGROUND].size      = maskRect;
        _multiplayerMenuTextures[MULTI_BACKGROUND].isVisible = 1;

        addMenuOption("Local", SCREEN_WIDTH / 2, 175, 250, 50, color_white, _multiplayerMenuTextures, MULTI_LOCAL);
        addMenuOption("Internet", SCREEN_WIDTH / 2, 225, 250, 50, color_white, _multiplayerMenuTextures, MULTI_INTERNET);
        addMenuOption("Retour", SCREEN_WIDTH / 2, 375, 300, 50, color_white, _multiplayerMenuTextures, MULTI_LEAVE);
        addMenuOption(">", 50, 200, 25, 25, color_white, _multiplayerMenuTextures, MULTI_OPENING_CHEVRON);
        addMenuOption("<", SCREEN_WIDTH - 50, 200, 25, 25, color_white, _multiplayerMenuTextures, MULTI_CLOSING_CHEVRON);
}

void initLocalMultiModeMenu() {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;
        mask       = loadTexture(BACKGROUND_MULTIPLAYER_MENU);
        _localMultiModeMenuTextures[LOCAL_MULTI_BACKGROUND].texture   = mask;
        _localMultiModeMenuTextures[LOCAL_MULTI_BACKGROUND].size      = maskRect;
        _localMultiModeMenuTextures[LOCAL_MULTI_BACKGROUND].isVisible = 1;

        addMenuOption("Accueillir", SCREEN_WIDTH / 2, 175, 250, 50, color_white, _localMultiModeMenuTextures, MULTI_LOCAL);
        addMenuOption("Rejoindre", SCREEN_WIDTH / 2, 225, 250, 50, color_white, _localMultiModeMenuTextures, MULTI_INTERNET);
        addMenuOption("Retour", SCREEN_WIDTH / 2, 375, 300, 50, color_white, _localMultiModeMenuTextures, MULTI_LEAVE);
        addMenuOption(">", 50, 200, 25, 25, color_white, _localMultiModeMenuTextures, MULTI_OPENING_CHEVRON);
        addMenuOption("<", SCREEN_WIDTH - 50, 200, 25, 25, color_white, _localMultiModeMenuTextures, MULTI_CLOSING_CHEVRON);
}

void initConfirmBox() {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;

        mask = loadTexture(BACKGROUND_CONFIRM_BOX);
        _confirmBoxTextures[CONFIRM_BACKGROUND].texture   = mask;
        _confirmBoxTextures[CONFIRM_BACKGROUND].size      = maskRect;
        _confirmBoxTextures[CONFIRM_BACKGROUND].isVisible = 1;

        addMenuOption("Confirmer ?", SCREEN_WIDTH / 2, 100, 250, 50, color_white, _confirmBoxTextures, CONFIRM_ASK);
        addMenuOption("Oui", SCREEN_WIDTH / 2, 175, 250, 50, color_white, _confirmBoxTextures, CONFIRM_YES);
        addMenuOption("Non", SCREEN_WIDTH / 2, 225, 250, 50, color_white, _confirmBoxTextures, CONFIRM_NO);
        addMenuOption(">", 200, 200, 25, 25, color_white, _confirmBoxTextures, CONFIRM_CHEVRON);
}

void initPlayOptionsBox() {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;

        mask = loadTexture(BACKGROUND_PLAY_OPTIONS_BOX);
        _playOptionsBoxTextures[PLAY_BACKGROUND].texture   = mask;
        _playOptionsBoxTextures[PLAY_BACKGROUND].size      = maskRect;
        _playOptionsBoxTextures[PLAY_BACKGROUND].isVisible = 1;

        addMenuOption("Continuer", SCREEN_WIDTH / 2, 175, 250, 50, color_white, _playOptionsBoxTextures, PLAY_CONTINUE);
        addMenuOption("Reset", SCREEN_WIDTH / 2, 225, 250, 50, color_white, _playOptionsBoxTextures, PLAY_RESET);
        addMenuOption("Supprimer", SCREEN_WIDTH / 2, 275, 250, 50, color_white, _playOptionsBoxTextures, PLAY_DELETE);
        addMenuOption("Retour", SCREEN_WIDTH / 2, 375, 250, 50, color_white, _playOptionsBoxTextures, PLAY_LEAVE);
        addMenuOption(">", 175, 200, 25, 25, color_white, _playOptionsBoxTextures, PLAY_CHEVRON);
}

void initInputTextMenu(char ask[51]) {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;

        mask = loadTexture(BACKGROUND_INPUT_TEXT_MENU);
        _inputTextMenuTextures[INPUT_BACKGROUND].texture   = mask;
        _inputTextMenuTextures[INPUT_BACKGROUND].size      = maskRect;
        _inputTextMenuTextures[INPUT_BACKGROUND].isVisible = 1;

        addMenuOption(ask, SCREEN_WIDTH / 2, 175, 250, 50, color_white, _inputTextMenuTextures, INPUT_ASK);
        addMenuOption("", SCREEN_WIDTH / 2, 225, 250, 50, color_white, _inputTextMenuTextures, INPUT_TEXT);
        addMenuOption("Valider", SCREEN_WIDTH / 4, SCREEN_HEIGHT - 100, SCREEN_WIDTH / 4, 50, color_white, _inputTextMenuTextures, INPUT_VALIDATE);
        addMenuOption("Retour", SCREEN_WIDTH - SCREEN_WIDTH / 4, SCREEN_HEIGHT - 100, SCREEN_WIDTH / 4, 50, color_white, _inputTextMenuTextures, INPUT_LEAVE);
        addMenuOption(">", 20, SCREEN_HEIGHT - 90, 25, 25, color_white, _inputTextMenuTextures, INPUT_CHEVRON);
}

void initPlayerMenu() {
        SDL_Texture *mask;
        SDL_Rect maskRect;

        maskRect.y = 0;
        maskRect.x = 0;
        maskRect.w = SCREEN_WIDTH;
        maskRect.h = SCREEN_HEIGHT;
        mask       = loadTexture(BACKGROUND_GAME_MODE_MENU);
        _playerDeadMenu[PLAYER_BACKGROUND].texture   = mask;
        _playerDeadMenu[PLAYER_BACKGROUND].size      = maskRect;
        _playerDeadMenu[PLAYER_BACKGROUND].isVisible = 1;

        addMenuOption("Vous etes mort !", SCREEN_WIDTH / 2, 50, 400, 100, color_white, _playerDeadMenu, PLAYER_LOGO);
        addMenuOption("Recommencer", SCREEN_WIDTH / 2, 175, 250, 50, color_white, _playerDeadMenu, PLAYER_RETRY);
        addMenuOption("Revenir au menu principal", SCREEN_WIDTH / 2, 225, 250, 50, color_white, _playerDeadMenu, PLAYER_BACK_TO_MENU);
        addMenuOption("Quitter", SCREEN_WIDTH / 2, 375, 300, 50, color_white, _playerDeadMenu, PLAYER_LEAVE);
        addMenuOption(">", 50, 200, 25, 25, color_white, _playerDeadMenu, PLAYER_OPENING_CHEVRON);
        addMenuOption("<", SCREEN_WIDTH - 50, 200, 25, 25, color_white, _playerDeadMenu, PLAYER_CLOSING_CHEVRON);
}

void initDialog(int level) {
        initDialogText(level);
        initDialogSprite();
}

void initDialogSprite() {
        _textures[DIALOG_BOX] = loadTexture(DIALOG_BOX_SPRITE_PATH);
}

void initDialogText(int level) {
        FILE *fp             = NULL;
        char *charConfigPath = smalloc(sizeof(char), 100);
        char *bossConfigPath = smalloc(sizeof(char), 100);
        int len            = -1;

        charConfigPath = createLevelStr("text/", NULL, "char", level);
        bossConfigPath = createLevelStr("text/", NULL, "boss", level);

        fp = sfopen(charConfigPath, "rb");

        len = filelen(fp);

        _mainChar.speech.text = smalloc(sizeof(char), len + 1);

        fread(_mainChar.speech.text, sizeof(char), len, fp);

        _mainChar.speech.text[len] = 0x00;

        _mainChar.speech.totalParts = (len / TEXT_MAX_LETTERS_PER_DISPLAY) + 1;

        logger("Character dialog text loaded\n", LOGGER_OK);

        fclose(fp);
        fp = NULL;

        fp = sfopen(bossConfigPath, "rb");

        len = filelen(fp);

        _boss.speech.text = smalloc(sizeof(char), len + 1);

        fread(_boss.speech.text, sizeof(char), len, fp);

        _boss.speech.text[len] = 0x00;

        _boss.speech.totalParts = (len / TEXT_MAX_LETTERS_PER_DISPLAY) + 1;

        logger("Boss dialog text loaded\n", LOGGER_OK);

        _textures[DIALOG_TEXT] = createDialogText(_boss.speech.text, color_white);

        fclose(fp);
        free(charConfigPath);
        free(bossConfigPath);
}

void initKeymap(int saveId) {
        logger("Loading keymap for save ", LOGGER_INFO);
        printf("%d...\n", saveId);

        loadKeymap(&_keymap, saveId);

        // loadGamepadKeymap(&_gamepadKeymap);
        logger("Keymap fully loaded !\n", LOGGER_OK);
}

void initGameParams() {
        _gameParams[CONFIG_FULLSCREEN]           = 0;
        _gameParams[CONFIG_MUSIC_PLAY]           = 0;
        _gameParams[CONFIG_MUSIC_VOLUME]         = 64;
        _gameParams[CONFIG_SOUND_EFFECTS_PLAY]   = 0;
        _gameParams[CONFIG_SOUND_EFFECTS_VOLUME] = 64;
}
