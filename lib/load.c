#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef _WIN32
      #include <io.h>     // _access
#elif __linux__
      #include <unistd.h> // access
#endif

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include "../headers/config.h"
#include "../headers/menu.h"
#include "../headers/init.h"
#include "../headers/utility.h"
#include "../headers/load.h"
#include "../headers/game.h"
#include "../headers/linked_list.h"
#include "../headers/sdl_tools.h"

extern SDL_Renderer *_renderer;
extern Keymap _keymap;

extern MainCharacter _mainChar;
extern Boss _boss;
extern LifeBar _bossLifeBar;
extern LifeBar _lifeBarHolder;
extern Platforms *_platforms;

extern SDL_Rect _shadowRect;

extern int _blackscreenOpacity;
extern SDL_Texture *_textures[TEXTURES_TOTAL];
extern Mix_Chunk *_sounds[SOUND_TOTAL];
extern Mix_Music *_music;

extern char _name[50];

extern int _gameParams[CONFIG_TOTAL];

extern int _crtTalking;
extern int _skipFrame;
int _currentLevel;

void loadCharSpriteSheet(char *configPath) {
        int i         = 0;
        int y         = 0;
        char *assetPath = NULL;

        assetPath = malloc(sizeof(char) * 200);

        SDL_Rect bufferRect;
        FILE *   fp = NULL;

        fp = sfopen(configPath, "r");

        if (!(fp)) {
                printf("File open failed\n");
                exit(EXIT_FAILURE);
        }

        while (fscanf(fp, "%d %d %d %s", &_mainChar.frameQty[i], &_mainChar.spriteWidth[i], &_mainChar.spriteHeight[i], assetPath) != EOF) {
                _mainChar.spriteRects[i]  = malloc(sizeof(SDL_Rect) * _mainChar.frameQty[i]);
                _mainChar.spriteSheets[i] = loadTexture(assetPath);

                for (y = 0; y < _mainChar.frameQty[i]; y++) {
                        bufferRect.x = _mainChar.spriteWidth[i] * y;
                        bufferRect.y = 0;
                        bufferRect.w = _mainChar.spriteWidth[i];
                        bufferRect.h = _mainChar.spriteHeight[i];
                        _mainChar.spriteRects[i][y] = bufferRect;
                }
                i++;
        }
}

void loadBossSpriteSheet(char *configPath) {
        int i         = 0;
        int y         = 0;
        char *assetPath = NULL;

        assetPath = malloc(sizeof(char) * 200);

        SDL_Rect bufferRect;
        FILE *   fp = NULL;

        fp = fopen(configPath, "r");

        if (!(fp)) {
                printf("File open failed\n");
                exit(EXIT_FAILURE);
        }

        while (fscanf(fp, "%d %d %d %s", &_boss.frameQty[i], &_boss.spriteWidth[i], &_boss.spriteHeight[i], assetPath) != EOF) {
                _boss.spriteRects[i]  = malloc(sizeof(SDL_Rect) * _boss.frameQty[i]);
                _boss.spriteSheets[i] = loadTexture(assetPath);

                for (y = 0; y < _boss.frameQty[i]; y++) {
                        bufferRect.x            = _boss.spriteWidth[i] * y;
                        bufferRect.y            = 0;
                        bufferRect.w            = _boss.spriteWidth[i];
                        bufferRect.h            = _boss.spriteHeight[i];
                        _boss.spriteRects[i][y] = bufferRect;
                }

                i++;
        }
        free(assetPath);
        fclose(fp);
}

void loadAttackSpriteSheet(char *configPath, int loadTargetId) {
        int i         = 0;
        int y         = 0;
        char *assetPath = NULL;

        void *structBuffer = NULL;

        assetPath = smalloc(sizeof(char), 200);

        SDL_Rect bufferRect;
        FILE *   fp = NULL;

        fp = sfopen(configPath, "r");

        if (!(fp)) {
                printf("File open failed\n");
                exit(EXIT_FAILURE);
        }

        switch (loadTargetId) {
        case LOAD_CHARACTER:
                structBuffer = &_mainChar;

                break;

        case LOAD_BOSS:
                structBuffer = &_boss;

                break;
        }

        while (fscanf(fp, "%d %d %d %s", &MAINCHAR_CAST(structBuffer)->shotList->animation->frameQty[i], &MAINCHAR_CAST(structBuffer)->shotList->animation->spriteWidth[i], &MAINCHAR_CAST(structBuffer)->shotList->animation->spriteHeight[i], assetPath) != EOF) {
                MAINCHAR_CAST(structBuffer)->shotList->animation->spriteRects[i]  = malloc(sizeof(SDL_Rect) * MAINCHAR_CAST(structBuffer)->shotList->animation->frameQty[i]);
                MAINCHAR_CAST(structBuffer)->shotList->animation->spriteSheets[i] = loadTexture(assetPath);

                for (y = 0; y < MAINCHAR_CAST(structBuffer)->shotList->animation->frameQty[i]; y++) {
                        bufferRect.x = MAINCHAR_CAST(structBuffer)->shotList->animation->spriteWidth[i] * y;
                        bufferRect.y = 0;
                        bufferRect.w = MAINCHAR_CAST(structBuffer)->shotList->animation->spriteWidth[i];
                        bufferRect.h = MAINCHAR_CAST(structBuffer)->shotList->animation->spriteHeight[i];
                        MAINCHAR_CAST(structBuffer)->shotList->animation->spriteRects[i][y] = bufferRect;
                }

                i++;
        }
        fclose(fp);
        free(assetPath);
}

void loadSoundEffects(int flag, int level){
        FILE *fp = NULL;
        char *characterPath = "character/";
        char *bossPath      = "boss/";
        char *path = smalloc(sizeof(char), 100);
        int index = 0;

        switch (flag) {
        case LOAD_CHARACTER:
                fp = sfopen(createLevelStr(characterPath, NULL, "sounds", level), "r");
                break;
        case LOAD_BOSS:
                fp = sfopen(createLevelStr(bossPath, NULL, "sounds", level), "r");
                break;
        }

        while (fscanf(fp, "%d %s", &index, path) != -1) {
                _sounds[index] = Mix_LoadWAV(path);
                if (_sounds[index] == NULL) {
                        logger("Failed to load sound ", LOGGER_ERR);
                        printf("%d : %s\n", index, Mix_GetError());
                }
                Mix_VolumeChunk(_sounds[index], _gameParams[CONFIG_SOUND_EFFECTS_VOLUME]);
        }

        fclose(fp);
        free(path);

}

void loadMusic() {
        if ((_music = Mix_LoadMUS(MUSIC_PATH)) == NULL) {
                logger("Failed to load music ", LOGGER_FAIL);
                printf("(%s)\n", MUSIC_PATH);
        }

        Mix_VolumeMusic(_gameParams[CONFIG_MUSIC_VOLUME]);
}

void loadSpriteSheet(int loadTargetId, int level) {
        char *characterSpritesPath = "character/";
        char *bossSpritesPath      = "boss/";
        char *sprites      = "sprites";
        char *attacksAsset = "attacks/";

        switch (loadTargetId) {
        case LOAD_BOSS:
                loadBossSpriteSheet(createLevelStr(bossSpritesPath, NULL, sprites, level));
                // No boss attacks yet
                break;

        case LOAD_CHARACTER:

                loadCharSpriteSheet(createLevelStr(characterSpritesPath, NULL, sprites, level));
                loadAttackSpriteSheet(createLevelStr(characterSpritesPath, attacksAsset, sprites, level), LOAD_CHARACTER);
                break;
        }
}

void loadLevel(int level, int saveId) {
        if (level == -1) {
                logger("Couldn't load level\n", LOGGER_FAIL);
                exit(EXIT_FAILURE);
        }else if (saveId == -1) {
                logger("Couldn't load chosen save\n", LOGGER_FAIL);
                exit(EXIT_FAILURE);
        }

        _crtTalking = 0;
        _skipFrame  = 0;

        initDialog(level);
        initScenery(level);
        initCharacter(level, saveId);
        initBoss(level);
}

void loadGame(int saveId, ...) {
        FILE *fp    = sfopen(createSaveStr(LOAD_SAVE, saveId), "rb");
        int level = -1;

        va_list ap;

        va_start(ap, saveId);

        level = va_arg(ap, int);

        va_end(ap);

        logger("Looking for save file ", LOGGER_INFO);
        printf("%d...\n", saveId);

        fread(&level, sizeof(int), 1, fp);

        if (level == -1) {
                logger("Corrupted save file\n", LOGGER_FAIL);
                exit(EXIT_FAILURE);
        }

        logger("Save file found !\n", LOGGER_OK);

        loadLevel(level, saveId);

        initKeymap(saveId);

        initDialogText(level);

        fclose(fp);
}

void renderBackground() {
        SDL_RenderCopy(_renderer, _textures[BACKGROUND_SPRITE], NULL, NULL);
}

void loadDialogBox() {
        SDL_Rect rect = {
                0,                          // x
                SCREEN_HEIGHT - TEXT_WINDOW_HEIGHT, // y
                TEXT_WINDOW_WIDTH,          // w
                TEXT_WINDOW_HEIGHT          // h
        };

        SDL_RenderCopy(_renderer, _textures[DIALOG_BOX], NULL, &rect);
}

void loadLifeBar(LifeBar lifeBar, LifeBar lifeBarHolder, float hp, int mode) {
        switch (mode) {
        case LIFEBAR_VERTICAL:
                lifeBar.srcRect.w = 25;
                lifeBar.dstRect.w = 25;

                lifeBar.srcRect.h = (hp / 1000) * lifeBar.h;
                lifeBar.dstRect.h = (hp / 1000) * lifeBar.h;

                break;

        case LIFEBAR_HORIZONTAL:
                lifeBar.srcRect.w = (hp / 1000) * lifeBar.w;
                lifeBar.dstRect.w = (hp / 1000) * lifeBar.w;

                lifeBarHolder.srcRect.w = lifeBarHolder.w;
                lifeBarHolder.dstRect.w = lifeBarHolder.w;
                break;
        }

        renderSprite(lifeBar.sprite, &lifeBar.srcRect, &lifeBar.dstRect);

        if (mode == LIFEBAR_HORIZONTAL) {
                renderSprite(lifeBarHolder.sprite, &lifeBarHolder.srcRect, &lifeBarHolder.dstRect);
        }
}

void loadPlatforms()
{
        int i = 0;
        for(i = 0; i < _platforms[0].nbPlatforms; i++)
        {
                SDL_RenderCopy(_renderer, _platforms[i].texture, &_platforms[i].srcRect, &_platforms[i].dstRect);
        }
}

char convertFomSDLKeyCode(int keycode) {
        // Returns -1 if key is not found
        int ret = -1;

        switch (keycode) {
        case SDLK_a:
                ret = 'a';
                break;

        case SDLK_b:
                ret = 'b';
                break;

        case SDLK_c:
                ret = 'c';
                break;

        case SDLK_d:
                ret = 'd';
                break;

        case SDLK_e:
                ret = 'e';
                break;

        case SDLK_f:
                ret = 'f';
                break;

        case SDLK_g:
                ret = 'g';
                break;

        case SDLK_h:
                ret = 'h';
                break;

        case SDLK_i:
                ret = 'i';
                break;

        case SDLK_j:
                ret = 'j';
                break;

        case SDLK_k:
                ret = 'k';
                break;

        case SDLK_l:
                ret = 'l';
                break;

        case SDLK_m:
                ret = 'm';
                break;

        case SDLK_n:
                ret = 'n';
                break;

        case SDLK_o:
                ret = 'o';
                break;

        case SDLK_p:
                ret = 'p';
                break;

        case SDLK_q:
                ret = 'q';
                break;

        case SDLK_r:
                ret = 'r';
                break;

        case SDLK_s:
                ret = 's';
                break;

        case SDLK_t:
                ret = 't';
                break;

        case SDLK_u:
                ret = 'u';
                break;

        case SDLK_v:
                ret = 'v';
                break;

        case SDLK_w:
                ret = 'w';
                break;

        case SDLK_x:
                ret = 'x';
                break;

        case SDLK_y:
                ret = 'y';
                break;

        case SDLK_z:
                ret = 'z';
                break;

        case SDLK_0:
                ret = '0';
                break;

        case SDLK_1:
                ret = '1';
                break;

        case SDLK_2:
                ret = '2';
                break;

        case SDLK_3:
                ret = '3';
                break;

        case SDLK_4:
                ret = '4';
                break;

        case SDLK_5:
                ret = '5';
                break;

        case SDLK_6:
                ret = '6';
                break;

        case SDLK_7:
                ret = '7';
                break;

        case SDLK_8:
                ret = '8';
                break;

        case SDLK_9:
                ret = '9';
                break;

        case SDLK_KP_0:
                ret = '0';
                break;

        case SDLK_KP_1:
                ret = '1';
                break;

        case SDLK_KP_2:
                ret = '2';
                break;

        case SDLK_KP_3:
                ret = '3';
                break;

        case SDLK_KP_4:
                ret = '4';
                break;

        case SDLK_KP_5:
                ret = '5';
                break;

        case SDLK_KP_6:
                ret = '6';
                break;

        case SDLK_KP_7:
                ret = '7';
                break;

        case SDLK_KP_8:
                ret = '8';
                break;

        case SDLK_KP_9:
                ret = '9';
                break;

        case SDLK_PERIOD:
                ret = '.';
                break;

        case SDLK_KP_PERIOD:
                ret = '.';
                break;


        default:
                logger("Unsupported key\n", LOGGER_ERR);
                break;
        }

        return ret;
}

int convertToSDLKeyCode(char *c) {
        // Returns -1 if key is not found
        int ret = -1;

        // TODO
        // Add controler mapping

        if (strlen(c) == 1) {
                switch (c[0]) {
                case 'a':
                        ret = SDLK_a;
                        break;

                case 'b':
                        ret = SDLK_b;
                        break;

                case 'c':
                        ret = SDLK_c;
                        break;

                case 'd':
                        ret = SDLK_d;
                        break;

                case 'e':
                        ret = SDLK_e;
                        break;

                case 'f':
                        ret = SDLK_f;
                        break;

                case 'g':
                        ret = SDLK_g;
                        break;

                case 'h':
                        ret = SDLK_h;
                        break;

                case 'i':
                        ret = SDLK_i;
                        break;

                case 'j':
                        ret = SDLK_j;
                        break;

                case 'k':
                        ret = SDLK_k;
                        break;

                case 'l':
                        ret = SDLK_l;
                        break;

                case 'm':
                        ret = SDLK_m;
                        break;

                case 'n':
                        ret = SDLK_n;
                        break;

                case 'o':
                        ret = SDLK_o;
                        break;

                case 'p':
                        ret = SDLK_p;
                        break;

                case 'q':
                        ret = SDLK_q;
                        break;

                case 'r':
                        ret = SDLK_r;
                        break;

                case 's':
                        ret = SDLK_s;
                        break;

                case 't':
                        ret = SDLK_t;
                        break;

                case 'u':
                        ret = SDLK_u;
                        break;

                case 'v':
                        ret = SDLK_v;
                        break;

                case 'w':
                        ret = SDLK_w;
                        break;

                case 'x':
                        ret = SDLK_x;
                        break;

                case 'y':
                        ret = SDLK_y;
                        break;

                case 'z':
                        ret = SDLK_z;
                        break;
                }
        }else{
                // Add key_left
                if (strcmp(c, "mouse_left") == 0) {
                        ret = SDL_BUTTON_LEFT;
                }else if (strcmp(c, "mouse_right") == 0) {
                        ret = SDL_BUTTON_RIGHT;
                }else if (strcmp(c, "mouse_middle") == 0) {
                        ret = SDL_BUTTON_MIDDLE;
                }else if (strcmp(c, "key_up") == 0) {
                        ret = SDLK_UP;
                }else if (strcmp(c, "key_down") == 0) {
                        ret = SDLK_DOWN;
                }else if (strcmp(c, "key_left") == 0) {
                        ret = SDLK_LEFT;
                }else if (strcmp(c, "key_right") == 0) {
                        ret = SDLK_RIGHT;
                }else if (strcmp(c, "alt_left") == 0) {
                        ret = SDLK_LALT;
                }else if (strcmp(c, "shift_left") == 0) {
                        ret = SDLK_LSHIFT;
                }else if (strcmp(c, "ctrl_left") == 0) {
                        ret = SDLK_LCTRL;
                }else if (strcmp(c, "alt_right") == 0) {
                        ret = SDLK_RALT;
                }else if (strcmp(c, "shift_right") == 0) {
                        ret = SDLK_RSHIFT;
                }else if (strcmp(c, "ctrl_right") == 0) {
                        ret = SDLK_RCTRL;
                }else if (strcmp(c, "escape") == 0) {
                        ret = SDLK_ESCAPE;
                }else if (strcmp(c, "space") == 0) {
                        ret = SDLK_SPACE;
                }
        }

        return ret;
}

void loadKeymap(Keymap *keymap, int saveId) {
        int i          = 0;
        char *configPath = smalloc(sizeof(char), 50);

        configPath = createSaveStr(LOAD_KEYMAP, saveId);

        FILE *fp = NULL;
        fp = sfopen(configPath, "rb");

        char *type = NULL;
        char *key  = NULL;

        type = smalloc(sizeof(char *), 50);
        key  = smalloc(sizeof(char *), 35);

        for (i = 0; i < 2; i++) {
                if (i == 1) {
                        configPath = createSaveStr(LOAD_KEYMAP_CUSTOM, saveId);

                        if (!(exists(configPath))) {
                                break;
                        }

                        fclose(fp);
                        fp = NULL;
                        fp = sfopen(configPath, "rb");
                        logger("Loading custom keymap...\n", LOGGER_INFO);
                }

                while (fscanf(fp, "%s %s", type, key) != EOF) {
                        printf("%s %s\n", type, key);
                        if (strcmp(type, "up") == 0) {
                                keymap->up = convertToSDLKeyCode(key);
                        }else if (strcmp(type, "down") == 0) {
                                keymap->down = convertToSDLKeyCode(key);
                        }else if (strcmp(type, "left") == 0) {
                                keymap->left = convertToSDLKeyCode(key);
                        }else if (strcmp(type, "right") == 0) {
                                keymap->right = convertToSDLKeyCode(key);
                        }else if (strcmp(type, "shoot") == 0) {
                                keymap->shoot = convertToSDLKeyCode(key);
                        }else if (strcmp(type, "special") == 0) {
                                keymap->special = convertToSDLKeyCode(key);
                        }else if (strcmp(type, "parry") == 0) {
                                keymap->parry = convertToSDLKeyCode(key);
                        }else if (strcmp(type, "menu") == 0) {
                                keymap->menu = convertToSDLKeyCode(key);
                        }else if (strcmp(type, "dash_left") == 0) {
                                keymap->dashLeft = convertToSDLKeyCode(key);
                        }else if (strcmp(type, "dash_right") == 0) {
                                keymap->dashRight = convertToSDLKeyCode(key);
                        }else{
                                logger("Couldn't load the key ", LOGGER_ERR);
                                printf("%s for %s : Unrecognized\n", key, type);
                        }
                }
                if (i == 1) {
                        logger("Custom keymap loaded\n", LOGGER_OK);
                }
        }


        fclose(fp);
        free(key);
        free(type);
        free(configPath);
}

void loadCharPassive(int saveId) {
        char *configFilePath = smalloc(sizeof(char), 50);

        configFilePath = createSaveStr(LOAD_SAVE, saveId);

        FILE *fp = NULL;
        fp = sfopen(configFilePath, "rb");

        fseek(fp, sizeof(int) + sizeof(Specs), SEEK_SET);

        fread(&_mainChar.passive, sizeof(Passive), 1, fp);

        fclose(fp);
        free(configFilePath);
}

void loadCharSpecs(int saveId) {
        char *configFilePath = smalloc(sizeof(char), 50);

        configFilePath = createSaveStr(LOAD_SAVE, saveId);

        FILE *fp = NULL;
        fp = sfopen(configFilePath, "rb");

        fseek(fp, sizeof(int), SEEK_SET);

        fread(&_mainChar.specs, sizeof(Specs), 1, fp);

        fclose(fp);
        free(configFilePath);
}

char *loadPlayerName(int saveId) {
        char *configFilePath = smalloc(sizeof(char), 50);

        configFilePath = createSaveStr(LOAD_SAVE, saveId);

        FILE *fp = NULL;
        fp = sfopen(configFilePath, "rb");

        char *playerName = smalloc(sizeof(char), 51);
        int len        = 0;

        fseek(fp, sizeof(int) + sizeof(Specs) + sizeof(Passive), SEEK_SET);

        logger("Fetching player name...\n", LOGGER_INFO);

        len = fread(playerName, sizeof(char), 50, fp);

        playerName[len] = '\0';

        logger("Player name found : ", LOGGER_OK);
        printf("%s\n", playerName);

        fclose(fp);
        free(configFilePath);

        return playerName;
}

void loadGameParams() {
        int i  = 0;
        FILE *fp = NULL;

        char *buffer = smalloc(sizeof(char), 100);

        fp = sfopen(PARAM_FILE_PATH, "r");

        for (i = 0; i < CONFIG_TOTAL; i++) {
                if ( !(fscanf(fp, "%s %d", buffer, &_gameParams[i]) )) {
                        logger("Failed to fetch param value ", LOGGER_ERR);
                        printf("%d\n", i);
                }
        }

        free(buffer);
}
void loadBlackScreen() {
        _textures[BLACKSCREEN] = loadTexture(BLACKSCREEN_PATH);

        SDL_SetTextureBlendMode(_textures[BLACKSCREEN],SDL_BLENDMODE_BLEND);

        SDL_SetTextureAlphaMod(_textures[BLACKSCREEN], _blackscreenOpacity);

        logger("Blackscreen alpha set ", LOGGER_INFO);
        printf("(%d)\n", _blackscreenOpacity);
}
