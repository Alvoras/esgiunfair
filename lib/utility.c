#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#ifdef _WIN32
    #include <windows.h> // sleep
    #include <io.h>      // _access
#elif __linux__
    #include <time.h>  // access
    #include <unistd.h>  // access
    #include <signal.h>  // Signal handling

int userInterruption = 0;
#endif

#include "../headers/utility.h"

extern int _ENV;

void crossSleep(int sec, unsigned long millisec) {
    struct timespec timeout = {
            sec, // seconds
            millisec  // nanoseconds (1/1 000 000)
    };
    struct timespec timeout2;

        #ifdef _WIN32
            sec *= 1000;     // Windows is using ms
            sec += millisec;
            Sleep(sec);
        #elif __linux__
            millisec *= 10000; // Using nanoseconds from milliseconds
            timeout.tv_nsec = millisec;
            nanosleep(&timeout, &timeout2);
        #endif

}

int exists(char *path) {
    int ret = 0;

    logger("Testing path... ", LOGGER_INFO);
    printf("(%s)\n", path);

#ifdef _WIN32
    if (_access(path, 00) != -1) {
#elif __linux__
    if (access(path, F_OK) != -1) {
#endif
        // Custom keymap not found
        ret = 1;
    }

    ((ret)) ? logger("Found !\n", LOGGER_OK) : logger("Not found !\n", LOGGER_INFO);

    return ret;
}

void crossUnlink(char *path) {
    logger("Removing ", LOGGER_INFO);
    printf("%s\n", path);

#ifdef _WIN32
    _unlink(path);
#elif __linux__
    unlink(path);
#endif
}

void dbg() {
    printf(">> debug <<\n");
}

void logger(char *str, int mode, ...) {
    if (_ENV != ENV_DEV) {
        return;
    }

    int  optionalMode   = -1;
    char serverPrefix[] = "(server) ";
    char clientPrefix[] = "(client) ";

    va_list ap;

    va_start(ap, mode);

    optionalMode = va_arg(ap, int);

    va_end(ap);

    char prefix[30] = { 0x00 };

    switch (mode) {
    case LOGGER_INFO:
        strcpy(prefix, "[\033[33;1mLOG\033[0m] ");
        break;

    case LOGGER_ERR:
        strcpy(prefix, "[\033[43;1mERR\033[0m] ");
        break;

    case LOGGER_OK:
        strcpy(prefix, "[\033[32;1mOK\033[0m] ");
        break;

    case LOGGER_FAIL:
        strcpy(prefix, "[\033[41;1mFAIL\033[0m] ");
        break;
    }

    if (optionalMode != -1) {
        switch (optionalMode) {
        case LOGGER_SRV:
            strcat(prefix, serverPrefix);
            break;

        case LOGGER_CLT:
            strcat(prefix, clientPrefix);
            break;
        }
    }

    strcat(prefix, str);

    printf("%s", prefix);
}

unsigned long filelen(FILE *fp) {
    logger("Fetching file length...\n", LOGGER_INFO);
    fseek(fp, 0, SEEK_END);

    int len = ftell(fp);

    rewind(fp);

    logger("Length : ", LOGGER_OK);
    printf("%d bytes\n", len);

    return len;
}

void *smalloc(int size, int qty) {
    void *ret = NULL;

    if ((ret = malloc(size * qty)) == NULL) {
        logger("Malloc failed ", LOGGER_FAIL);
        printf("(%d*%d)\n", size, qty);
        exit(EXIT_FAILURE);
    }

    return ret;
}

void *srealloc(void *ptr, int size) {
    void *ret = NULL;

    if ((ret = realloc(ptr, size)) == NULL) {
        logger("Realloc failed ", LOGGER_FAIL);
        printf("(%d)\n", size);
        exit(EXIT_FAILURE);
    }

    return ret;
}

FILE *sfopen(char *path, char *mode) {
    FILE *ret = NULL;

    if ((ret = fopen(path, mode)) == NULL) {
        if (strstr(mode, "w") != NULL) {
            logger("Failed to open the file ", LOGGER_ERR);
            printf("%s\n", path);
        }else{
            logger("Failed to read the file ", LOGGER_FAIL);
            printf("%s\n", path);
            exit(EXIT_FAILURE);
        }
    }

    return ret;
}

void *push(void *array, void *element, int mode) {
    void *tmp = NULL;

    return tmp;
}

void popStr(char *str, int pos, int flush) {
    int i = 0;

    if (pos < 0) {
        return;
    }

    str[pos] = 0x00;

    // Flush : set all the remaining chars to \0
    if ((flush)) {
        for (i = pos; i < strlen(str); i++) {
            str[i] = 0x00;
        }
    }
}

void appendStr(char *path, char *str) {
    FILE *fp = sfopen(path, "ab");

    fwrite(str, sizeof(char), strlen(str), fp);

    fclose(fp);
}

float getPercentFromVal(float pos, float maxVal) {
    return pos / (maxVal / 100);
}

float getValFromPercent(float percent, float maxVal) {
    return maxVal / (100 / percent);
}

// Variadic option is used to update extern counter
char **splitStr(char *str, char *delimiter, ...) {
    va_list ap;

    char * token      = NULL;
    char **tokenArray = NULL;
    int    count      = 0;

    token         = strtok(str, delimiter); // Get the first token
    tokenArray    = smalloc(sizeof(char **), 1);
    tokenArray[0] = 0x00;

    if (!token) {
        return tokenArray;
    }

    while (token != NULL) { // While valid tokens are returned
        tokenArray[count] = strdup(token);
        count++;
        tokenArray = srealloc(tokenArray, sizeof(char **) * (count + 1));
        token      = strtok(NULL, delimiter); // Get the next token
      }

    tokenArray[count] = 0x00;

    va_start(ap, delimiter);

    *va_arg(ap, int *) = count;

     va_end(ap);


    return tokenArray;
}

char *joinStr(char **str, char *delimiter) {
    char *joinedStr = NULL;
    int   i         = 1;

    joinedStr = smalloc(sizeof(char), strlen(str[0]) + 1);

    strcpy(joinedStr, str[0]);

    if (str[0] == NULL) {
        return joinedStr;
    }

    while (str[i] != NULL) {
        printf("%p\n", str[i]);
        joinedStr = srealloc(joinedStr, strlen(joinedStr) + strlen(str[i]) + strlen(delimiter) + 1);
        strcat(joinedStr, delimiter);
        strcat(joinedStr, str[i]);
        i++;
    }

    joinedStr[strlen(joinedStr)] = 0x00;

    return joinedStr;
}

char **splitlines(char *text) {
    return splitStr(text, "\n");
}

char *keyValJoin(char **array) {
    int    i             = 0;
    int    j             = 0;
    char **bufferArray   = NULL;
    char   bufferStr[50] = { 0x00 };
    char * text          = NULL;

    char bufferAtoi[4] = { 0x00 };

    long arrayLength = 0;
    long totalLength = 0;

    while (array[arrayLength] != NULL) {
        printf("%ld\n", arrayLength);
        arrayLength++;
    }

    printf("arraylength %ld\n", arrayLength);

    bufferArray = smalloc(sizeof(char *), (arrayLength / 2));
    for (i = 0; i < arrayLength / 2; i++) {
        bufferArray[i] = smalloc(sizeof(char), 50);
    }

    i = 0;

    while (array[i] != NULL && array[i + 1] != NULL) {
        printf("array[%d] (%p)\n", i, array[i]);
        strcpy(bufferStr, array[i]);
        strcat(bufferStr, " ");

        if ((atoi(array[i + 1]))) {
            sprintf(bufferAtoi, "%d", atoi(array[i + 1]));
        }else{
            strcpy(bufferAtoi, array[i + 1]);
        }

        strcat(bufferStr, bufferAtoi);

        printf("bufferArray[%d] = %s (%p)\n", j, bufferStr, bufferArray[j]);
        strcpy(bufferArray[j], bufferStr);
        totalLength += strlen(bufferStr);

        j++;
        i += 2;
    }

    bufferArray[j] = NULL;

    text = smalloc(sizeof(char), totalLength);

    text = joinStr(bufferArray, "\n");

    printf("joinStr returns : %s\n", text);

    return text;
}

#ifdef __linux__

void signalHandler(int signo) {
    userInterruption = 1;
}

#endif
