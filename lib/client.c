#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../headers/utility.h"
#include "../headers/config.h"
#include "../headers/socket.h"
#include "../headers/client.h"
#include "../headers/menu.h"

#include <fcntl.h>

extern int tCloseClient;
extern ClientSocketInfo tClientSocketInfo;
extern int   tNewGameEvent;
extern int   tNewServerEvent;
extern int   tEvent;
extern char *tSerialDataToServer;
extern char *tSerialDataFromServer;
extern int   _playingChar;
extern char tGameInfo;

void initClient() {
    #ifdef WIN32
    WSADATA wsa;
    int     err = WSAStartup(MAKEWORD(2, 2), &wsa);
    if (err < 0) {
        logger("WSAStartup failed !", LOGGER_FAIL, LOGGER_CLT);
        exit(EXIT_FAILURE);
    }
    #endif
}

void clientCleanUp() {
#ifdef WIN32
    WSACleanup();
#endif
}

void initSocketInfo(char *ip) {
    tClientSocketInfo.address = smalloc(sizeof(char), 16);
    strcpy(tClientSocketInfo.address, ip);
    // Ready to handle custom names if we want to expand the multiplayer to more than 2 players
    tClientSocketInfo.name = "a";
}

void *clientLoop(void *threadArgs) {
    ClientSocketInfo *args = (ClientSocketInfo *)threadArgs;

    int offset = 0;

    struct timeval timeout;

    timeout.tv_sec = 0;
    // Sampling frequency should be al least two times higher than event frequecy (shanon-nyquist)
    timeout.tv_usec = FRAME_DELAY / 2;

    char *address = args->address;
    char *name    = args->name;

    SOCKET socket = initClientConnection(address);
    char   buffer[BUF_SIZE] = {0x00};

    fd_set fdWatch;

    // Send our first message. The first byte is the game level the players are going to play on and then follows the name
    // For now it is not used by the server
    if ( (_playingChar) ) {
        printf("tGameInfo %d\n", tGameInfo);
        buffer[0] = tGameInfo;
        printf("buffer[0] %d\n", buffer[0]);
        offset += sizeof(char);
    }

    memcpy(buffer+offset, name, strlen(name));
    strcat(buffer, "\r\n");

    for (size_t i = 0; i < strlen(buffer); i++) {
        printf("sending %d\n", buffer[i]);
    }

    writeServer(socket, buffer);

    buffer[0] = 0x00;

    while (!(tCloseClient)) {
        FD_ZERO(&fdWatch);

        // Add the socket
        FD_SET(socket, &fdWatch);

        if (select(socket + 1, &fdWatch, NULL, NULL, &timeout) == -1) {
            perror("select()");
            exit(errno);
        }

        if ((tNewGameEvent)) {
            printf("New game event detected by client\n");
            strcat(tSerialDataToServer, "\r\n");
            writeServer(socket, tSerialDataToServer);
            tNewGameEvent = 0;
        }

        // Data received from the server
        if (FD_ISSET(socket, &fdWatch)) {
            printf("reading server input\n");
            int n = readServer(socket, buffer);

            // Server down
            if (n == 0) {
                logger("Server disconnected", LOGGER_ERR, LOGGER_CLT);
                break;
            }

            printf("Data received from server\n");

            if (_playingChar) {
                tNewServerEvent = 1;
                memcpy(tSerialDataToServer, buffer, (sizeof(int) * 2) + (sizeof(char)));
            }else{
                if (tGameInfo == -1) {
                    tGameInfo = buffer[0];
                    printf("Received game info : %d\n", tGameInfo);
                }else{
                    tNewServerEvent = 1;
                    memcpy(tSerialDataToServer, buffer, (sizeof(int) * 2) + (sizeof(char)));
                }
            }
        }
    }

    endClientConnection(socket);

    return NULL;
}

int initClientConnection(char *address) {
    SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
    //fcntl(sock, F_SETFL, O_NONBLOCK);
    SOCKADDR_IN     initClientAddr = { 0 };
    struct hostent *hostinfo;

    if (sock == INVALID_SOCKET) {
        perror("socket()");
        exit(errno);
    }

    hostinfo = gethostbyname(address);
    if (hostinfo == NULL) {
        logger("Unknown host ", LOGGER_FAIL, LOGGER_CLT);
        printf("%s\n", address);
        exit(EXIT_FAILURE);
    }

    initClientAddr.sin_addr   = *(IN_ADDR *)hostinfo->h_addr;
    initClientAddr.sin_port   = htons(PORT);
    initClientAddr.sin_family = AF_INET;

    if (connect(sock, (SOCKADDR *)&initClientAddr, sizeof(SOCKADDR)) == SOCKET_ERROR) {
        perror("connect()");
        exit(errno);
    }

    return sock;
}

void endClientConnection(SOCKET socket) {
    free(tClientSocketInfo.address);
    closesocket(socket);
}

int readServer(SOCKET socket, char *buffer) {
    int n = 0;

    if ((n = recv(socket, buffer, BUF_SIZE - 1, 0)) < 0) {
        perror("recv()");
        exit(errno);
    }

    buffer[n] = 0;

    return n;
}

void writeServer(SOCKET socket, char *buffer) {
    if (send(socket, buffer, strlen(buffer), 0) < 0) {
        perror("send()");
        exit(errno);
    }
    printf("Data sent to server\n");
}

void *clientGameSocket(void *threadArgs) {
    initClient();

    clientLoop(threadArgs);

    clientCleanUp();

    return NULL;
}
