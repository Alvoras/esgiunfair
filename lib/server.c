#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#ifdef _WIN32
  #include <ws2tcpip.h>
#endif

#include <SDL2/SDL.h>

#include "../headers/utility.h"
#include "../headers/config.h"
#include "../headers/socket.h"
#include "../headers/server.h"

#include "fcntl.h"

extern int tCloseServer;
extern int tWaitingForPlayer2;

void initServer() {
#ifdef WIN32
    WSADATA wsa;
    int     err = WSAStartup(MAKEWORD(2, 2), &wsa);
    if (err < 0) {
        logger("WSAStartup failed !", LOGGER_FAIL, LOGGER_SRV);
        exit(EXIT_FAILURE);
    }
#endif
}

void serverCleanUp() {
#ifdef WIN32
    WSACleanup();
#endif
}

void serverLoop() {
    int    i      = 0;
    int cnt = 0;
    SOCKET socket = initServerConnection();

    char   buffer[BUF_SIZE];
    int    crtSocketQty = 0;
    int    max          = socket;

    char gameInfo = -1;
    int offset = 0;

    Client clients[MAX_CLIENTS];

    struct timeval timeout;

    timeout.tv_sec = 0;
    // Sampling frequency should be al least two times higher than event frequecy (shanon-nyquist)
    timeout.tv_usec = FRAME_DELAY/2;

    // File descriptor set that we are monitoring
    fd_set fdWatch;

    /*FILE *servPipe   = fopen("./servSharedMem", "wb");
     * int servPipeFd = fileno(servPipe);*/


    while (!(tCloseServer)) {
        FD_ZERO(&fdWatch);

        // We add the connection socket to the set
        FD_SET(socket, &fdWatch);

        // We add every connected clients' file descriptors to the watching list
        for (i = 0; i < crtSocketQty; i++) {
            FD_SET(clients[i].socket, &fdWatch);
        }

        if (select(max + 1, &fdWatch, NULL, NULL, &timeout) == -1) {
            perror("select()");
            exit(errno);
        }

        // If there is a new client
        if (FD_ISSET(socket, &fdWatch)) {
            SOCKADDR_IN newClientAddr     = { 0 };
            socklen_t   newClientAddrSize = sizeof(newClientAddr);
            int         newClientSocket   = accept(socket, (SOCKADDR *)&newClientAddr, &newClientAddrSize);
            printf("accepted\n");

            offset = 0;

            if (newClientSocket == SOCKET_ERROR) {
                perror("accept");
                continue;
            }

            // Fetch the client's name
            if (readClient(newClientSocket, buffer) == -1) {
                logger("Client disconnected\n", LOGGER_ERR, LOGGER_SRV);
                // Disconnected
                continue;
            }
            printf("client read\n");

            // Adjust the fd count
            max = (newClientSocket > max) ? newClientSocket : max;

            // Add the new client to the watching list
            FD_SET(newClientSocket, &fdWatch);

            Client c = { newClientSocket };


              for (size_t i = 0; i < strlen(buffer); i++) {
                  printf("Received from player 2 :  %d\n", buffer[i]);
              }

              if ( gameInfo == -1 ) {
                gameInfo = buffer[0];
                offset += sizeof(char);
                printf("GAME INFO RECEIVED FROM CLIENT %d : %d\n", crtSocketQty, gameInfo);
              }

              // Fetch the name and offset the gameinfo if needed
              strncpy(c.name, buffer+offset, BUF_SIZE - 1);
              clients[crtSocketQty] = c;
              crtSocketQty++;

              printf("client added\n");

            // If counter is strictly greater than player quantity-1
            if (cnt > 0) {
                // Signal to main process that the second player has connected
                if ( (tWaitingForPlayer2) ) {
                  tWaitingForPlayer2 = 0;
                }
                logger("Player 2 connected !\n", LOGGER_OK, LOGGER_SRV);

                // Send game info to latest client connected
                writeClient(clients[crtSocketQty-1].socket, &gameInfo);
                logger("Game info sent ! ", LOGGER_OK, LOGGER_SRV);
                printf("(level = %d)\n", gameInfo);
            }
            logger("New player connected !\n", LOGGER_OK, LOGGER_SRV);
            cnt++;
        }else{
            for (i = 0; i < crtSocketQty; i++) {
                //printf("Checking socket %d input\n", i);
                // If we received data from client
                if (FD_ISSET(clients[i].socket, &fdWatch)) {
                    printf("Server received data from client %d\n", i);
                    Client client = clients[i];

                    int c = readClient(clients[i].socket, buffer);

                    printf("c = %d READ\n", c);

                    // If the client is disconnected
                    if (c == 0) {
                        closesocket(clients[i].socket);
                        removeClient(clients, i, &crtSocketQty);
                        strncpy(buffer, client.name, BUF_SIZE - 1);
                        strncat(buffer, " disconnected", BUF_SIZE - strlen(buffer) - 1);
                    }else{
                        // Send the data to all the clients but the sender
                        logger("Data sent by client ", LOGGER_INFO, LOGGER_SRV);
                        printf("%d broadcasted\n", i);

                        broadcastMessage(clients, clients[i], crtSocketQty, buffer);

                        /*intBuffer = atoi(buffer);
                         * write(servPipeFd, &intBuffer, sizeof(int));*/
                        //write(servPipeFd, &buffer, sizeof(SDL_Event));
                        //logger("Written by serverLoop", LOGGER_INFO);
                        // logger("Written by serverLoop : ", LOGGER_INFO);
                        // printf("%d\n", intBuffer);
                    }
                    break;
                }
            }
        }
    }

    clearClients(clients, crtSocketQty);
    endServerConnection(socket);
}

void clearClients(Client *clients, int crtSocketQty) {
    int i = 0;

    // close all the sockets
    for (i = 0; i < crtSocketQty; i++) {
        closesocket(clients[i].socket);
    }
}

void removeClient(Client *clients, int pos, int *crtSocketQty) {
    Client *clientToRemoveAddr = clients + pos;

    memmove(clientToRemoveAddr, clientToRemoveAddr + 1, sizeof(Client) * (*crtSocketQty - pos - 1));

    (*crtSocketQty)--;
}

void broadcastMessage(Client *clients, Client sender, int crtSocketQty, char *buffer) {
    int  i = 0;
    char message[BUF_SIZE];

    message[0] = 0x00;

    for (i = 0; i < crtSocketQty; i++) {
        // Send message to every clients but the sender
        if (sender.socket != clients[i].socket) {
            strncat(message, buffer, sizeof(message) - strlen(message) - 1);
            writeClient(clients[i].socket, message);
        }
    }
}

int initServerConnection() {
    SOCKET      sock           = socket(AF_INET, SOCK_STREAM, 0);
    SOCKADDR_IN initClientAddr = { 0 };

    if (sock == INVALID_SOCKET) {
        perror("socket()");
        exit(errno);
    }

    initClientAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    initClientAddr.sin_port        = htons(PORT);
    initClientAddr.sin_family      = AF_INET;

    if (bind(sock, (SOCKADDR *)&initClientAddr, sizeof(initClientAddr)) == SOCKET_ERROR) {
        perror("bind()");
        exit(errno);
    }

    if (listen(sock, MAX_CLIENTS) == SOCKET_ERROR) {
        perror("listen()");
        exit(errno);
    }

    return sock;
}

void endServerConnection(SOCKET socket) {
    closesocket(socket);
}

int readClient(SOCKET socket, char *buffer) {
    int n = 0;

    if ((n = recv(socket, buffer, BUF_SIZE - 1, 0)) < 0) {
        perror("recv()");
        // If we get an error, we return 0 to disconnect the client
        n = 0;
    }

    // Add null terminator
    buffer[n] = 0x00;

    return n;
}

void writeClient(SOCKET socket, char *buffer) {
    if (send(socket, buffer, strlen(buffer), 0) < 0) {
        perror("send()");
        exit(errno);
    }
}

void *gameServer() {
    initServer();

    serverLoop();

    serverCleanUp();

    return NULL;
}
