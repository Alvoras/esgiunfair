#include <stdlib.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include "../headers/config.h"
#include "../headers/utility.h"
#include "../headers/sdl_tools.h"
#include "../headers/linked_list.h"
#include "../headers/game.h"
#include "../headers/menu.h"
#include "../headers/character.h"
#include "../headers/boss.h"
#include "../headers/ability.h"

extern int _mouse[MOUSE_TOTAL];

// extern : this var comes from another file
extern MainCharacter _mainChar;
extern Boss _boss;
extern LifeBar _bossLifeBar;
extern Platforms *_platforms;
extern int _laserChannel;

void startJump() {
        _mainChar.velY      = -10;
        _mainChar.isJumping = 1;
        _mainChar.isFalling = 0;
        playSound(SOUND_CHAR_JUMP);
}

void endJump() {
        if (_mainChar.velY < -6) {
                _mainChar.velY      = -6;
                _mainChar.isJumping = 0;
                _mainChar.isFalling = 1;
        }
}

void moveLeft() {
        _mainChar.velX = -5;
}

void moveRight() {
        _mainChar.velX = 5;
}

void endMoveXAxis() {
        _mainChar.velX = 0;
}

void dash(int teleportation, int mode){

        playSound(SOUND_CHAR_DASH);

        _mainChar.animationLock = 1;
        _mainChar.isDashing = 1;

        if ( (teleportation) ) {
                switch (mode) {
                case DASH_LEFT:
                        _mainChar.dstRect.x += _mainChar.dstRect.w*2;
                        break;
                case DASH_RIGHT:
                        _mainChar.dstRect.x -= _mainChar.dstRect.w*2;
                        break;
                }
        }else{
                switch (mode) {
                case DASH_LEFT:
                        _mainChar.velX = 12;
                        _mainChar.state = CHAR_DASH_LEFT;
                        break;
                case DASH_RIGHT:
                        _mainChar.velX = -12;
                        _mainChar.state = CHAR_DASH_RIGHT;
                        break;
                }
        }

        _mainChar.frame = 0;
}
void updateCharges() {
        if (_mainChar.charges >= MAX_CHARGES) {
                return;
        }

        if (SDL_GetTicks() - _mainChar.chargesTimer > CHAR_CHARGES_DELAY) {
                _mainChar.charges++;
                _mainChar.chargesTimer = SDL_GetTicks();
        }
}

void updateCharPhysics()
{
        int onPlatform = 0; //Bool if char is on platforms
        int i = 0;

        if (_mainChar.animationLock == 1) {
                updateAnimation(&_mainChar.timer, &_mainChar.frame, CHAR_SPECIAL_ANIM_DELAY);

                // TODO
                // ADD COOLDOWN
                if ( (_mainChar.isDashing) ) {
                        if ( !(_mainChar.passive.hasTeleportation) ) {
                                //_mainChar.velX      += _mainChar.gravityX;
                                _mainChar.dstRect.x += _mainChar.velX;
                        }
                }
                // Finish animation
                if (_mainChar.frame >= _mainChar.frameQty[_mainChar.state]) {
                        if (_mainChar.state == CHAR_SPECIAL) {
                                stopSound(_laserChannel);
                        }
                        _mainChar.animationLock = 0;

                        if (_mainChar.isDashing == 1) {
                                _mainChar.velX = 0;
                                _mainChar.isDashing = 0;
                                _mainChar.dashTimer = SDL_GetTicks();
                        }
                }
                return;
        }

        //Update Special Effects
        if(_mainChar.specs.isInvulnerable)
        {
                if(SDL_GetTicks() - _mainChar.specs.timerInvulnerable > 1000) _mainChar.specs.isInvulnerable = 0;
                if(SDL_GetTicks() - _mainChar.specs.timerFrameInvulnerable > _mainChar.specs.cooldownFrameInvulnerable)
                {
                        _mainChar.specs.timerFrameInvulnerable = SDL_GetTicks();
                        if(_mainChar.specs.frameInvulnerable == 0) _mainChar.specs.frameInvulnerable = 1;
                        else _mainChar.specs.frameInvulnerable = 0;
                }
        }
        if(_mainChar.specs.isParalized)
        {
                if(SDL_GetTicks() - _mainChar.specs.timerParalized > 2000) _mainChar.specs.isParalized = 0;
        }
        else if(_mainChar.specs.isSlowed)
        {
                if(SDL_GetTicks() - _mainChar.specs.timerSlowed > 5000) _mainChar.specs.isSlowed = 0;
                else
                {
                        _mainChar.velX /= 2;
                        _mainChar.velY /= 2;
                }
        }
        if(_mainChar.specs.isPoisoned)
        {
                if(_mainChar.hp > 1) _mainChar.hp -= 1;
                if(SDL_GetTicks() - _mainChar.specs.timerPoisoned > 1000) _mainChar.specs.isPoisoned = 0;
        }
        if(_mainChar.specs.isWeakened)
        {
                if(SDL_GetTicks() - _mainChar.specs.timerWeakened > 5000) _mainChar.specs.isWeakened = 0;
        }

        // Update sprite's position
        if(!_mainChar.specs.isParalized)
        {
                if(_mainChar.isFalling) _mainChar.velY += _mainChar.gravityY;
                _mainChar.dstRect.y += _mainChar.velY;
                _mainChar.dstRect.x += _mainChar.velX;
        }

        // Update charges
        updateCharges();

        // While idle
        if (!(_mainChar.direction[DIRECTION_UP]) && !(_mainChar.direction[DIRECTION_DOWN]) && !(_mainChar.direction[DIRECTION_LEFT]) && !(_mainChar.direction[DIRECTION_RIGHT])) {
                _mainChar.state = CHAR_IDLE;
                _mainChar.state_blank = CHAR_IDLE_BLANK;
        }

        // While char is going left
        if ((_mainChar.direction[DIRECTION_LEFT])) {
                _mainChar.state      = CHAR_RUN_LEFT;
                _mainChar.state_blank= CHAR_RUN_LEFT_BLANK;
                moveLeft();
        }

        // While char is going right
        if ((_mainChar.direction[DIRECTION_RIGHT])) {
                _mainChar.state      = CHAR_RUN_RIGHT;
                _mainChar.state_blank= CHAR_RUN_RIGHT_BLANK;
                moveRight();
        }

        // JUMP
        if ((_mainChar.direction[DIRECTION_UP]) && _mainChar.currentJump != _mainChar.maxJumps) {
                _mainChar.state = CHAR_JUMP;
                _mainChar.state_blank = CHAR_JUMP_BLANK;
                startJump();
        }

        if (_mainChar.isJumping && !(_mainChar.isFalling)) {
                if (_mainChar.initialJumpPos - _mainChar.dstRect.y >= JUMP_HEIGHT) {
                        _mainChar.velY      = 0;
                        _mainChar.isFalling = 1;
                        _mainChar.isJumping = 0;
                        _mainChar.currentJump++;
                        _mainChar.direction[DIRECTION_UP] = 0;
                }
        }

        //Test on platforms
        for(i = 0; i < _platforms[0].nbPlatforms; i++)
        {
                if((_mainChar.dstRect.x + _mainChar.dstRect.w/2) >= _platforms[i].dstRect.x && (_mainChar.dstRect.x + _mainChar.dstRect.w/2) <= (_platforms[i].dstRect.x + _platforms[i].dstRect.w))
                {
                        if((_mainChar.dstRect.y + _mainChar.dstRect.h) >= _platforms[i].dstRect.y - 5 && (_mainChar.dstRect.y + _mainChar.dstRect.h) <= _platforms[i].dstRect.y + 5)
                        {
                                _mainChar.currentJump = 0;
                                _mainChar.isJumping   = 0;
                                _mainChar.isFalling   = 0;
                                _mainChar.velY = 0;
                                onPlatform = 1;
                        }
                }
                updateAnimation(&_mainChar.timer, &_mainChar.frame, CHAR_ANIM_DELAY);
        }
        if(!onPlatform && (_mainChar.dstRect.y < SCREEN_HEIGHT - (CHAR_H + FLOOR_PADDING))) _mainChar.isFalling = 1;
        // Keydown pressed
        if ((_mainChar.direction[DIRECTION_DOWN])) {
                if(onPlatform)
                {
                        _mainChar.isFalling = 1;
                }
                else
                {
                        _mainChar.state = CHAR_BOW;
                }
        }

        // Update char state while falling
        if ((_mainChar.isFalling)) {
                _mainChar.state = CHAR_FALL;
                _mainChar.frame = 0;

                if (_mainChar.dstRect.y >= SCREEN_HEIGHT - (CHAR_H + FLOOR_PADDING)) {
                        _mainChar.currentJump = 0;
                        _mainChar.isJumping   = 0;
                        _mainChar.isFalling   = 0;
                        _mainChar.initialJumpPos = SCREEN_HEIGHT - (CHAR_H + FLOOR_PADDING);
                }
        }
        updateAnimation(&_mainChar.timer, &_mainChar.frame, CHAR_ANIM_DELAY);

        // Reset frame counter
        if (_mainChar.frame >= _mainChar.frameQty[_mainChar.state]-1) {
                _mainChar.frame = 0;
        }
}

void mainCharAction() {
        if ((_mouse[MOUSE_LEFT])) {
                addShot(CHAR_SHOT_LIST);
                playSound(SOUND_CHAR_SHOOT);
        }else if(_mouse[MOUSE_MIDDLE]) {
                if(_mainChar.state != CHAR_SPECIAL)
                {
                        if (_mainChar.charges > 0) {
                                _mainChar.specialAttack->specialAbility(NULL);
                                playSound(SOUND_CHAR_SPECIAL);
                        }
                }
        }else if (_mouse[MOUSE_RIGHT]) {
                printf("parry\n");
        }
}

int isPlayerHit(SDL_Rect rect)
{
        int res = 0;

        CalculCoord codeA, codeB;
        if(_mainChar.state == CHAR_BOW)
        {
                codeA = calculCoord(_mainChar.dstRect.x, _mainChar.dstRect.y + _mainChar.dstRect.h/2, rect.x, rect.y, (rect.x + rect.w), (rect.y + rect.h));
                codeB = calculCoord((_mainChar.dstRect.x + _mainChar.dstRect.w), (_mainChar.dstRect.y + _mainChar.dstRect.h), rect.x, rect.y, (rect.x + rect.w), (rect.y + rect.h));
        }
        else
        {
                codeA = calculCoord(_mainChar.dstRect.x, _mainChar.dstRect.y, rect.x, rect.y, (rect.x + rect.w), (rect.y + rect.h));
                codeB = calculCoord((_mainChar.dstRect.x + _mainChar.dstRect.w), (_mainChar.dstRect.y + _mainChar.dstRect.h), rect.x, rect.y, (rect.x + rect.w), (rect.y + rect.h));
        }

        if(_mainChar.state != CHAR_SPECIAL)
        {
                if((codeA.sum == 0 && codeB.sum == 0) || (calculCoordBin(codeA, codeB) == 0))
                {
                        res = 1;
                }
        }

        return res;
}

int isPlayerHitSpecial(SpecialAttack *ability)
{
        int res = 0;

        CalculCoord codeA, codeB;
        if(_mainChar.state == CHAR_BOW)
        {
                codeA = calculCoord(_mainChar.dstRect.x, _mainChar.dstRect.y + _mainChar.dstRect.h/2, ability->spriteRectDst.x, ability->spriteRectDst.y, (ability->spriteRectDst.x + ability->spriteRectDst.w), (ability->spriteRectDst.y + ability->spriteRectDst.h));
                codeB = calculCoord((_mainChar.dstRect.x + _mainChar.dstRect.w), (_mainChar.dstRect.y + _mainChar.dstRect.h), ability->spriteRectDst.x, ability->spriteRectDst.y, (ability->spriteRectDst.x + ability->spriteRectDst.w), (ability->spriteRectDst.y + ability->spriteRectDst.h));
        }
        else
        {
                codeA = calculCoord(_mainChar.dstRect.x, _mainChar.dstRect.y, ability->spriteRectDst.x, ability->spriteRectDst.y, (ability->spriteRectDst.x + ability->spriteRectDst.w), (ability->spriteRectDst.y + ability->spriteRectDst.h));
                codeB = calculCoord((_mainChar.dstRect.x + _mainChar.dstRect.w), (_mainChar.dstRect.y + _mainChar.dstRect.h), ability->spriteRectDst.x, ability->spriteRectDst.y, (ability->spriteRectDst.x + ability->spriteRectDst.w), (ability->spriteRectDst.y + ability->spriteRectDst.h));
        }

        if(codeA.sum == 0 && codeB.sum == 0) res = 1;
        else if(calculCoordBin(codeA, codeB) == 0) res = 1;

        return res;
}

int damagePlayer(int strength)
{
        crossSleep(0, 500);

        int res = 0;

        if(_mainChar.specs.isInvulnerable) return 0;

        _mainChar.hp -= strength;

        playSound(SOUND_CHAR_HIT);

        if(_mainChar.hp <= 0)
        {
                res = 1;
                playSound(SOUND_CHAR_DEATH);
                //setBlackscreenTimer(2,0);
                //toggleBlackscreenFade(FADE_IN);
                playerDeadMenu();
        }

        if(strength > 0)
        {
                _mainChar.specs.isInvulnerable = 1;
                _mainChar.specs.frameInvulnerable = 0;
                _mainChar.specs.cooldownFrameInvulnerable = 50;
                _mainChar.specs.timerInvulnerable = SDL_GetTicks();
                _mainChar.specs.timerFrameInvulnerable = SDL_GetTicks();
        }
        return res;
}
