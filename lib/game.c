#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../headers/config.h"
#include "../headers/utility.h"
#include "../headers/load.h"
#include "../headers/sdl_tools.h"
#include "../headers/linked_list.h"
#include "../headers/game.h"
#include "../headers/character.h"
#include "../headers/boss.h"
#include "../headers/menu.h"
#include "../headers/ability.h"

// extern : this var comes from another file
extern SDL_Window *  _mainWindow;
extern SDL_Renderer *_renderer;
extern SDL_Texture * _textures[TEXTURES_TOTAL];
extern int           _loops[LOOP_TOTAL];
extern Keymap        _keymap;
//extern Keymap _keymapControler;

extern MainCharacter _mainChar;
extern Boss          _boss;
extern LifeBar       _lifeBar;

extern SDL_Rect _shadowRect;
extern Keymap   _keymap;

extern int _fadeIn;
extern int _fadeOut;

int _mouse[MOUSE_TOTAL] = { 0 };

extern TTF_Font *_announcementFont;

extern SDL_Color color_white;
extern SDL_Color color_black;
extern SDL_Color color_red;
extern SDL_Color color_green;
extern SDL_Color color_blue;
extern SDL_Color color_cyan;
extern SDL_Color color_magenta;
extern SDL_Color color_yellow;
extern SDL_Color color_grey;

int              _crtTalking = 0;
int              _skipFrame  = 0;
extern int       _multiplayer;
extern int       _playingChar;
extern int       _playingBoss;
extern SDL_Event tEvent;
extern int       tNewGameEvent;
extern int       tNewServerEvent;
extern char *    tSerialDataToServer;
extern char *    tSerialDataFromServer;

int save(int levelId, int saveId, Specs specs, Passive passive) {
    int   res  = -1;
    FILE *fp   = NULL;
    char *path = smalloc(sizeof(char), 50);

    path = createSaveStr(LOAD_SAVE, saveId);

    fp = sfopen(path, "wb");

    WriteBuffer buf;

    buf.level   = levelId;
    buf.specs   = specs;
    buf.passive = passive;

    logger("Writing save...\n", LOGGER_INFO);

    if (!(fwrite(&buf, sizeof(WriteBuffer), 1, fp))) {
        logger("Failed to save", LOGGER_FAIL);
        res = 0;
    }else{
        res = 1;
    }

    free(path);
    fclose(fp);

    return res;
}

int resetSave(int saveId) {
    int   res  = -1;
    char *path = smalloc(sizeof(char), 50);

    // config/save/%saveId/.new
    path = createSaveStr(NEW_FILE, saveId);

    FILE *fp = sfopen(path, "wb");
    fclose(fp);

    Specs specs = {
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        250
    };

    Passive passive = {
        0
    };

    if ((save(1, saveId, specs, passive)) < 0) {
        res = 0;
    }

    free(path);
    return res;
}

void handleServerEvents(int *servEvent) {
    switch (*servEvent) {
    case SRV_BOSS_JUMP:
        _boss.dstRect.y += 5;
        break;

    case SRV_BOSS_LEFT:
        _boss.dstRect.x -= 20;
        break;

    case SRV_BOSS_RIGHT:
        _boss.dstRect.x += 20;
        break;
    }

    *servEvent = -1;
}

void handleEventsLoop() {
    if (!(isDialogDone())) {
        return;
    }

    // Event handler
    SDL_Event e;

    while (SDL_PollEvent(&e) != 0) {
        handleEvents(e);
    }     // END OF SDL_PollEvent
}

void handleEvents(SDL_Event e) {
    int sendEvent = 1;

    if (!(_playingBoss)|| (tNewServerEvent)) {
        // User requests quit
        if (e.type == SDL_QUIT) {
            closeSDL();
            exit(1);
            _loops[PROGRAM_LOOP] = 1;
        }
        if (e.type == SDL_MOUSEMOTION) {
            _mouse[MOUSE_X] = e.button.x;
            _mouse[MOUSE_Y] = e.button.y;
        }else if (e.type == SDL_KEYDOWN) {
            if (e.key.keysym.sym == _keymap.up) {
                if (_mainChar.currentJump < _mainChar.maxJumps) {
                    if (_mainChar.direction[DIRECTION_UP] != 1) {
                        _mainChar.initialJumpPos = _mainChar.dstRect.y;
                    }
                    startJump();
                    _mainChar.isFalling = 0;
                    _mainChar.direction[DIRECTION_UP] = 1;
                }
            }else if (e.key.keysym.sym == _keymap.down) {
                _mainChar.direction[DIRECTION_DOWN] = 1;
                endJump();
            }else if (e.key.keysym.sym == _keymap.left) {
                _mainChar.direction[DIRECTION_LEFT] = 1;
                endMoveXAxis();
            }else if (e.key.keysym.sym == _keymap.right) {
                _mainChar.direction[DIRECTION_RIGHT] = 1;
                endMoveXAxis();
            }else if (e.key.keysym.sym == _keymap.menu) {
                inGameMenu(_loops[PROGRAM_LOOP], _loops[GAME_LOOP]);
                if ((_loops[PROGRAM_LOOP])) {
                    return;
                }
            }else if (e.key.keysym.sym == _keymap.dashLeft) {
                if ((SDL_GetTicks() - _mainChar.dashTimer > CHAR_DASH_DELAY)) {
                    dash(_mainChar.passive.hasTeleportation, DASH_LEFT);
                }
            }else if (e.key.keysym.sym == _keymap.dashRight) {
                if ((SDL_GetTicks() - _mainChar.dashTimer > CHAR_DASH_DELAY)) {
                    dash(_mainChar.passive.hasTeleportation, DASH_RIGHT);
                }
            }
        }else if (e.type == SDL_KEYUP) {
            if (e.key.keysym.sym == _keymap.up) {
                if (_mainChar.direction[DIRECTION_UP] == 1) {
                    _mainChar.direction[DIRECTION_UP] = 0;
                    _mainChar.isFalling = 1;
                    _mainChar.currentJump++;
                    endJump();
                }
            }else if (e.key.keysym.sym == _keymap.down) {
                _mainChar.direction[DIRECTION_DOWN] = 0;
            }else if (e.key.keysym.sym == _keymap.left) {
                _mainChar.direction[DIRECTION_LEFT] = 0;
                endMoveXAxis();
            }else if (e.key.keysym.sym == _keymap.right) {
                _mainChar.direction[DIRECTION_RIGHT] = 0;
                endMoveXAxis();
            }
        }else if (e.type == SDL_MOUSEBUTTONDOWN) {
            if (e.button.button == _keymap.shoot) {
                _mouse[MOUSE_LEFT] = 1;
                _mouse[MOUSE_X]    = e.button.x;
                _mouse[MOUSE_Y]    = e.button.y;
            }else if (e.button.button == _keymap.parry) {
                _mouse[MOUSE_RIGHT] = 1;
            }else if (e.button.button == _keymap.special) {
                _mouse[MOUSE_MIDDLE] = 1;
            }
        }else if (e.type == SDL_MOUSEBUTTONUP) {
            if (e.button.button == _keymap.shoot) {
                _mouse[MOUSE_LEFT] = 0;
            }else if (e.button.button == _keymap.parry) {
                _mouse[MOUSE_RIGHT] = 0;
            }else if (e.button.button == _keymap.special) {
                _mouse[MOUSE_MIDDLE] = 0;
            }
        }else{
            // if the received keypress is not usefull to the game, don't send it
            sendEvent = 0;
        }
    }else if (_playingBoss) {
        // HANDLE BOSS 1.2.3.4 AND MOUSE CLICK ON BOSS HUD
        if (e.type == SDL_QUIT) {
            // send quit flag to server
            closeSDL();
            exit(1);
            _loops[PROGRAM_LOOP] = 1;
        }else if (e.type == SDL_KEYDOWN) {
            if (e.key.keysym.sym == _keymap.menu) {
                inGameMenu(_loops[PROGRAM_LOOP], _loops[GAME_LOOP]);
                if ((_loops[PROGRAM_LOOP])) {
                    return;
                }
            }
        }else if (e.type == SDL_KEYUP) {
        }else if (e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEMOTION) {
        }else if (e.type == SDL_MOUSEBUTTONUP) {
        }else{
            // if the received keypress is not usefull to the game, don't send it
            sendEvent = 0;
        }
    }

    if ((sendEvent) && (_multiplayer)) {
        // Send event to client
        // Client then forwards it to server
        // Server broadcast to all but sender
        // Other client(s) then put the received event into their own handleEvent() function and update the char's position accordingly
        tEvent = e;
        tSerialDataToServer = serializeEvent(e);
        tNewGameEvent       = 1;
    }
}

char *serializeEvent(SDL_Event e) {
    // Don't forget the trailing "\r\n" to signal the end of the packet to the server
    int   size   = (sizeof(int) * 2) + (sizeof(char) + 2);
    char *buffer = smalloc(size, 1);

    int totalCnt = 0;

    int i = 0;
    int j = 24;

    for (i = 0, j = sizeof(int)*8; i < sizeof(int); i++) {
        buffer[totalCnt] = e.type >> j;
        j -= 8;
        totalCnt++;
    }

/*
    buffer[0] = e.type >> 24;
    buffer[1] = e.type >> 16;
    buffer[2] = e.type >> 8;
    buffer[3] = e.type;
*/

    for (i = 0, j = sizeof(int)*8; i < sizeof(int); i++) {
        buffer[totalCnt] = e.key.keysym.sym >> j;
        j -= 8;
        totalCnt++;
    }

/*
    buffer[4] = e.key.keysym.sym >> 24;
    buffer[5] = e.key.keysym.sym >> 16;
    buffer[6] = e.key.keysym.sym >> 8;
    buffer[7] = e.key.keysym.sym;
*/
    buffer[8] = e.button.button;

    strcat(buffer, "\r\n");

    printf("e.type : sent %x -- %02x %02x %02x %02x  serialized\n", e.type, buffer[0],buffer[1],buffer[2],buffer[3]);
    printf("e.keysym : sent %x -- %02x %02x %02x %02x  serialized\n", e.key.keysym.sym, buffer[4],buffer[5],buffer[6],buffer[7]);
    printf("e.button : sent %x -- %02x  serialized\n", e.button.button, buffer[8]);

    return buffer;
}

int updateProjectilePhysics() {
    int            cnt        = 0;
    int            playerDead = 0;
    Shot *         crtShot    = _mainChar.shotList->first;
    SpecialAttack *attack     = _boss.specialAttack;

    while (crtShot != NULL) {
        animateShot(crtShot);

        crtShot->dstRect.x += crtShot->vectorX * crtShot->vel;
        crtShot->dstRect.y += crtShot->vectorY * crtShot->vel;
        if (isBossHit(&crtShot->dstRect)) {
            damageBoss(crtShot, cnt);
        }

        if (isOutside(&crtShot->dstRect)) {
            removeShot(cnt, CHAR_SHOT_LIST);
        }

        crtShot = crtShot->next;
        cnt++;
    }
    if (_mainChar.state == CHAR_SPECIAL) {
        if (isBossHitSpecial(_mainChar.specialAttack)) {
            damageBossSpecial(_mainChar.specialAttack->strength);
        }
    }

    while (attack != NULL) {
        cnt = 0;
        if (attack->type == SHOT || attack->type == RAIN) {
            crtShot = attack->nextShot;
            while (crtShot != NULL) {
                crtShot->dstRect.x += crtShot->vectorX * crtShot->vel;
                crtShot->dstRect.y += crtShot->vectorY * crtShot->vel;
                if (isPlayerHit(crtShot->dstRect)) {
                    removeShotSpecial(attack, cnt);
                    if (damagePlayer(attack->strength)) {
                        playerDead = 1;
                        break;
                    }
                    switch (attack->effects) {
                    case POISONING:
                        _mainChar.specs.isPoisoned    = 1;
                        _mainChar.specs.timerPoisoned = SDL_GetTicks();
                        break;

                    case WEAKENING:
                        _mainChar.specs.isWeakened    = 1;
                        _mainChar.specs.timerWeakened = SDL_GetTicks();
                        break;

                    case SLOWING:
                        _mainChar.specs.isSlowed    = 1;
                        _mainChar.specs.timerSlowed = SDL_GetTicks();
                        break;

                    case PARALIZING:
                        _mainChar.specs.isParalized    = 1;
                        _mainChar.specs.timerParalized = SDL_GetTicks();
                        break;

                    case NONE:
                    default:
                        break;
                    }
                }
                if (isOutside(&crtShot->dstRect)) {
                    removeShotSpecial(attack, cnt);
                }

                crtShot = crtShot->next;
                cnt++;
            }
        }else if (attack->type == RAY) {
            if (attack->isActivated) {
                if (isPlayerHitSpecial(attack)) {
                    if (damagePlayer(attack->strength)) {
                        playerDead = 1;
                        break;
                    }
                    switch (attack->effects) {
                    case POISONING:
                        _mainChar.specs.isPoisoned    = 1;
                        _mainChar.specs.timerPoisoned = SDL_GetTicks();
                        break;

                    case WEAKENING:
                        _mainChar.specs.isWeakened    = 1;
                        _mainChar.specs.timerWeakened = SDL_GetTicks();
                        break;

                    case SLOWING:
                        _mainChar.specs.isSlowed    = 1;
                        _mainChar.specs.timerSlowed = SDL_GetTicks();
                        break;

                    case PARALIZING:
                        _mainChar.specs.isParalized    = 1;
                        _mainChar.specs.timerParalized = SDL_GetTicks();
                        break;

                    case NONE:
                    default:
                        break;
                    }
                }
            }
        }
        if (playerDead) {
            break;
        }
        attack = attack->next;
    }
    return playerDead;
}

void animateShot(Shot *shot) {
    updateAnimation(&shot->timer, &shot->frame, SHOT_ANIM_DELAY);

    if (shot->frame >= _mainChar.shotList->animation->frameQty[shot->state]) {
        shot->frame = 0;
    }
}

int isOutside(SDL_Rect *rect) {
    int res       = 0;
    int bonusSize = 50;

    // Check left border
    if (rect->x < -bonusSize) {
        return 1;
    }

    // Check top border
    if (rect->y < -bonusSize) {
        return 1;
    }
    // Check right border
    if (rect->x > SCREEN_WIDTH + rect->w + bonusSize) {
        return 1;
    }

    // Check bottom border
    if (rect->y > SCREEN_HEIGHT + (rect->h) + bonusSize) {
        return 1;
    }

    return res;
}

void wallSafe(SDL_Rect *rect) {
    // Check left border
    if (rect->x < 0) {
        rect->x = 0;
    }
    // Check top border
    if (rect->y < 0) {
        rect->y = 0;
    }
    // Check right border
    if (rect->x > SCREEN_WIDTH - rect->w) {
        rect->x = SCREEN_WIDTH - rect->w;
    }

    if (rect->y > SCREEN_HEIGHT - (rect->h + FLOOR_PADDING)) {
        rect->y = SCREEN_HEIGHT - (rect->h + FLOOR_PADDING);
    }
}

void updateShadow() {
    _shadowRect.x = _mainChar.dstRect.x + (CHAR_W * 0.12);
    _shadowRect.y = SCREEN_HEIGHT - FLOOR_PADDING - (CHAR_H * 0.2);
}

int update() {
    if (!(isDialogDone())) {
        return 0;
    }

    mainCharAction();

    updateCharPhysics();
    updateBossPhysics();

    if ((updateProjectilePhysics())) {
        return 1;
    }

    updateSpecialAbility(_mainChar.specialAttack);
    updateSpecialAbility(_boss.specialAttack);

    updateShadow();

    wallSafe(&_mainChar.dstRect);
    if (_loops[GAME_LOOP]) {
        bossAttack(_boss.specialAttack);
    }

    return 0;
}

int isDialogDone() {
    if (_mainChar.speech.hasTalked && _boss.speech.hasTalked) {
        return 1;
    }

    return 0;
}

void dialog() {
    if ((isDialogDone())) {
        return;
    }

    int       len    = 0;
    int       crtLen = 0;
    SDL_Event event;

    void *structBuffer[2] = { &_boss, &_mainChar };

    //MAINCHAR_CAST(structBuffer[_crtTalking])->speech.timer = SDL_GetTicks();

    crtLen = strlen(MAINCHAR_CAST(structBuffer[_crtTalking])->speech.text);

    len = (crtLen < TEXT_MAX_LETTERS_PER_DISPLAY) ? crtLen : TEXT_MAX_LETTERS_PER_DISPLAY;

    // Load textures
    _textures[DIALOG_TEXT] = createDialogText(&MAINCHAR_CAST(structBuffer[_crtTalking])->speech.text[MAINCHAR_CAST(structBuffer[_crtTalking])->speech.cnt * TEXT_MAX_LETTERS_PER_DISPLAY], color_white);
    switch (_crtTalking) {
    case 0:
        _textures[DIALOG_TEXT_TITLE] = createDialogText("Boss Name ", color_grey);
        break;

    case 1:
        _textures[DIALOG_TEXT_TITLE] = createDialogText("Char Name ", color_grey);
        break;
    }

    crtLen = strlen(MAINCHAR_CAST(structBuffer[_crtTalking])->speech.text);

    len = (crtLen < TEXT_MAX_LETTERS_PER_DISPLAY) ? crtLen : TEXT_MAX_LETTERS_PER_DISPLAY;

    loadDialogBox();

    renderDialog(TEXT_MARGIN, (SCREEN_HEIGHT - TEXT_WINDOW_HEIGHT + TEXT_MARGIN) + 4, TEXT_FONT_WIDTH * len, 26, DIALOG_MODE_TEXT);
    renderDialog(TEXT_MARGIN, (SCREEN_HEIGHT - TEXT_WINDOW_HEIGHT + (TEXT_MARGIN / 2) - 4), 10 * 8, 24, DIALOG_MODE_TEXT_TITLE);

    SDL_RenderPresent(_renderer);

    // Remove the keyup "Enter" from the previous menu
    // Needed if we don't want the dialog to skip the first text
    SDL_WaitEvent(&event);

    while (!(isDialogDone())) {
        if ((_fadeIn) || (_fadeOut)) {
            return;
        }

        // Load textures
        _textures[DIALOG_TEXT] = createDialogText(&MAINCHAR_CAST(structBuffer[_crtTalking])->speech.text[MAINCHAR_CAST(structBuffer[_crtTalking])->speech.cnt * TEXT_MAX_LETTERS_PER_DISPLAY], color_white);

        switch (_crtTalking) {
        case 0:
            _textures[DIALOG_TEXT_TITLE] = createDialogText("Boss Name ", color_grey);
            break;

        case 1:
            _textures[DIALOG_TEXT_TITLE] = createDialogText("Char Name ", color_grey);
            break;
        }

        crtLen = strlen(MAINCHAR_CAST(structBuffer[_crtTalking])->speech.text);

        // Adjust length of the rendered text to avoid overflow
        len = (crtLen < TEXT_MAX_LETTERS_PER_DISPLAY) ? crtLen : TEXT_MAX_LETTERS_PER_DISPLAY;

        renderDialog(TEXT_MARGIN, (SCREEN_HEIGHT - TEXT_WINDOW_HEIGHT + TEXT_MARGIN) + 4, TEXT_FONT_WIDTH * len, 26, DIALOG_MODE_TEXT);
        renderDialog(TEXT_MARGIN, (SCREEN_HEIGHT - TEXT_WINDOW_HEIGHT + (TEXT_MARGIN / 2) - 4), 10 * 8, 24, DIALOG_MODE_TEXT_TITLE);

        SDL_WaitEvent(&event);
        switch (event.type) {

        case SDL_KEYUP:
            switch (event.key.keysym.sym) {
            case SDLK_ESCAPE:
            case SDLK_RETURN:
                if (MAINCHAR_CAST(structBuffer[_crtTalking])->speech.cnt >= MAINCHAR_CAST(structBuffer[_crtTalking])->speech.totalParts - 1) {
                    MAINCHAR_CAST(structBuffer[_crtTalking])->speech.hasTalked = 1;
                    printf("crtTalking++\n");
                    _crtTalking++;

                    if ((isDialogDone())) {
                        break;
                    }
                }else{
                    // Next dialog chunk
                    MAINCHAR_CAST(structBuffer[_crtTalking])->speech.cnt++;
                }

                // Refresh timeout timer
                //MAINCHAR_CAST(structBuffer[_crtTalking])->speech.timer = SDL_GetTicks();
                break;
            }

            break;
        }
    }
}

void updateAnimation(unsigned long *timer, int *frame, int delay) {
    if (SDL_GetTicks() - *timer >= delay) {
        *timer  = SDL_GetTicks();
        *frame += 1;
    }
}

CalculCoord calculCoord(int x, int y, int xMinScreen, int yMinScreen, int xMaxScreen, int yMaxScreen) {
    CalculCoord c = {
        .sum   = 0,
        .left  = 0,
        .right = 0,
        .up    = 0,
        .down  = 0
    };

    if (y > yMaxScreen) {
        c.up = 1;
        c.sum++;
    }else if (y < yMinScreen) {
        c.down = 1;
        c.sum++;
    }
    if (x > xMaxScreen) {
        c.right = 1;
        c.sum++;
    }else if (x < xMinScreen) {
        c.left = 1;
        c.sum++;
    }
    return c;
}

int calculCoordBin(CalculCoord codeA, CalculCoord codeB) {
    int res = 0;

    if (codeA.up & codeB.up) {
        res = 1;
    }else if (codeA.down & codeB.down) {
        res = 1;
    }else if (codeA.right & codeB.right) {
        res = 1;
    }else if (codeA.left & codeB.left) {
        res = 1;
    }
    return res;
}
