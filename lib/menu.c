#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include "../headers/config.h"
#include "../headers/utility.h"
#include "../headers/sdl_tools.h"
#include "../headers/menu.h"
#include "../headers/init.h"
#include "../headers/load.h"
#include "../headers/linked_list.h"
#include "../headers/socket.h"
#include "../headers/server.h"
#include "../headers/client.h"
#include "../headers/game.h"

extern SDL_Color color_white;
extern SDL_Color color_black;
extern SDL_Color color_red;
extern SDL_Color color_green;
extern SDL_Color color_blue;
extern SDL_Color color_cyan;
extern SDL_Color color_magenta;
extern SDL_Color color_yellow;
extern SDL_Color color_grey;

extern TTF_Font *_menuFont;
extern TTF_Font *_dialogFont;

extern int _gameParams[CONFIG_TOTAL];
extern int tCloseServer;
extern int tCloseClient;
extern int tEvent;
extern char tGameInfo;
extern int _multiplayer;
extern int _playingBoss;
extern int _playingChar;

extern int _fadeIn;
extern int _fadeOut;

extern int _mouse[MOUSE_TOTAL];

int _isSliderBallClicked = 0;

int tWaitingForPlayer2 = 1;

// Thread
pthread_t clientSocketThread;
pthread_t serverThread;
extern ClientSocketInfo tClientSocketInfo;

MenuStruct _menuInGameTexture[IG_TOTAL_MENU];
MenuStruct _mainMenuTexture[MAIN_TOTAL_MENU];
MenuStruct _paramMenu[PARAM_TOTAL_MENU];
MenuStruct _loadCharMenuTextures[LOAD_CHAR_TOTAL];
MenuStruct _gameModeMenuTextures[GAME_MODE_TOTAL];
MenuStruct _multiplayerMenuTextures[MULTI_TOTAL];
MenuStruct _confirmBoxTextures[CONFIRM_TOTAL];
MenuStruct _playOptionsBoxTextures[PLAY_TOTAL];
MenuStruct _inputTextMenuTextures[INPUT_TOTAL];
MenuStruct _localMultiModeMenuTextures[INPUT_TOTAL];
MenuStruct _playerDeadMenu[PLAYER_TOTAL_MENU];
MenuStruct _soundMenuTextures[SOUND_MENU_TOTAL];

int _loops[LOOP_TOTAL];
int _currentSaveId;

extern SDL_Window *  _mainWindow;
extern SDL_Renderer *_renderer;

extern int _timerStartFight;

void mainMenu() {
        int selectCursor = MAIN_PLAY;
        SDL_Event event;

        renderMenu(_mainMenuTexture, MAIN_TOTAL_MENU);

        while (!_loops[MAIN_MENU_LOOP]) {
                SDL_WaitEvent(&event);
                switch (event.type) {
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_z:
                        case SDLK_UP:
                                selectCursor--;
                                if (selectCursor < MAIN_PLAY) {
                                        selectCursor = MAIN_LEAVE;                              //Option = Leave
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_s:
                        case SDLK_DOWN:
                                selectCursor++;
                                if (selectCursor > MAIN_LEAVE) {
                                        selectCursor = MAIN_PLAY;                               //Option = Play
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_RETURN:
                                switch (selectCursor) {
                                case MAIN_PLAY:
                                        _loops[GAME_LOOP]      = 1;
                                        _loops[MAIN_MENU_LOOP] = 1;
                                        gameModeMenu();
                                        break;

                                case MAIN_PARAM:
                                        paramMenu();
                                        break;

                                case MAIN_LEAVE:
                                        _loops[MAIN_MENU_LOOP] = 1;
                                        closeSDL();
                                        exit(1);
                                        break;
                                }
                                break;
                        }
                        break;

                case SDL_MOUSEMOTION:
                        if (event.motion.y < (_mainMenuTexture[MAIN_PLAY].size.y + _mainMenuTexture[MAIN_PLAY].size.h)) {
                                selectCursor = MAIN_PLAY;
                        }else if ((event.motion.y >= _mainMenuTexture[MAIN_PARAM].size.y) && event.motion.y < (_mainMenuTexture[MAIN_PARAM].size.y + _mainMenuTexture[MAIN_PARAM].size.h)) {
                                selectCursor = MAIN_PARAM;
                        }else if ((event.motion.y >= _mainMenuTexture[MAIN_LEAVE].size.y) && event.motion.y < (_mainMenuTexture[MAIN_LEAVE].size.y + _mainMenuTexture[MAIN_LEAVE].size.h)) {
                                selectCursor = MAIN_LEAVE;
                        }
                        break;

                case SDL_MOUSEBUTTONDOWN:
                        switch (selectCursor) {
                        case MAIN_PLAY:
                                if ((isHovering(_mainMenuTexture[MAIN_PLAY], event.motion.x, event.motion.y))) {
                                        _loops[GAME_LOOP]      = 1;
                                        _loops[MAIN_MENU_LOOP] = 1;
                                        gameModeMenu();
                                }
                                break;

                        case MAIN_PARAM:
                                if ((isHovering(_mainMenuTexture[MAIN_PARAM], event.motion.x, event.motion.y))) {
                                        paramMenu();
                                }
                                break;

                        case MAIN_LEAVE:
                                if ((isHovering(_mainMenuTexture[MAIN_LEAVE], event.motion.x, event.motion.y))) {
                                        _loops[MAIN_MENU_LOOP] = 1;
                                        closeSDL();
                                        exit(1);
                                }
                                break;
                        }
                        break;
                }
                changeMainPosCursor(selectCursor);
                renderMenu(_mainMenuTexture, MAIN_TOTAL_MENU);
        }
}

void changeMainPosCursor(int posCursor) {
        switch (posCursor) {
        case MAIN_PLAY:
                _mainMenuTexture[MAIN_OPENING_CHEVRON].size.y = 190;
                _mainMenuTexture[MAIN_CLOSING_CHEVRON].size.y = 190;
                break;

        case MAIN_PARAM:
                _mainMenuTexture[MAIN_OPENING_CHEVRON].size.y = 290;
                _mainMenuTexture[MAIN_CLOSING_CHEVRON].size.y = 290;
                break;

        case MAIN_LEAVE:
                _mainMenuTexture[MAIN_OPENING_CHEVRON].size.y = 390;
                _mainMenuTexture[MAIN_CLOSING_CHEVRON].size.y = 390;
                break;

        default:
                break;
        }
}

//=================================================================================================

void inGameMenu() {
        SDL_Event event;

        _loops[INGAME_MENU_LOOP] = 0;
        int selectCursor = IG_CONTINUE; //Option = Continue
        renderMenu(_menuInGameTexture, IG_TOTAL_MENU);

        while (!_loops[INGAME_MENU_LOOP]) {
                SDL_WaitEvent(&event);
                switch (event.type) {
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_ESCAPE:
                                _loops[INGAME_MENU_LOOP] = 1;
                                break;

                        case SDLK_z:
                        case SDLK_UP:
                                selectCursor--;
                                if (selectCursor < IG_CONTINUE) {
                                        selectCursor = IG_LEAVE;                                //Option = Leave
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_s:
                        case SDLK_DOWN:
                                selectCursor++;
                                if (selectCursor > IG_LEAVE) {
                                        selectCursor = IG_CONTINUE;                             //Option = Continue
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_RETURN:
                                switch (selectCursor) {
                                case IG_CONTINUE:
                                        _loops[INGAME_MENU_LOOP] = 1;
                                        break;

                                case IG_BACK_TO_MENU:
                                        _loops[PROGRAM_LOOP]     = 0;
                                        _loops[GAME_LOOP]        = 0;
                                        _loops[INGAME_MENU_LOOP] = 1;
                                        break;

                                case IG_PARAM:
                                        paramMenu();
                                        break;

                                case IG_LEAVE:
                                        closeSDL();
                                        exit(1);
                                        _loops[INGAME_MENU_LOOP] = 1;
                                        break;
                                }
                                break;

                        default:
                                break;
                        }
                        break;

                case SDL_MOUSEMOTION:
                        if (event.motion.y < (_menuInGameTexture[IG_CONTINUE].size.y + _menuInGameTexture[IG_CONTINUE].size.h)) {
                                selectCursor = IG_CONTINUE;
                        } else if ((event.motion.y >= _menuInGameTexture[IG_BACK_TO_MENU].size.y) && event.motion.y < (_menuInGameTexture[IG_BACK_TO_MENU].size.y + _menuInGameTexture[IG_BACK_TO_MENU].size.h)) {
                                selectCursor = IG_BACK_TO_MENU;
                        } else if ((event.motion.y >= _menuInGameTexture[IG_PARAM].size.y) && event.motion.y < (_menuInGameTexture[IG_PARAM].size.y + _menuInGameTexture[IG_PARAM].size.h)) {
                                selectCursor = IG_PARAM;
                        } else if ((event.motion.y >= _menuInGameTexture[IG_LEAVE].size.y) && event.motion.y < (_menuInGameTexture[IG_LEAVE].size.y + _menuInGameTexture[IG_LEAVE].size.h)) {
                                selectCursor = IG_LEAVE;
                        }
                        break;

                case SDL_MOUSEBUTTONDOWN:
                        switch (selectCursor) {
                        case IG_CONTINUE:
                                if ((isHovering(_menuInGameTexture[IG_CONTINUE], event.motion.x, event.motion.y))) {
                                        _loops[INGAME_MENU_LOOP] = 1;
                                }
                                break;

                        case IG_BACK_TO_MENU:
                                if ((isHovering(_menuInGameTexture[IG_BACK_TO_MENU], event.motion.x, event.motion.y))) {
                                        _loops[PROGRAM_LOOP]     = 0;
                                        _loops[INGAME_MENU_LOOP] = 1;
                                        _loops[GAME_LOOP]        = 0;
                                }
                                break;

                        case IG_PARAM:
                                if ((isHovering(_menuInGameTexture[IG_PARAM], event.motion.x, event.motion.y))) {
                                        paramMenu();
                                }
                                break;

                        case IG_LEAVE:
                                if ((isHovering(_menuInGameTexture[IG_LEAVE], event.motion.x, event.motion.y))) {
                                        closeSDL();
                                        exit(1);
                                        _loops[INGAME_MENU_LOOP] = 1;
                                }
                                break;
                        }
                        break;

                default:
                        break;
                }
                changeInGamePosCursor(selectCursor);
                renderMenu(_menuInGameTexture, IG_TOTAL_MENU);
        }
}

void changeInGamePosCursor(int posCursor) {
        switch (posCursor) {
        case IG_CONTINUE:
                _menuInGameTexture[IG_OPENING_CHEVRON].size.y = 165;
                _menuInGameTexture[IG_CLOSING_CHEVRON].size.y = 165;
                break;

        case IG_BACK_TO_MENU:
                _menuInGameTexture[IG_OPENING_CHEVRON].size.y = 240;
                _menuInGameTexture[IG_CLOSING_CHEVRON].size.y = 240;
                break;

        case IG_PARAM:
                _menuInGameTexture[IG_OPENING_CHEVRON].size.y = 315;
                _menuInGameTexture[IG_CLOSING_CHEVRON].size.y = 315;
                break;

        case IG_LEAVE:
                _menuInGameTexture[IG_OPENING_CHEVRON].size.y = 390;
                _menuInGameTexture[IG_CLOSING_CHEVRON].size.y = 390;
                break;
        }
}

// ======================================================================================

void paramMenu() {
        _loops[PARAM_MENU_LOOP] = 0;
        int selectCursor = PARAM_FULLSCREEN;
        SDL_Event event;
        renderMenu(_paramMenu, PARAM_TOTAL_MENU);

        while (!_loops[PARAM_MENU_LOOP]) {
                SDL_WaitEvent(&event);
                switch (event.type) {
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_z:
                        case SDLK_UP:
                                selectCursor--;
                                if (selectCursor < PARAM_FULLSCREEN) {
                                        selectCursor = PARAM_LEAVE;
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_s:
                        case SDLK_DOWN:
                                selectCursor++;
                                if (selectCursor > PARAM_LEAVE) {
                                        selectCursor = PARAM_FULLSCREEN;
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_RETURN:
                                switch (selectCursor) {
                                case PARAM_SOUND:
                                        soundMenu();
                                        break;

                                case PARAM_FULLSCREEN:
                                        if (_paramMenu[PARAM_FULLSCREEN_PICTURE].isVisible) {
                                                _paramMenu[PARAM_FULLSCREEN].isVisible++;
                                                _paramMenu[PARAM_FULLSCREEN_PICTURE].isVisible--;
                                                SDL_SetWindowFullscreen(_mainWindow, 0);
                                                setParam(CONFIG_FULLSCREEN, 0);
                                                _gameParams[CONFIG_FULLSCREEN] = 0;
                                        }else {
                                                _paramMenu[PARAM_FULLSCREEN].isVisible--;
                                                _paramMenu[PARAM_FULLSCREEN_PICTURE].isVisible++;
                                                SDL_SetWindowFullscreen(_mainWindow, SDL_WINDOW_FULLSCREEN);
                                                setParam(CONFIG_FULLSCREEN, 1);
                                                _gameParams[CONFIG_FULLSCREEN] = 1;
                                        }
                                        break;

                                case PARAM_LEAVE:
                                        _loops[PARAM_MENU_LOOP] = 1;
                                        break;
                                }
                                break;
                        }
                        break;

                case SDL_MOUSEMOTION:
                        if ((event.motion.y >= _paramMenu[PARAM_FULLSCREEN].size.y) && event.motion.y < (_paramMenu[PARAM_FULLSCREEN].size.y + _paramMenu[PARAM_FULLSCREEN].size.h)) {
                                selectCursor = PARAM_FULLSCREEN;
                        } else if (event.motion.y < (_paramMenu[PARAM_SOUND].size.y + _paramMenu[PARAM_SOUND].size.h)) {
                                selectCursor = PARAM_SOUND;
                        } else if ((event.motion.y >= _paramMenu[PARAM_LEAVE].size.y) && event.motion.y < (_paramMenu[PARAM_LEAVE].size.y + _paramMenu[PARAM_LEAVE].size.h)) {
                                selectCursor = PARAM_LEAVE;
                        }
                        break;

                case SDL_MOUSEBUTTONDOWN:
                        switch (selectCursor) {
                        case PARAM_SOUND:
                                if ((isHovering(_paramMenu[PARAM_SOUND], event.motion.x, event.motion.y))) {
                                        soundMenu();
                                }

                                break;

                        case PARAM_FULLSCREEN:
                                if ((isHovering(_paramMenu[PARAM_FULLSCREEN], event.motion.x, event.motion.y))) {
                                        if (_paramMenu[PARAM_FULLSCREEN_PICTURE].isVisible) {
                                                _paramMenu[PARAM_FULLSCREEN].isVisible++;
                                                _paramMenu[PARAM_FULLSCREEN_PICTURE].isVisible--;
                                                SDL_SetWindowFullscreen(_mainWindow, 0);
                                                setParam(CONFIG_FULLSCREEN, 0);
                                                _gameParams[CONFIG_FULLSCREEN] = 0;
                                        }else {
                                                _paramMenu[PARAM_FULLSCREEN].isVisible--;
                                                _paramMenu[PARAM_FULLSCREEN_PICTURE].isVisible++;
                                                SDL_SetWindowFullscreen(_mainWindow, SDL_WINDOW_FULLSCREEN);
                                                setParam(CONFIG_FULLSCREEN, 1);
                                                _gameParams[CONFIG_FULLSCREEN] = 1;
                                        }
                                }
                                break;

                        case PARAM_LEAVE:
                                if ((isHovering(_paramMenu[PARAM_LEAVE], event.motion.x, event.motion.y))) {
                                        _loops[PARAM_MENU_LOOP] = 1;
                                }

                                break;
                        }
                        break;

                default:
                        break;
                }
                changeParamPosCursor(selectCursor);
                renderMenu(_paramMenu, PARAM_TOTAL_MENU);
        }
}

void changeParamPosCursor(int posCursor) {
        switch (posCursor) {
        case PARAM_FULLSCREEN:
                _paramMenu[PARAM_OPENING_CHEVRON].size.y = 165;
                _paramMenu[PARAM_CLOSING_CHEVRON].size.y = 165;
                break;

        case PARAM_SOUND:
                _paramMenu[PARAM_OPENING_CHEVRON].size.y = 240;
                _paramMenu[PARAM_CLOSING_CHEVRON].size.y = 240;
                break;

        case PARAM_LEAVE:
                _paramMenu[PARAM_OPENING_CHEVRON].size.y = 390;
                _paramMenu[PARAM_CLOSING_CHEVRON].size.y = 390;
                break;
        }
}

// ======================================================================================

void loadCharMenu() {
        char *newName = smalloc(sizeof(char), 51);

        _loops[LOAD_CHAR_LOOP] = 0;

        int selectCursor = LOAD_CHAR_SLOT_1;
        SDL_Event event;
        char *    text       = smalloc(sizeof(char), 30);
        char *    playerName = smalloc(sizeof(char), 51);

        int slot1New = 1;
        int slot2New = 1;
        int slot3New = 1;

        int firstTime = 0;

        int rescanSaves = 0;

        while (!_loops[LOAD_CHAR_LOOP]) {
                renderMenu(_loadCharMenuTextures, LOAD_CHAR_TOTAL);

                SDL_WaitEvent(&event);

                if ((rescanSaves) || firstTime == 0) {
                        if ((firstTime)) {
                                firstTime++;
                        }
                        if ((exists("config/save/1/.new"))) {
                                strcpy(text, "New game");
                                slot1New = 1;
                        }else{
                                playerName = loadPlayerName(1);
                                strcpy(text, playerName);
                                slot1New = 0;
                        }
                        addMenuOption(text, SCREEN_WIDTH / 2, 175, 250, 50, color_white, _loadCharMenuTextures, LOAD_CHAR_SLOT_1);

                        if ((exists("config/save/2/.new"))) {
                                strcpy(text, "New game");
                                slot2New = 1;
                        }else{
                                playerName = loadPlayerName(2);
                                strcpy(text, playerName);
                                slot2New = 0;
                        }
                        addMenuOption(text, SCREEN_WIDTH / 2, 225, 250, 50, color_white, _loadCharMenuTextures, LOAD_CHAR_SLOT_2);

                        if ((exists("config/save/3/.new"))) {
                                strcpy(text, "New game");
                                slot3New = 1;
                        }else{
                                playerName = loadPlayerName(3);
                                strcpy(text, playerName);

                                slot3New = 0;
                        }
                        addMenuOption(text, SCREEN_WIDTH / 2, 275, 250, 50, color_white, _loadCharMenuTextures, LOAD_CHAR_SLOT_3);
                }

                rescanSaves = 0;

                switch (event.type) {
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_z:
                        case SDLK_UP:
                                selectCursor--;
                                if (selectCursor < LOAD_CHAR_SLOT_1) {
                                        selectCursor = LOAD_CHAR_LEAVE;                                     //Option = Leave
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_s:
                        case SDLK_DOWN:
                                selectCursor++;
                                if (selectCursor > LOAD_CHAR_LEAVE) {
                                        selectCursor = LOAD_CHAR_SLOT_1;                                    //Option = Activate sound
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_RETURN:
                                switch (selectCursor) {
                                case LOAD_CHAR_SLOT_1:
                                        _currentSaveId         = 1;
                                        _loops[LOAD_CHAR_LOOP] = 1;
                                        if ((slot1New)) {
                                                newName = inputTextMenu(INPUT_TEXT_NAME_MODE);
                                                if (newName == NULL) {
                                                        _loops[LOAD_CHAR_LOOP] = 0;
                                                        break;
                                                }
                                                appendStr(createSaveStr(LOAD_SAVE, _currentSaveId), newName);
                                                crossUnlink(createSaveStr(NEW_FILE, _currentSaveId));
                                                loadGame(_currentSaveId);

                                                rescanSaves = 1;
                                                // start character creation
                                        }else{
                                                playOptionsBox(_currentSaveId);
                                        }
                                        break;

                                case LOAD_CHAR_SLOT_2:
                                        _currentSaveId         = 2;
                                        _loops[LOAD_CHAR_LOOP] = 1;
                                        if ((slot2New)) {
                                                newName = inputTextMenu(INPUT_TEXT_NAME_MODE);
                                                if (newName == NULL) {
                                                        _loops[LOAD_CHAR_LOOP] = 0;
                                                        break;
                                                }
                                                appendStr(createSaveStr(LOAD_SAVE, _currentSaveId), newName);
                                                crossUnlink(createSaveStr(NEW_FILE, _currentSaveId));
                                                loadGame(_currentSaveId);
                                                rescanSaves = 1;
                                                // start character creation
                                        }else{
                                                playOptionsBox(_currentSaveId);
                                        }
                                        break;

                                case LOAD_CHAR_SLOT_3:
                                        _currentSaveId         = 3;
                                        _loops[LOAD_CHAR_LOOP] = 1;
                                        if ((slot3New)) {
                                                newName = inputTextMenu(INPUT_TEXT_NAME_MODE);
                                                if (newName == NULL) {
                                                        _loops[LOAD_CHAR_LOOP] = 0;
                                                        break;
                                                }
                                                appendStr(createSaveStr(LOAD_SAVE, _currentSaveId), newName);
                                                crossUnlink(createSaveStr(NEW_FILE, _currentSaveId));
                                                loadGame(_currentSaveId);
                                                rescanSaves = 1;
                                                // start character creation
                                        }else{
                                                playOptionsBox(_currentSaveId);
                                        }
                                        break;

                                case LOAD_CHAR_LEAVE:
                                        _loops[LOAD_CHAR_LOOP] = 1;
                                        _loops[GAME_LOOP]      = 0;
                                        _loops[GAME_MODE_LOOP] = 0;
                                        gameModeMenu();
                                        break;
                                }
                                break;
                        }
                        break;

                case SDL_MOUSEMOTION:
                        if (event.motion.y < (_loadCharMenuTextures[LOAD_CHAR_SLOT_1].size.y + _loadCharMenuTextures[LOAD_CHAR_SLOT_1].size.h)) {
                                selectCursor = LOAD_CHAR_SLOT_1;
                        } else if ((event.motion.y >= _loadCharMenuTextures[LOAD_CHAR_SLOT_2].size.y) && event.motion.y < (_loadCharMenuTextures[LOAD_CHAR_SLOT_2].size.y + _loadCharMenuTextures[LOAD_CHAR_SLOT_2].size.h)) {
                                selectCursor = LOAD_CHAR_SLOT_2;
                        } else if ((event.motion.y >= _loadCharMenuTextures[LOAD_CHAR_SLOT_3].size.y) && event.motion.y < (_loadCharMenuTextures[LOAD_CHAR_SLOT_3].size.y + _loadCharMenuTextures[LOAD_CHAR_SLOT_3].size.h)) {
                                selectCursor = LOAD_CHAR_SLOT_3;
                        } else if ((event.motion.y >= _loadCharMenuTextures[LOAD_CHAR_LEAVE].size.y) && event.motion.y < (_loadCharMenuTextures[LOAD_CHAR_LEAVE].size.y + _loadCharMenuTextures[LOAD_CHAR_LEAVE].size.h)) {
                                selectCursor = LOAD_CHAR_LEAVE;
                        }
                        break;

                case SDL_MOUSEBUTTONDOWN:
                        switch (selectCursor) {
                        case LOAD_CHAR_SLOT_1:
                                if ((isHovering(_loadCharMenuTextures[LOAD_CHAR_SLOT_1], event.motion.x, event.motion.y))) {
                                        _currentSaveId         = 1;
                                        _loops[LOAD_CHAR_LOOP] = 1;
                                        if ((slot1New)) {
                                                newName = inputTextMenu(INPUT_TEXT_NAME_MODE);
                                                if (newName == NULL) {
                                                        _loops[LOAD_CHAR_LOOP] = 0;
                                                        break;
                                                }
                                                appendStr(createSaveStr(LOAD_SAVE, _currentSaveId), newName);
                                                crossUnlink(createSaveStr(NEW_FILE, _currentSaveId));
                                                loadGame(_currentSaveId);
                                                rescanSaves = 1;
                                                // start character creation
                                        }else{
                                                playOptionsBox(_currentSaveId);
                                        }
                                }
                                break;

                        case LOAD_CHAR_SLOT_2:
                                if ((isHovering(_loadCharMenuTextures[LOAD_CHAR_SLOT_2], event.motion.x, event.motion.y))) {
                                        _currentSaveId         = 2;
                                        _loops[LOAD_CHAR_LOOP] = 1;
                                        if ((slot2New)) {
                                                newName = inputTextMenu(INPUT_TEXT_NAME_MODE);
                                                if (newName == NULL) {
                                                        _loops[LOAD_CHAR_LOOP] = 0;
                                                        break;
                                                }
                                                appendStr(createSaveStr(LOAD_SAVE, _currentSaveId), newName);
                                                crossUnlink(createSaveStr(NEW_FILE, _currentSaveId));
                                                loadGame(_currentSaveId);
                                                rescanSaves = 1;
                                                // start character creation
                                        }else{
                                                playOptionsBox(_currentSaveId);
                                        }
                                }
                                break;

                        case LOAD_CHAR_SLOT_3:
                                if ((isHovering(_loadCharMenuTextures[LOAD_CHAR_SLOT_3], event.motion.x, event.motion.y))) {
                                        _currentSaveId         = 3;
                                        _loops[LOAD_CHAR_LOOP] = 1;
                                        if ((slot3New)) {
                                                newName = inputTextMenu(INPUT_TEXT_NAME_MODE);
                                                if (newName == NULL) {
                                                        _loops[LOAD_CHAR_LOOP] = 0;
                                                        break;
                                                }
                                                appendStr(createSaveStr(LOAD_SAVE, _currentSaveId), newName);
                                                crossUnlink(createSaveStr(NEW_FILE, _currentSaveId));
                                                loadGame(_currentSaveId);
                                                rescanSaves = 1;
                                                // start character creation
                                        }else{
                                                playOptionsBox(_currentSaveId);
                                        }
                                }
                                break;

                        case LOAD_CHAR_LEAVE:
                                if ((isHovering(_loadCharMenuTextures[LOAD_CHAR_LEAVE], event.motion.x, event.motion.y))) {
                                        gameModeMenu();
                                        _loops[LOAD_CHAR_LOOP] = 1;
                                        _loops[GAME_LOOP]      = 0;
                                        _loops[GAME_MODE_LOOP] = 0;
                                }
                                break;
                        }
                        break;
                }
                changeLoadCharPosCursor(selectCursor);
                renderMenu(_loadCharMenuTextures, LOAD_CHAR_TOTAL);
        }

        if (newName != NULL) {
                free(newName);
        }
}

void changeLoadCharPosCursor(int posCursor) {
        switch (posCursor) {
        case LOAD_CHAR_SLOT_1:
                _loadCharMenuTextures[LOAD_CHAR_CHEVRON].size.y = 190;
                break;

        case LOAD_CHAR_SLOT_2:
                _loadCharMenuTextures[LOAD_CHAR_CHEVRON].size.y = 240;
                break;

        case LOAD_CHAR_SLOT_3:
                _loadCharMenuTextures[LOAD_CHAR_CHEVRON].size.y = 290;
                break;

        case LOAD_CHAR_LEAVE:
                _loadCharMenuTextures[LOAD_CHAR_CHEVRON].size.y = 390;
                break;
        }
}

// ======================================================================================

void gameModeMenu() {
        _loops[GAME_MODE_LOOP] = 0;
        int selectCursor = GAME_MODE_AVENTURE;
        SDL_Event event;
        int chosenLevel = 0;
        renderMenu(_gameModeMenuTextures, GAME_MODE_TOTAL);

        while (!_loops[GAME_MODE_LOOP]) {
                SDL_WaitEvent(&event);
                switch (event.type) {
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_z:
                        case SDLK_UP:
                                selectCursor--;
                                if (selectCursor < GAME_MODE_AVENTURE) {
                                        selectCursor = GAME_MODE_LEAVE;                                       //Option = Leave
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_s:
                        case SDLK_DOWN:
                                selectCursor++;
                                if (selectCursor > GAME_MODE_LEAVE) {
                                        selectCursor = GAME_MODE_AVENTURE;
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_RETURN:
                                switch (selectCursor) {
                                case GAME_MODE_AVENTURE:
                                        _loops[GAME_MODE_LOOP] = 1;
                                        loadCharMenu();
                                        break;

                                case GAME_MODE_MULTIPLAYER:
                                        _loops[GAME_MODE_LOOP] = 1;
                                        multiplayerMenu();
                                        break;

                                case GAME_MODE_ARCADE:
                                        _loops[GAME_MODE_LOOP] = 1;
                                        chosenLevel            = atoi(inputTextMenu(INPUT_TEXT_SELECT_LEVEL_MODE));

                                        if (chosenLevel != 0 && chosenLevel <= 6) {
                                                _loops[GAME_LOOP] = 1;
                                                loadGame(ARCADE_SAVE, chosenLevel);
                                        }else {
                                                _loops[GAME_MODE_LOOP] = 0;
                                        }

                                        break;

                                case GAME_MODE_LEAVE:
                                        _loops[GAME_MODE_LOOP] = 1;
                                        _loops[MAIN_MENU_LOOP] = 0;
                                        break;
                                }
                                break;
                        }
                        break;

                case SDL_MOUSEMOTION:
                        if (event.motion.y < (_gameModeMenuTextures[GAME_MODE_AVENTURE].size.y + _gameModeMenuTextures[GAME_MODE_AVENTURE].size.h)) {
                                selectCursor = GAME_MODE_AVENTURE;
                        } else if ((event.motion.y >= _gameModeMenuTextures[GAME_MODE_MULTIPLAYER].size.y) && event.motion.y < (_gameModeMenuTextures[GAME_MODE_MULTIPLAYER].size.y + _gameModeMenuTextures[GAME_MODE_MULTIPLAYER].size.h)) {
                                selectCursor = GAME_MODE_MULTIPLAYER;
                        } else if ((event.motion.y >= _gameModeMenuTextures[GAME_MODE_ARCADE].size.y) && event.motion.y < (_gameModeMenuTextures[GAME_MODE_ARCADE].size.y + _gameModeMenuTextures[GAME_MODE_ARCADE].size.h)) {
                                selectCursor = GAME_MODE_ARCADE;
                        } else if ((event.motion.y >= _gameModeMenuTextures[GAME_MODE_LEAVE].size.y) && event.motion.y < (_gameModeMenuTextures[GAME_MODE_LEAVE].size.y + _gameModeMenuTextures[GAME_MODE_LEAVE].size.h)) {
                                selectCursor = GAME_MODE_LEAVE;
                        }
                        break;

                case SDL_MOUSEBUTTONDOWN:
                        switch (selectCursor) {
                        case GAME_MODE_AVENTURE:
                                if ((isHovering(_gameModeMenuTextures[GAME_MODE_AVENTURE], event.motion.x, event.motion.y))) {
                                        _loops[GAME_MODE_LOOP] = 1;
                                        loadCharMenu();
                                }
                                break;

                        case GAME_MODE_MULTIPLAYER:
                                if ((isHovering(_gameModeMenuTextures[GAME_MODE_MULTIPLAYER], event.motion.x, event.motion.y))) {
                                        _loops[GAME_MODE_LOOP] = 1;
                                        multiplayerMenu();
                                }
                                break;

                        case GAME_MODE_ARCADE:
                                if ((isHovering(_gameModeMenuTextures[GAME_MODE_ARCADE], event.motion.x, event.motion.y))) {
                                        _loops[GAME_MODE_LOOP] = 1;
                                        chosenLevel            = atoi(inputTextMenu(INPUT_TEXT_SELECT_LEVEL_MODE));

                                        if (chosenLevel != 0 && chosenLevel <= 6) {
                                                _loops[GAME_LOOP] = 1;
                                                loadGame(ARCADE_SAVE, chosenLevel);
                                        }else {
                                                _loops[GAME_MODE_LOOP] = 0;
                                        }
                                }
                                break;

                        case GAME_MODE_LEAVE:
                                if ((isHovering(_gameModeMenuTextures[GAME_MODE_LEAVE], event.motion.x, event.motion.y))) {
                                        _loops[GAME_MODE_LOOP] = 1;
                                        _loops[MAIN_MENU_LOOP] = 0;
                                }
                                break;
                        }
                        break;
                }
                changeGameModePosCursor(selectCursor);
                renderMenu(_gameModeMenuTextures, GAME_MODE_TOTAL);
        }
}

void changeGameModePosCursor(int posCursor) {
        switch (posCursor) {
        case GAME_MODE_AVENTURE:
                _gameModeMenuTextures[GAME_MODE_OPENING_CHEVRON].size.y = 190;
                _gameModeMenuTextures[GAME_MODE_CLOSING_CHEVRON].size.y = 190;
                break;

        case GAME_MODE_MULTIPLAYER:
                _gameModeMenuTextures[GAME_MODE_OPENING_CHEVRON].size.y = 240;
                _gameModeMenuTextures[GAME_MODE_CLOSING_CHEVRON].size.y = 240;
                break;

        case GAME_MODE_ARCADE:
                _gameModeMenuTextures[GAME_MODE_OPENING_CHEVRON].size.y = 290;
                _gameModeMenuTextures[GAME_MODE_CLOSING_CHEVRON].size.y = 290;
                break;

        case GAME_MODE_LEAVE:
                _gameModeMenuTextures[GAME_MODE_OPENING_CHEVRON].size.y = 390;
                _gameModeMenuTextures[GAME_MODE_CLOSING_CHEVRON].size.y = 390;
                break;
        }
}

// ======================================================================================

void localMultiModeMenu() {
        char *ipAddr = NULL;

        long serverWaitTimeout = 0;

        int chosenLevel = 0;

        _loops[LOCAL_MULTI_LOOP] = 0;
        int selectCursor = LOCAL_MULTI_HOST;
        SDL_Event event;
        renderMenu(_localMultiModeMenuTextures, LOCAL_MULTI_TOTAL);

        while (!_loops[LOCAL_MULTI_LOOP]) {
                SDL_WaitEvent(&event);
                switch (event.type) {
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_z:
                        case SDLK_UP:
                                selectCursor--;
                                if (selectCursor < LOCAL_MULTI_HOST) {
                                        selectCursor = LOCAL_MULTI_LEAVE;                                //Option = Leave
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_s:
                        case SDLK_DOWN:
                                selectCursor++;
                                if (selectCursor > LOCAL_MULTI_LEAVE) {
                                        selectCursor = LOCAL_MULTI_HOST;
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_RETURN:
                                switch (selectCursor) {
                                case LOCAL_MULTI_HOST:
                                        chosenLevel = atoi(inputTextMenu(INPUT_TEXT_SELECT_LEVEL_MODE));

                                        if (chosenLevel <= 0 || chosenLevel > 6) {
                                                break;
                                        }else{
                                                loadGame(chosenLevel);
                                        }
                                        // spawn server thread
                                        logger("Creating server thread...\n", LOGGER_INFO);
                                        pthread_create(&serverThread, NULL, &gameServer, NULL);
                                        logger("Client server thread started\n", LOGGER_OK);

                                        logger("Initializing client socket info...\n", LOGGER_INFO);
                                        ipAddr = smalloc(sizeof(char), 16);

                                        strcpy(ipAddr, "127.0.0.1");
                                        printf("Connecting to %s\n", ipAddr);

                                        initSocketInfo(ipAddr);
                                        logger("Client socket info initialized\n", LOGGER_OK);

                                        logger("Creating socket thread...\n", LOGGER_INFO);
                                        pthread_create(&clientSocketThread, NULL, &clientGameSocket, &tClientSocketInfo);
                                        logger("Client socket thread started\n", LOGGER_OK);

                                        _playingChar = 1;
                                        _multiplayer = 1;

                                        if (renderWaitingScreen()) {
                                                // Client connected
                                                // load and launch chosen level
                                                _loops[GAME_LOOP] = 1;
                                        }else{
                                                // Timeout (15 sec)
                                                tCloseClient = 1;
                                                tCloseServer = 1;
                                        }

                                        _loops[LOCAL_MULTI_LOOP] = 1;
                                        break;

                                case LOCAL_MULTI_CLIENT:
                                        ipAddr = smalloc(sizeof(char), 16);
                                        ipAddr = inputTextMenu(INPUT_TEXT_IP_MODE);

                                        if (ipAddr == NULL) {
                                                break;
                                        }
                                        // Spawn client connecting to ip
                                        // Server needs to be started first to receive connections
                                        logger("Initializing client socket info...\n", LOGGER_INFO);
                                        printf("Connecting to %s\n", ipAddr);

                                        initSocketInfo(ipAddr);
                                        logger("Client socket info initialized\n", LOGGER_OK);

                                        logger("Creating socket thread...\n", LOGGER_INFO);
                                        pthread_create(&clientSocketThread, NULL, &clientGameSocket, &tClientSocketInfo);
                                        logger("Client socket thread started\n", LOGGER_OK);

                                        // START BOSS MODE LOOP
                                        printf("Launching boss mode\n");
                                        _playingBoss = 1;
                                        _multiplayer = 1;

                                        // load and launch chosen level

                                        _loops[LOCAL_MULTI_LOOP] = 1;
                                        _loops[GAME_LOOP]        = 1;
                                        break;

                                case LOCAL_MULTI_LEAVE:
                                        _loops[MULTIPLAYER_LOOP] = 0;
                                        _loops[GAME_LOOP]        = 0;
                                        _loops[LOCAL_MULTI_LOOP] = 1;
                                        break;
                                }
                                break;
                        }
                        break;

                case SDL_MOUSEMOTION:
                        if (event.motion.y < (_localMultiModeMenuTextures[LOCAL_MULTI_HOST].size.y + _localMultiModeMenuTextures[LOCAL_MULTI_HOST].size.h)) {
                                selectCursor = LOCAL_MULTI_HOST;
                        } else if ((event.motion.y >= _localMultiModeMenuTextures[LOCAL_MULTI_CLIENT].size.y) && event.motion.y < (_localMultiModeMenuTextures[LOCAL_MULTI_CLIENT].size.y + _localMultiModeMenuTextures[LOCAL_MULTI_CLIENT].size.h)) {
                                selectCursor = LOCAL_MULTI_CLIENT;
                        } else if ((event.motion.y >= _localMultiModeMenuTextures[LOCAL_MULTI_LEAVE].size.y) && event.motion.y < (_localMultiModeMenuTextures[LOCAL_MULTI_LEAVE].size.y + _localMultiModeMenuTextures[LOCAL_MULTI_LEAVE].size.h)) {
                                selectCursor = LOCAL_MULTI_LEAVE;
                        }
                        break;

                case SDL_MOUSEBUTTONDOWN:
                        switch (selectCursor) {
                        case LOCAL_MULTI_HOST:
                                if ((isHovering(_localMultiModeMenuTextures[LOCAL_MULTI_HOST], event.motion.x, event.motion.y))) {
                                        chosenLevel = atoi(inputTextMenu(INPUT_TEXT_SELECT_LEVEL_MODE));

                                        if (chosenLevel <= 0 || chosenLevel > 6) {
                                                break;
                                        }else{
                                                loadGame(chosenLevel);
                                        }

                                        // spawn server thread
                                        logger("Creating server thread...\n", LOGGER_INFO);
                                        pthread_create(&serverThread, NULL, &gameServer, NULL);
                                        logger("Client server thread started\n", LOGGER_OK);

                                        logger("Initializing client socket info...\n", LOGGER_INFO);
                                        ipAddr = smalloc(sizeof(char), 16);

                                        strcpy(ipAddr, "127.0.0.1");
                                        printf("Connecting to %s\n", ipAddr);

                                        initSocketInfo(ipAddr);
                                        logger("Client socket info initialized\n", LOGGER_OK);

                                        tGameInfo = chosenLevel;

                                        printf("Started thread with gameinfo %d\n", tGameInfo);

                                        logger("Creating socket thread...\n", LOGGER_INFO);
                                        pthread_create(&clientSocketThread, NULL, &clientGameSocket, &tClientSocketInfo);
                                        logger("Client socket thread started\n", LOGGER_OK);

                                        _playingChar = 1;
                                        _multiplayer = 1;

                                        if (renderWaitingScreen()) {
                                                // Client connected
                                                // load and launch chosen level
                                                _loops[GAME_LOOP] = 1;
                                        }else{
                                                // Timeout (15 sec)
                                                tCloseClient = 1;
                                                tCloseServer = 1;
                                        }

                                        _loops[LOCAL_MULTI_LOOP] = 1;
                                }
                                break;

                        case LOCAL_MULTI_CLIENT:
                                if ((isHovering(_localMultiModeMenuTextures[LOCAL_MULTI_CLIENT], event.motion.x, event.motion.y))) {
                                        ipAddr = smalloc(sizeof(char), 16);
                                        ipAddr = inputTextMenu(INPUT_TEXT_IP_MODE);

                                        if (ipAddr == NULL) {
                                                break;
                                        }
                                        // Spawn client connecting to ip
                                        // Server needs to be started first to receive connections
                                        logger("Initializing client socket info...\n", LOGGER_INFO);
                                        printf("Connecting to %s\n", ipAddr);

                                        initSocketInfo(ipAddr);
                                        logger("Client socket info initialized\n", LOGGER_OK);

                                        logger("Creating socket thread...\n", LOGGER_INFO);
                                        pthread_create(&clientSocketThread, NULL, &clientGameSocket, &tClientSocketInfo);
                                        logger("Client socket thread started\n", LOGGER_OK);

                                        // START BOSS MODE LOOP
                                        _playingBoss = 1;
                                        _multiplayer = 1;

                                        logger("Waiting game info from server...\n", LOGGER_INFO);

                                        serverWaitTimeout = SDL_GetTicks();

                                        while (tGameInfo == -1 && SDL_GetTicks() - serverWaitTimeout < SERVER_WAIT_TIMEOUT) {
                                                ;
                                        }

                                        logger("Received game info from server : ", LOGGER_OK);
                                        printf("%d\n", tGameInfo);

                                        if (tGameInfo == -1) {
                                                logger("No game info received from server\n", LOGGER_ERR);
                                                ipAddr[0] = 0x00;
                                        }else if (tGameInfo < 0 && tGameInfo > 6) {
                                                logger("Invalid game info received (level not in range)\n", LOGGER_ERR);
                                                ipAddr[0] = 0x00;
                                        }else{
                                                loadGame(tGameInfo);

                                                _loops[LOCAL_MULTI_LOOP] = 1;
                                                _loops[GAME_LOOP]        = 1;
                                        }
                                }
                                break;

                        case LOCAL_MULTI_LEAVE:
                                if ((isHovering(_localMultiModeMenuTextures[LOCAL_MULTI_LEAVE], event.motion.x, event.motion.y))) {
                                        _loops[MULTIPLAYER_LOOP] = 0;
                                        _loops[GAME_LOOP]        = 0;
                                        _loops[LOCAL_MULTI_LOOP] = 1;
                                }
                                break;
                        }
                        break;
                }
                changeLocalMultiModeCursor(selectCursor);
                renderMenu(_localMultiModeMenuTextures, LOCAL_MULTI_TOTAL);
        }

        if (ipAddr != NULL) {
                free(ipAddr);
        }
}

void changeLocalMultiModeCursor(int posCursor) {
        switch (posCursor) {
        case LOCAL_MULTI_HOST:
                _localMultiModeMenuTextures[LOCAL_MULTI_OPENING_CHEVRON].size.y = 190;
                _localMultiModeMenuTextures[LOCAL_MULTI_CLOSING_CHEVRON].size.y = 190;
                break;

        case LOCAL_MULTI_CLIENT:
                _localMultiModeMenuTextures[LOCAL_MULTI_OPENING_CHEVRON].size.y = 240;
                _localMultiModeMenuTextures[LOCAL_MULTI_CLOSING_CHEVRON].size.y = 240;
                break;

        case LOCAL_MULTI_LEAVE:
                _localMultiModeMenuTextures[LOCAL_MULTI_OPENING_CHEVRON].size.y = 390;
                _localMultiModeMenuTextures[LOCAL_MULTI_CLOSING_CHEVRON].size.y = 390;
                break;
        }
}

// ======================================================================================

void multiplayerMenu() {
        _loops[MULTIPLAYER_LOOP] = 0;
        int selectCursor = MULTI_LOCAL;
        SDL_Event event;
        renderMenu(_multiplayerMenuTextures, MULTI_TOTAL);

        while (!_loops[MULTIPLAYER_LOOP]) {
                SDL_WaitEvent(&event);
                switch (event.type) {
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_z:
                        case SDLK_UP:
                                selectCursor--;
                                if (selectCursor < MULTI_LOCAL) {
                                        selectCursor = MULTI_LEAVE;                                //Option = Leave
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_s:
                        case SDLK_DOWN:
                                selectCursor++;
                                if (selectCursor > MULTI_LEAVE) {
                                        selectCursor = MULTI_LOCAL;
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_RETURN:
                                switch (selectCursor) {
                                case MULTI_LOCAL:
                                        _loops[MULTIPLAYER_LOOP] = 1;
                                        localMultiModeMenu();
                                        break;

                                case MULTI_INTERNET:
                                        // spawn local/internet scene
                                        break;

                                case MULTI_LEAVE:
                                        _loops[GAME_MODE_LOOP]   = 0;
                                        _loops[PROGRAM_LOOP]     = 0;
                                        _loops[MULTIPLAYER_LOOP] = 1;
                                        break;
                                }
                                break;
                        }
                        break;

                case SDL_MOUSEMOTION:
                        if (event.motion.y < (_multiplayerMenuTextures[MULTI_LOCAL].size.y + _multiplayerMenuTextures[MULTI_LOCAL].size.h)) {
                                selectCursor = MULTI_LOCAL;
                        } else if ((event.motion.y >= _multiplayerMenuTextures[MULTI_INTERNET].size.y) && event.motion.y < (_multiplayerMenuTextures[MULTI_INTERNET].size.y + _multiplayerMenuTextures[MULTI_INTERNET].size.h)) {
                                selectCursor = MULTI_INTERNET;
                        } else if ((event.motion.y >= _multiplayerMenuTextures[MULTI_LEAVE].size.y) && event.motion.y < (_multiplayerMenuTextures[MULTI_LEAVE].size.y + _multiplayerMenuTextures[MULTI_LEAVE].size.h)) {
                                selectCursor = MULTI_LEAVE;
                        }
                        break;

                case SDL_MOUSEBUTTONDOWN:
                        switch (selectCursor) {
                        case MULTI_LOCAL:
                                _loops[MULTIPLAYER_LOOP] = 1;
                                localMultiModeMenu();
                                break;

                        case MULTI_INTERNET:
                                // spawn local/internet scene
                                break;

                        case MULTI_LEAVE:
                                _loops[GAME_LOOP]        = 0;
                                _loops[GAME_MODE_LOOP]   = 0;
                                _loops[MULTIPLAYER_LOOP] = 1;
                                break;
                        }
                        break;
                }
                changeMultiplayerPosCursor(selectCursor);
                renderMenu(_multiplayerMenuTextures, MULTI_TOTAL);
        }
}

void changeMultiplayerPosCursor(int posCursor) {
        switch (posCursor) {
        case MULTI_LOCAL:
                _multiplayerMenuTextures[MULTI_OPENING_CHEVRON].size.y = 190;
                _multiplayerMenuTextures[MULTI_CLOSING_CHEVRON].size.y = 190;
                break;

        case MULTI_INTERNET:
                _multiplayerMenuTextures[MULTI_OPENING_CHEVRON].size.y = 240;
                _multiplayerMenuTextures[MULTI_CLOSING_CHEVRON].size.y = 240;
                break;

        case MULTI_LEAVE:
                _multiplayerMenuTextures[MULTI_OPENING_CHEVRON].size.y = 390;
                _multiplayerMenuTextures[MULTI_CLOSING_CHEVRON].size.y = 390;
                break;
        }
}

// ======================================================================================

int confirmBox() {
        _loops[CONFIRM_LOOP] = 0;
        int selectCursor = CONFIRM_YES;
        SDL_Event event;
        renderMenu(_confirmBoxTextures, MULTI_TOTAL);

        while (!_loops[CONFIRM_LOOP]) {
                SDL_WaitEvent(&event);
                switch (event.type) {
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_z:
                        case SDLK_UP:
                                selectCursor--;
                                if (selectCursor < CONFIRM_YES) {
                                        selectCursor = CONFIRM_NO;                                //Option = Leave
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_s:
                        case SDLK_DOWN:
                                selectCursor++;
                                if (selectCursor > CONFIRM_NO) {
                                        selectCursor = CONFIRM_YES;
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_RETURN:
                                _loops[PLAY_OPTIONS_LOOP] = 1;

                                return selectCursor;

                                break;
                        }
                        break;

                case SDL_MOUSEMOTION:
                        if (event.motion.y < (_confirmBoxTextures[CONFIRM_YES].size.y + _confirmBoxTextures[CONFIRM_YES].size.h)) {
                                selectCursor = CONFIRM_YES;
                        } else if ((event.motion.y >= _confirmBoxTextures[CONFIRM_NO].size.y) && event.motion.y < (_confirmBoxTextures[CONFIRM_NO].size.y + _confirmBoxTextures[CONFIRM_NO].size.h)) {
                                selectCursor = CONFIRM_NO;
                        }
                        break;

                case SDL_MOUSEBUTTONDOWN:
                        if ((isHovering(_confirmBoxTextures[CONFIRM_YES], event.motion.x, event.motion.y)) || (isHovering(_confirmBoxTextures[CONFIRM_NO], event.motion.x, event.motion.y))) {
                                return selectCursor;
                        }
                        break;
                }
                changeConfirmBoxPosCursor(selectCursor);
                renderMenu(_confirmBoxTextures, CONFIRM_TOTAL);
        }

        return -1;
}

void changeConfirmBoxPosCursor(int posCursor) {
        switch (posCursor) {
        case CONFIRM_YES:
                _confirmBoxTextures[CONFIRM_CHEVRON].size.y = 190;
                break;

        case CONFIRM_NO:
                _confirmBoxTextures[CONFIRM_CHEVRON].size.y = 240;
                break;
        }
}

// ======================================================================================

void playOptionsBox(int currentSaveId) {
        int res = -1;

        char *newName = smalloc(sizeof(char), 51);

        _loops[PLAY_OPTIONS_LOOP] = 0;
        int selectCursor = PLAY_CONTINUE;
        SDL_Event event;
        renderMenu(_playOptionsBoxTextures, PLAY_TOTAL);

        while (!_loops[PLAY_OPTIONS_LOOP]) {
                SDL_WaitEvent(&event);
                switch (event.type) {
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_z:
                        case SDLK_UP:
                                selectCursor--;
                                if (selectCursor < PLAY_CONTINUE) {
                                        selectCursor = PLAY_LEAVE;                                  //Option = Leave
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_s:
                        case SDLK_DOWN:
                                selectCursor++;
                                if (selectCursor > PLAY_LEAVE) {
                                        selectCursor = PLAY_CONTINUE;
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_RETURN:
                                switch (selectCursor) {
                                case PLAY_CONTINUE:
                                        toggleBlackscreenFade(FADE_IN);
                                        renderBlackscreenMenu(_playOptionsBoxTextures, PLAY_TOTAL);

                                        loadGame(currentSaveId);

                                        _loops[GAME_LOOP]         = 1;
                                        _loops[LOAD_CHAR_LOOP]    = 1;
                                        _loops[GAME_MODE_LOOP]    = 1;
                                        _loops[PLAY_OPTIONS_LOOP] = 1;
                                        break;

                                case PLAY_RESET:
                                        res = confirmBox();
                                        if (res == -1) {
                                                logger("Something went wrong with the confirmation dialog", LOGGER_FAIL);
                                        }else if (res == CONFIRM_YES) {
                                                resetSave(currentSaveId);
                                                newName = inputTextMenu(INPUT_TEXT_NAME_MODE);
                                                appendStr(createSaveStr(LOAD_SAVE, currentSaveId), newName);
                                                crossUnlink(createSaveStr(NEW_FILE, currentSaveId));
                                                loadGame(currentSaveId);
                                                _loops[PLAY_OPTIONS_LOOP] = 1;
                                                // start char creation
                                        }else if (res == CONFIRM_NO) {
                                                _loops[PLAY_OPTIONS_LOOP] = 0;
                                        }
                                        break;

                                case PLAY_DELETE:
                                        res = confirmBox();
                                        if (res == -1) {
                                                logger("Something went wrong with the confirmation dialog", LOGGER_FAIL);
                                        }else if (res == CONFIRM_YES) {
                                                resetSave(currentSaveId);
                                                _loops[PLAY_OPTIONS_LOOP] = 1;
                                        }else if (res == CONFIRM_NO) {
                                                _loops[PLAY_OPTIONS_LOOP] = 0;
                                        }

                                        break;

                                case PLAY_LEAVE:
                                        _loops[GAME_LOOP]         = 0;
                                        _loops[LOAD_CHAR_LOOP]    = 0;
                                        _loops[PLAY_OPTIONS_LOOP] = 1;
                                        break;
                                }
                        }
                        break;

                case SDL_MOUSEMOTION:
                        if (event.motion.y < (_playOptionsBoxTextures[PLAY_CONTINUE].size.y + _playOptionsBoxTextures[PLAY_CONTINUE].size.h)) {
                                selectCursor = PLAY_CONTINUE;
                        } else if ((event.motion.y >= _playOptionsBoxTextures[PLAY_RESET].size.y) && event.motion.y < (_playOptionsBoxTextures[PLAY_RESET].size.y + _playOptionsBoxTextures[PLAY_RESET].size.h)) {
                                selectCursor = PLAY_RESET;
                        }else if ((event.motion.y >= _playOptionsBoxTextures[PLAY_DELETE].size.y) && event.motion.y < (_playOptionsBoxTextures[PLAY_DELETE].size.y + _playOptionsBoxTextures[PLAY_DELETE].size.h)) {
                                selectCursor = PLAY_DELETE;
                        }else if ((event.motion.y >= _playOptionsBoxTextures[PLAY_LEAVE].size.y) && event.motion.y < (_playOptionsBoxTextures[PLAY_LEAVE].size.y + _playOptionsBoxTextures[PLAY_LEAVE].size.h)) {
                                selectCursor = PLAY_LEAVE;
                        }
                        break;

                case SDL_MOUSEBUTTONDOWN:
                        switch (selectCursor) {
                        case PLAY_CONTINUE:
                                if ((isHovering(_playOptionsBoxTextures[PLAY_CONTINUE], event.motion.x, event.motion.y))) {
                                        toggleBlackscreenFade(FADE_IN);
                                        renderBlackscreenMenu(_playOptionsBoxTextures, PLAY_TOTAL);

                                        loadGame(currentSaveId);

                                        _loops[GAME_LOOP]         = 1;
                                        _loops[LOAD_CHAR_LOOP]    = 1;
                                        _loops[GAME_MODE_LOOP]    = 1;
                                        _loops[PLAY_OPTIONS_LOOP] = 1;
                                }
                                break;

                        case PLAY_RESET:
                                if ((isHovering(_playOptionsBoxTextures[PLAY_CONTINUE], event.motion.x, event.motion.y))) {
                                        res = confirmBox();
                                        if (res == -1) {
                                                logger("Something went wrong with the confirmation dialog", LOGGER_FAIL);
                                        }else if (res == CONFIRM_YES) {
                                                resetSave(currentSaveId);
                                                newName = inputTextMenu(INPUT_TEXT_NAME_MODE);
                                                appendStr(createSaveStr(LOAD_SAVE, currentSaveId), newName);
                                                crossUnlink(createSaveStr(NEW_FILE, currentSaveId));
                                                loadGame(currentSaveId);
                                                _loops[PLAY_OPTIONS_LOOP] = 1;
                                                // start char creation
                                        }else if (res == CONFIRM_NO) {
                                                _loops[PLAY_OPTIONS_LOOP] = 0;
                                        }
                                }
                                break;

                        case PLAY_DELETE:
                                if ((isHovering(_playOptionsBoxTextures[PLAY_DELETE], event.motion.x, event.motion.y))) {
                                        res = confirmBox();
                                        if (res == -1) {
                                                logger("Something went wrong with the confirmation dialog", LOGGER_FAIL);
                                        }else if (res == CONFIRM_YES) {
                                                resetSave(currentSaveId);
                                        }else if (res == CONFIRM_NO) {
                                                _loops[PLAY_OPTIONS_LOOP] = 0;
                                        }
                                }
                                break;

                        case PLAY_LEAVE:
                                if ((isHovering(_playOptionsBoxTextures[PLAY_LEAVE], event.motion.x, event.motion.y))) {
                                        _loops[GAME_LOOP]         = 0;
                                        _loops[LOAD_CHAR_LOOP]    = 0;
                                        _loops[PLAY_OPTIONS_LOOP] = 1;
                                }
                                break;
                        }
                        break;
                }
                changePlayOptionsBoxCursor(selectCursor);
                renderMenu(_playOptionsBoxTextures, PLAY_TOTAL);
        }

        if (newName != NULL) {
                free(newName);
        }
}

void changePlayOptionsBoxCursor(int posCursor) {
        switch (posCursor) {
        case PLAY_CONTINUE:
                _playOptionsBoxTextures[PLAY_CHEVRON].size.y = 190;
                break;

        case PLAY_RESET:
                _playOptionsBoxTextures[PLAY_CHEVRON].size.y = 240;
                break;

        case PLAY_DELETE:
                _playOptionsBoxTextures[PLAY_CHEVRON].size.y = 290;
                break;

        case PLAY_LEAVE:
                _playOptionsBoxTextures[PLAY_CHEVRON].size.y = 390;
                break;
        }
}

// ======================================================================================

char *inputTextMenu(int mode) {
        int i   = 0;
        char *str = smalloc(sizeof(char), 51);

        for (i = 0; i < 50; i++) {
                str[i] = 0x00;
        }

        char charBuffer = 0x00;
        int len        = 0;
        int minLength  = -1;
        int maxLength  = -1;

        _loops[INPUT_TEXT_LOOP] = 0;
        int selectCursor = INPUT_VALIDATE;
        SDL_Event event;

        renderMenu(_inputTextMenuTextures, INPUT_TOTAL);

        switch (mode) {
        case INPUT_TEXT_NAME_MODE:
                addMenuOption("Nom du personnage :", SCREEN_WIDTH / 2, 175, 250, 50, color_white, _inputTextMenuTextures, INPUT_ASK);
                maxLength = 50;
                minLength = 1;
                break;

        case INPUT_TEXT_IP_MODE:
                addMenuOption("IP du serveur :", SCREEN_WIDTH / 2, 175, 250, 50, color_white, _inputTextMenuTextures, INPUT_ASK);
                maxLength = 15;
                minLength = 8;

                break;

        case INPUT_TEXT_SELECT_LEVEL_MODE:
                maxLength = 1;
                minLength = 1;
                addMenuOption("Entrez un niveau :", SCREEN_WIDTH / 2, 175, 250, 50, color_white, _inputTextMenuTextures, INPUT_ASK);
                break;
        }

        while (!_loops[INPUT_TEXT_LOOP]) {
                SDL_WaitEvent(&event);
                str[len] = '\0';

                switch (event.type) {
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        // Prevents more than 50 characters and less than 1 for the name and between 8 and 15 (including dots) for the ip
                        case SDLK_LEFT:
                                selectCursor--;
                                if (selectCursor < INPUT_VALIDATE) {
                                        selectCursor = INPUT_VALIDATE;                                   //Option = Leave
                                }
                                //  SDL_Delay(100);
                                break;

                        case SDLK_RIGHT:
                                selectCursor++;
                                if (selectCursor > INPUT_LEAVE) {
                                        selectCursor = INPUT_LEAVE;
                                }
                                //  SDL_Delay(100);
                                break;

                        case SDLK_RETURN:
                                switch (selectCursor) {
                                case INPUT_LEAVE:
                                        _loops[INPUT_TEXT_LOOP] = 1;
                                        str = NULL;

                                case INPUT_VALIDATE:
                                        _loops[INPUT_TEXT_LOOP] = 1;
                                        break;
                                }
                                break;

                        case SDLK_BACKSPACE:
                                if (len > 0) {
                                        len--;
                                        popStr(str, len, 0);
                                        addMenuOption(str, SCREEN_WIDTH / 2, 225, 250, 50, color_white, _inputTextMenuTextures, INPUT_TEXT);
                                }
                                break;

                        default:
                                if (strlen(str) >= maxLength && strlen(str) <= minLength) {
                                        break;
                                }

                                charBuffer = convertFomSDLKeyCode(event.key.keysym.sym);

                                if (mode == INPUT_TEXT_SELECT_LEVEL_MODE && (atoi(&charBuffer) <= 0 || atoi(&charBuffer) > 6)) {
                                        break;
                                }

                                if (charBuffer != -1) {
                                        str[len] = charBuffer;
                                        len++;
                                }

                                addMenuOption(str, SCREEN_WIDTH / 2, 225, 250, 50, color_white, _inputTextMenuTextures, INPUT_TEXT);
                                break;
                        }
                        break;

                case SDL_MOUSEMOTION:
                        if (event.motion.x < (_inputTextMenuTextures[INPUT_VALIDATE].size.x + _inputTextMenuTextures[INPUT_VALIDATE].size.w)) {
                                selectCursor = INPUT_VALIDATE;
                        } else if ((event.motion.x >= _inputTextMenuTextures[INPUT_LEAVE].size.x) && event.motion.x < (_inputTextMenuTextures[INPUT_LEAVE].size.x + _inputTextMenuTextures[INPUT_LEAVE].size.w)) {
                                selectCursor = INPUT_LEAVE;
                        }
                        break;

                case SDL_MOUSEBUTTONDOWN:
                        switch (selectCursor) {
                        case INPUT_LEAVE:
                                if ((isHovering(_inputTextMenuTextures[INPUT_LEAVE], event.motion.x, event.motion.y))) {
                                        _loops[INPUT_TEXT_LOOP] = 1;
                                        str = NULL;
                                }
                                break;

                        case INPUT_VALIDATE:
                                if ((isHovering(_inputTextMenuTextures[INPUT_VALIDATE], event.motion.x, event.motion.y))) {
                                        _loops[INPUT_TEXT_LOOP] = 1;
                                }
                                break;
                        }
                        break;
                }
                changeInputTextMenuCursor(selectCursor);
                renderMenu(_inputTextMenuTextures, PLAY_TOTAL);
        }

        return str;
}

void refreshDisplayStr(MenuStruct *menuArray, int arrayId, char *str) {
        if (strlen(str) > 50) {
                return;
        }

        addMenuOption(str, SCREEN_WIDTH / 2, 225, 250, 50, color_white, menuArray, arrayId);
}

void refreshVolumeValue(MenuStruct *menu, int arrayId, char *str) {
        int y = 0;

        switch (arrayId) {
        case SOUND_MENU_MUSIC_VOLUME_VALUE:
                y = 100;
                break;

        case SOUND_MENU_EFFECTS_VOLUME_VALUE:
                y = 200;
                break;

        case SOUND_MENU_MAIN_VOLUME_VALUE:
                y = 300;
                break;
        }
        addMenuOption(str, SCREEN_WIDTH - SCREEN_WIDTH / 6, y, 100, 50, color_white, _soundMenuTextures, arrayId);
}

void changeInputTextMenuCursor(int posCursor) {
        switch (posCursor) {
        case INPUT_VALIDATE:
                _inputTextMenuTextures[INPUT_CHEVRON].size.x = SCREEN_WIDTH / 12;
                break;

        case INPUT_LEAVE:
                _inputTextMenuTextures[INPUT_CHEVRON].size.x = SCREEN_WIDTH - (SCREEN_WIDTH / 8 + SCREEN_WIDTH / 4 + _inputTextMenuTextures[INPUT_CHEVRON].size.w);
                break;
        }
}

// ======================================================================================

void wallSafeCursor(MenuStruct *cursor, int ballFlag, int barFlag) {
        if (cursor->size.y < (_soundMenuTextures[ballFlag].size.y + _soundMenuTextures[ballFlag].size.h)) {
                if (_mouse[MOUSE_X] <= _soundMenuTextures[barFlag].size.x - SLIDER_BALL_WIDTH / 2) {
                        _soundMenuTextures[ballFlag].size.x = _soundMenuTextures[barFlag].size.x - SLIDER_BALL_WIDTH / 2;
                }else if (_soundMenuTextures[ballFlag].size.x >= (_soundMenuTextures[barFlag].size.x + _soundMenuTextures[barFlag].size.w) - SLIDER_BALL_WIDTH / 2) {
                        _soundMenuTextures[ballFlag].size.x = (_soundMenuTextures[barFlag].size.x + _soundMenuTextures[barFlag].size.w) - SLIDER_BALL_WIDTH / 2;
                }
        }
}

void soundMenu() {
        _loops[SOUND_MENU_LOOP] = 0;
        int selectCursor = SOUND_MENU_MUSIC_PLAY;
        int buffer       = -1;
        int actualVolume = -1;
        SDL_Event event;
        char bufferStr[4] = { 0x00 };

        renderMenu(_soundMenuTextures, SOUND_MENU_TOTAL);

        while (!_loops[SOUND_MENU_LOOP]) {
                SDL_WaitEvent(&event);

                switch (event.type) {
                case SDL_MOUSEBUTTONUP:
                        if (!(_isSliderBallClicked)) {
                                break;
                        }
                        if (event.motion.y < (_soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL].size.y + _soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL].size.h)) {
                                _soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL].size.x = _mouse[MOUSE_X] - (SLIDER_BALL_WIDTH / 2);
                                wallSafeCursor(&_soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL], SOUND_MENU_MUSIC_SLIDER_BALL, SOUND_MENU_MUSIC_SLIDER_BAR);

                                buffer       = (int)getPercentFromVal(_soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL].size.x - (SCREEN_WIDTH / 4), _soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BAR].size.w) + 4;
                                actualVolume = getValFromPercent(buffer, 128);

                                sprintf(bufferStr, "%d", buffer);
                                refreshVolumeValue(_soundMenuTextures, SOUND_MENU_MUSIC_VOLUME_VALUE, bufferStr);

                                _gameParams[CONFIG_MUSIC_VOLUME] = actualVolume;
                                applySoundParams();
                                _isSliderBallClicked = 0;
                                setParam(CONFIG_MUSIC_VOLUME, actualVolume);
                                break;
                        }else if (event.motion.y < (_soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL].size.y + _soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL].size.h)) {
                                _soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL].size.x = _mouse[MOUSE_X] - (SLIDER_BALL_WIDTH / 2);
                                wallSafeCursor(&_soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL], SOUND_MENU_EFFECTS_SLIDER_BALL, SOUND_MENU_EFFECTS_SLIDER_BAR);

                                buffer = (int)getPercentFromVal(_soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL].size.x - (SCREEN_WIDTH / 4), _soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BAR].size.w) + 4;

                                sprintf(bufferStr, "%d", buffer);
                                refreshVolumeValue(_soundMenuTextures, SOUND_MENU_EFFECTS_VOLUME_VALUE, bufferStr);

                                actualVolume = getValFromPercent(buffer, 128);

                                _gameParams[CONFIG_SOUND_EFFECTS_VOLUME] = actualVolume;
                                applySoundParams();
                                _isSliderBallClicked = 0;
                                setParam(CONFIG_SOUND_EFFECTS_VOLUME, actualVolume);
                                break;
                        }else if (event.motion.y < (_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL].size.y + _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL].size.h)) {
                                _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL].size.x = _mouse[MOUSE_X] - (SLIDER_BALL_WIDTH / 2);
                                wallSafeCursor(&_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL], SOUND_MENU_MAIN_VOLUME_SLIDER_BALL, SOUND_MENU_MAIN_VOLUME_SLIDER_BAR);

                                buffer = (int)getPercentFromVal(_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL].size.x - (SCREEN_WIDTH / 4), _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BAR].size.w) + 4;

                                sprintf(bufferStr, "%d", buffer);
                                refreshVolumeValue(_soundMenuTextures, SOUND_MENU_MAIN_VOLUME_VALUE, bufferStr);


                                actualVolume = getValFromPercent(buffer, 128);

                                _gameParams[CONFIG_MAIN_VOLUME] = actualVolume;
                                applySoundParams();
                                _isSliderBallClicked = 0;
                                setParam(CONFIG_MAIN_VOLUME, actualVolume);
                                break;
                        }

                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_DOWN:
                                selectCursor++;
                                if (selectCursor > SOUND_MENU_LEAVE) {
                                        selectCursor = SOUND_MENU_MUSIC_PLAY;
                                }
                                break;

                        case SDLK_UP:
                                selectCursor--;
                                if (selectCursor < SOUND_MENU_MUSIC_PLAY) {
                                        selectCursor = SOUND_MENU_LEAVE;
                                }
                                break;

                        case SDLK_RETURN:
                                switch (selectCursor) {
                                case SOUND_MENU_MUSIC_PLAY:
                                        if (_soundMenuTextures[SOUND_MENU_MUSIC_PLAY_PICTURE].isVisible) {
                                                _soundMenuTextures[SOUND_MENU_MUSIC_PLAY].isVisible++;
                                                _soundMenuTextures[SOUND_MENU_MUSIC_PLAY_PICTURE].isVisible--;
                                                toggleSound(MUSIC);
                                                applySoundParams();
                                                setParam(CONFIG_MUSIC_PLAY, 0);
                                        }else {
                                                _soundMenuTextures[SOUND_MENU_MUSIC_PLAY].isVisible--;
                                                _soundMenuTextures[SOUND_MENU_MUSIC_PLAY_PICTURE].isVisible++;
                                                toggleSound(MUSIC);
                                                applySoundParams();
                                                setParam(CONFIG_MUSIC_PLAY, 1);
                                        }
                                        break;

                                case SOUND_MENU_SOUND_EFFECTS_PLAY:
                                        if (_soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY_PICTURE].isVisible) {
                                                _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].isVisible++;
                                                _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY_PICTURE].isVisible--;
                                                toggleSound(EFFECTS);
                                                applySoundParams();
                                                setParam(CONFIG_SOUND_EFFECTS_PLAY, 0);
                                        }else {
                                                _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].isVisible--;
                                                _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY_PICTURE].isVisible++;
                                                toggleSound(EFFECTS);
                                                applySoundParams();
                                                setParam(CONFIG_SOUND_EFFECTS_PLAY, 1);
                                        }

                                        break;

                                case SOUND_MENU_MAIN_VOLUME_PLAY:
                                        if (_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY_PICTURE].isVisible) {
                                                _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].isVisible++;
                                                _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY_PICTURE].isVisible--;
                                                toggleSound(SOUND);
                                                applySoundParams();
                                                setParam(CONFIG_MAIN_VOLUME_PLAY, 0);
                                        }else {
                                                _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].isVisible--;
                                                _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY_PICTURE].isVisible++;
                                                toggleSound(SOUND);
                                                applySoundParams();
                                                setParam(CONFIG_MAIN_VOLUME_PLAY, 1);
                                        }
                                        break;

                                case SOUND_MENU_LEAVE:
                                        _loops[SOUND_MENU_LOOP] = 1;
                                        break;
                                }
                                break;
                        }
                        break;

                case SDL_MOUSEMOTION:
                        _mouse[MOUSE_X] = event.button.x;
                        _mouse[MOUSE_Y] = event.button.y;

                        if (event.motion.y < (_soundMenuTextures[SOUND_MENU_MUSIC_PLAY].size.y + _soundMenuTextures[SOUND_MENU_MUSIC_PLAY].size.h)) {
                                selectCursor = SOUND_MENU_MUSIC_PLAY;
                        } else if ((event.motion.y >= _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].size.y) && event.motion.y < (_soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].size.y + _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].size.h)) {
                                selectCursor = SOUND_MENU_SOUND_EFFECTS_PLAY;
                        } else if ((event.motion.y >= _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].size.y) && event.motion.y < (_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].size.y + _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].size.h)) {
                                selectCursor = SOUND_MENU_MAIN_VOLUME_PLAY;
                        } else if ((event.motion.y >= _soundMenuTextures[SOUND_MENU_LEAVE].size.y) && event.motion.y < (_soundMenuTextures[SOUND_MENU_LEAVE].size.y + _soundMenuTextures[SOUND_MENU_LEAVE].size.h)) {
                                selectCursor = SOUND_MENU_LEAVE;
                        }

                        if ((_isSliderBallClicked)) {
                                if (event.motion.y < (_soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL].size.y + _soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL].size.h) && event.motion.y > (_soundMenuTextures[SOUND_MENU_MUSIC_PLAY].size.y + _soundMenuTextures[SOUND_MENU_MUSIC_PLAY].size.h)) {
                                        _soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL].size.x = _mouse[MOUSE_X] - (SLIDER_BALL_WIDTH / 2);
                                        wallSafeCursor(&_soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL], SOUND_MENU_MUSIC_SLIDER_BALL, SOUND_MENU_MUSIC_SLIDER_BAR);

                                        sprintf(bufferStr, "%d", (int)getPercentFromVal(_soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL].size.x - (SCREEN_WIDTH / 4), _soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BAR].size.w) + 4);
                                        refreshVolumeValue(_soundMenuTextures, SOUND_MENU_MUSIC_VOLUME_VALUE, bufferStr);
                                }else if (event.motion.y < (_soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL].size.y + _soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL].size.h) && event.motion.y > (_soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].size.y + _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].size.h)) {
                                        _soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL].size.x = _mouse[MOUSE_X] - (SLIDER_BALL_WIDTH / 2);
                                        wallSafeCursor(&_soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL], SOUND_MENU_EFFECTS_SLIDER_BALL, SOUND_MENU_EFFECTS_SLIDER_BAR);

                                        sprintf(bufferStr, "%d", (int)getPercentFromVal(_soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL].size.x - (SCREEN_WIDTH / 4), _soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BAR].size.w) + 4);
                                        refreshVolumeValue(_soundMenuTextures, SOUND_MENU_EFFECTS_VOLUME_VALUE, bufferStr);
                                }else if (event.motion.y < (_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL].size.y + _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL].size.h) && event.motion.y > (_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].size.y + _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].size.h)) {
                                        _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL].size.x = _mouse[MOUSE_X] - (SLIDER_BALL_WIDTH / 2);
                                        wallSafeCursor(&_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL], SOUND_MENU_MAIN_VOLUME_SLIDER_BALL, SOUND_MENU_MAIN_VOLUME_SLIDER_BAR);

                                        sprintf(bufferStr, "%d", (int)getPercentFromVal(_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL].size.x - (SCREEN_WIDTH / 4), _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BAR].size.w) + 4);
                                        refreshVolumeValue(_soundMenuTextures, SOUND_MENU_MAIN_VOLUME_VALUE, bufferStr);
                                }
                        }

                        break;

                case SDL_MOUSEBUTTONDOWN:
                        if ((isHovering(_soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL], event.motion.x, event.motion.y))) {
                                _isSliderBallClicked = 1;
                                break;
                        }else if ((isHovering(_soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL], event.motion.x, event.motion.y))) {
                                _isSliderBallClicked = 1;
                                break;
                        }else if ((isHovering(_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL], event.motion.x, event.motion.y))) {
                                _isSliderBallClicked = 1;
                                break;
                        }

/*
 *          if (event.motion.y < (_soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL].size.y + _soundMenuTextures[SOUND_MENU_MUSIC_SLIDER_BALL].size.h) && event.motion.y > (_soundMenuTextures[SOUND_MENU_MUSIC_PLAY].size.y + _soundMenuTextures[SOUND_MENU_MUSIC_PLAY].size.h)) {
 *              break;
 *          }else if (event.motion.y < (_soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL].size.y + _soundMenuTextures[SOUND_MENU_EFFECTS_SLIDER_BALL].size.h) && event.motion.y > (_soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].size.y + _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].size.h)) {
 *              _isSliderBallClicked = 1;
 *              break;
 *          }else if (event.motion.y < (_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL].size.y + _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_SLIDER_BALL].size.h) && event.motion.y > (_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].size.y + _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].size.h)) {
 *              _isSliderBallClicked = 1;
 *              break;
 *          }
 */
                        switch (selectCursor) {
                        case SOUND_MENU_MUSIC_PLAY:
                                if ((isHovering(_soundMenuTextures[SOUND_MENU_MUSIC_PLAY], event.motion.x, event.motion.y))) {
                                        if (_soundMenuTextures[SOUND_MENU_MUSIC_PLAY_PICTURE].isVisible) {
                                                _soundMenuTextures[SOUND_MENU_MUSIC_PLAY].isVisible++;
                                                _soundMenuTextures[SOUND_MENU_MUSIC_PLAY_PICTURE].isVisible--;
                                                toggleSound(MUSIC);
                                                applySoundParams();
                                                setParam(CONFIG_MUSIC_PLAY, 0);
                                        }else {
                                                _soundMenuTextures[SOUND_MENU_MUSIC_PLAY].isVisible--;
                                                _soundMenuTextures[SOUND_MENU_MUSIC_PLAY_PICTURE].isVisible++;
                                                toggleSound(MUSIC);
                                                applySoundParams();
                                                setParam(CONFIG_MUSIC_PLAY, 1);
                                        }
                                }
                                break;

                        case SOUND_MENU_SOUND_EFFECTS_PLAY:
                                if ((isHovering(_soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY], event.motion.x, event.motion.y))) {
                                        if (_soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY_PICTURE].isVisible) {
                                                _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].isVisible++;
                                                _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY_PICTURE].isVisible--;
                                                toggleSound(EFFECTS);
                                                applySoundParams();
                                                setParam(CONFIG_SOUND_EFFECTS_PLAY, 0);
                                        }else {
                                                _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY].isVisible--;
                                                _soundMenuTextures[SOUND_MENU_SOUND_EFFECTS_PLAY_PICTURE].isVisible++;
                                                toggleSound(EFFECTS);
                                                applySoundParams();
                                                setParam(CONFIG_SOUND_EFFECTS_PLAY, 1);
                                        }
                                }
                                break;

                        case SOUND_MENU_MAIN_VOLUME_PLAY:
                                if ((isHovering(_soundMenuTextures[SOUND_MENU_MUSIC_PLAY], event.motion.x, event.motion.y))) {
                                        if (_soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY_PICTURE].isVisible) {
                                                _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].isVisible++;
                                                _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY_PICTURE].isVisible--;
                                                toggleSound(SOUND);
                                                applySoundParams();
                                                setParam(CONFIG_MAIN_VOLUME_PLAY, 0);
                                        }else {
                                                _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY].isVisible--;
                                                _soundMenuTextures[SOUND_MENU_MAIN_VOLUME_PLAY_PICTURE].isVisible++;
                                                toggleSound(SOUND);
                                                applySoundParams();
                                                setParam(CONFIG_MAIN_VOLUME_PLAY, 1);
                                        }
                                }
                                break;

                        case SOUND_MENU_LEAVE:
                                if ((isHovering(_soundMenuTextures[SOUND_MENU_LEAVE], event.motion.x, event.motion.y))) {
                                        _loops[SOUND_MENU_LOOP] = 1;
                                }
                                break;
                        }
                        break;
                }
                changeSoundMenuCursor(selectCursor);
                renderMenu(_soundMenuTextures, SOUND_MENU_TOTAL);
        }
}

void changeSoundMenuCursor(int posCursor) {
        switch (posCursor) {
        case SOUND_MENU_MUSIC_PLAY:
                _soundMenuTextures[SOUND_MENU_CHEVRON].size.y = 60;
                break;

        case SOUND_MENU_SOUND_EFFECTS_PLAY:
                _soundMenuTextures[SOUND_MENU_CHEVRON].size.y = 160;
                break;

        case SOUND_MENU_MAIN_VOLUME_PLAY:
                _soundMenuTextures[SOUND_MENU_CHEVRON].size.y = 260;
                break;

        case SOUND_MENU_LEAVE:
                _soundMenuTextures[SOUND_MENU_CHEVRON].size.y = 360;
                break;
        }
}

//========================================================================================================

void playerDeadMenu() {
        SDL_Event event;

        _loops[PLAYER_DEAD_MENU_LOOP] = 0;

        int selectCursor = PLAYER_RETRY;

        setBlackscreenTimer(0, 100);
        //toggleBlackscreenFade(FADE_OUT);
        //renderMenu(_playerDeadMenu, PLAYER_TOTAL_MENU);

        while (!_loops[PLAYER_DEAD_MENU_LOOP]) {
                SDL_WaitEvent(&event);
                switch (event.type) {
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_ESCAPE:
                                _loops[PLAYER_DEAD_MENU_LOOP] = 1;
                                break;

                        case SDLK_z:
                        case SDLK_UP:
                                selectCursor--;
                                if (selectCursor < PLAYER_RETRY) {
                                        selectCursor = PLAYER_LEAVE;
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_s:
                        case SDLK_DOWN:
                                selectCursor++;
                                if (selectCursor > PLAYER_LEAVE) {
                                        selectCursor = PLAYER_RETRY;
                                }
                                SDL_Delay(100);
                                break;

                        case SDLK_RETURN:
                                switch (selectCursor) {
                                case PLAYER_RETRY:
                                        freePointer();
                                        loadGame(_currentSaveId);
                                        _loops[PLAYER_DEAD_MENU_LOOP] = 1;
                                        _loops[MAIN_MENU_LOOP]        = 1;
                                        break;

                                case PLAYER_BACK_TO_MENU:
                                        freePointer();
                                        _loops[PROGRAM_LOOP]          = 0;
                                        _loops[GAME_LOOP]             = 0;
                                        _loops[PLAYER_DEAD_MENU_LOOP] = 1;
                                        break;

                                case PLAYER_LEAVE:
                                        closeSDL();
                                        exit(1);
                                        _loops[PLAYER_DEAD_MENU_LOOP] = 1;
                                        break;
                                }
                                break;

                        default:
                                break;
                        }
                        break;

                case SDL_MOUSEMOTION:
                        if (event.motion.y < (_playerDeadMenu[PLAYER_RETRY].size.y + _playerDeadMenu[PLAYER_RETRY].size.h)) {
                                selectCursor = PLAYER_RETRY;
                        } else if ((event.motion.y >= _playerDeadMenu[PLAYER_BACK_TO_MENU].size.y) && event.motion.y < (_playerDeadMenu[PLAYER_BACK_TO_MENU].size.y + _playerDeadMenu[PLAYER_BACK_TO_MENU].size.h)) {
                                selectCursor = PLAYER_BACK_TO_MENU;
                        } else if ((event.motion.y >= _playerDeadMenu[PLAYER_LEAVE].size.y) && event.motion.y < (_playerDeadMenu[PLAYER_LEAVE].size.y + _playerDeadMenu[PLAYER_LEAVE].size.h)) {
                                selectCursor = PLAYER_LEAVE;
                        }
                        break;

                case SDL_MOUSEBUTTONDOWN:
                        switch (selectCursor) {
                        case PLAYER_RETRY:
                                freePointer();
                                loadGame(_currentSaveId);
                                _loops[PLAYER_DEAD_MENU_LOOP] = 1;
                                _loops[MAIN_MENU_LOOP]        = 1;
                                break;

                        case PLAYER_BACK_TO_MENU:
                                freePointer();
                                _loops[PROGRAM_LOOP]          = 0;
                                _loops[GAME_LOOP]             = 0;
                                _loops[PLAYER_DEAD_MENU_LOOP] = 1;
                                break;

                        case PLAYER_LEAVE:
                                closeSDL();
                                exit(1);
                                _loops[PLAYER_DEAD_MENU_LOOP] = 1;
                                break;
                        }
                        break;

                default:
                        break;
                }
                changePlayerPosCursor(selectCursor);
                renderMenu(_playerDeadMenu, IG_TOTAL_MENU);
        }
}

void changePlayerPosCursor(int posCursor) {
        switch (posCursor) {
        case PLAYER_RETRY:
                _playerDeadMenu[PLAYER_OPENING_CHEVRON].size.y = 190;
                _playerDeadMenu[PLAYER_CLOSING_CHEVRON].size.y = 190;
                break;

        case PLAYER_BACK_TO_MENU:
                _playerDeadMenu[PLAYER_OPENING_CHEVRON].size.y = 240;
                _playerDeadMenu[PLAYER_CLOSING_CHEVRON].size.y = 240;
                break;

        case PLAYER_LEAVE:
                _playerDeadMenu[PLAYER_OPENING_CHEVRON].size.y = 390;
                _playerDeadMenu[PLAYER_CLOSING_CHEVRON].size.y = 390;
                break;
        }
}

void addMenuOption(char *message, int x, int y, int w, int h, SDL_Color color, MenuStruct *menu, int param) {
        SDL_Texture *textureMessage;
        SDL_Surface *surfaceMessage;
        SDL_Rect sizeMessage;

        surfaceMessage = TTF_RenderText_Solid(_menuFont, message, color);
        w = strlen(message) * FONT_WIDTH;

        sizeMessage.y = y;
        sizeMessage.x = x - (w / 2);
        sizeMessage.w = w;
        sizeMessage.h = h;

        textureMessage = SDL_CreateTextureFromSurface(_renderer, surfaceMessage);

        menu[param].texture   = textureMessage;
        menu[param].size      = sizeMessage;
        menu[param].isVisible = 1;
}

void addMenuTexture(int x, int y, int w, int h, MenuStruct *menu, char *path, int flag) {
        SDL_Texture *tex;
        SDL_Rect dest;

        tex = loadTexture(path);

        dest = (SDL_Rect){
                x,
                y,
                w,
                h
        };


        menu[flag].texture   = tex;
        menu[flag].size      = dest;
        menu[flag].isVisible = 1;
}

int isHovering(MenuStruct item, int x, int y) {
        int cnt = 0;

        if (x > item.size.x && x < item.size.x + item.size.w) {
                cnt++;
        }

        if (y > item.size.y && y < item.size.y + item.size.h) {
                cnt++;
        }

        return (cnt == 2) ? 1 : 0;
}

void renderBlackscreenMenu(MenuStruct menu[], int totalElements) {
        int i = 0;

        for (i = 0; i < totalElements; i++) {
                if (menu[i].isVisible) {
                        SDL_RenderCopy(_renderer, menu[i].texture, NULL, &menu[i].size);
                }
        }
        renderBlackscreen();
        SDL_RenderPresent(_renderer);
}

void renderMenu(MenuStruct menu[], int totalElements) {
        int i;

        SDL_RenderClear(_renderer);

        if (!(_fadeIn) && !(_fadeOut)) {
                for (i = 0; i < totalElements; i++) {
                        if (menu[i].isVisible) {
                                SDL_RenderCopy(_renderer, menu[i].texture, NULL, &menu[i].size);
                        }
                }

                SDL_RenderPresent(_renderer);
        }else{
                while ((_fadeIn) || (_fadeOut)) {
                        renderBlackscreenMenu(menu, totalElements);
                }
        }
}
