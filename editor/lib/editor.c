#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../../headers/utility.h"

int _ENV = ENV_DEV;

char *input(char *dataType){
        char *str = NULL;
        char *buffer = NULL;

        buffer = smalloc(sizeof(char), 151);
        str = smalloc(sizeof(char), 151);

        if (strcmp(dataType, "%d") != 0 && strcmp(dataType, "%s") != 0) {
                return NULL;
        }

      //  do {
                fgets(str, 150, stdin);
      //  } while (sscanf(buffer, dataType, str) == -1);

        str[strlen(str)] = 0x00;

        free(buffer);

        return str;
}

char *createDisplayCmd(char *str){
        int len = strlen(str);
        char *buf = smalloc(sizeof(char), len+5);

        strcpy(buf, "(");
        strcat(buf, str);
        strcat(buf, ")> ");

        return buf;
}

void printWelcome() {
        printf("ESGIUnfair configuration editor v1.0\n");
        printf("'exit' ou 'quit' : quitter le shell d'édition\n");
        printf("'list' ou 'show' : afficher les différentes options disponibles\n");
        printf("'use <mode>' : changer de mode.\n");
        printf("\n");
        printf("Mode disponibles : edit, set (alias), get, fetch (alias)\n");
        printf("\n");
}

void printHelp() {
        printf("Options :");
        printf("   Edit : Edit the specified config file. You need to specify the file and the value you want to get overwritten");
        printf("      Example : uce edit boss attacks 0 cooldown");
        printf("   > This command will update the 'cooldown' key of the 'boss' 'attacks' file of the item '0'");
        printf("\n");
        printf("   List : List all the available options");

}

void printAttacks() {
  printf("└── Attacks :\n");
  printf("    ├── FunctionPointerType (int)\n");
  printf("    ├── Cooldown (int)\n");
  printf("    ├── Duration (int)\n");
  printf("    ├── ImageCount (int)\n");
  printf("    ├── FrameInterval (int)\n");
  printf("    ├── Strength (int)\n");
  printf("    ├── Type (int)\n");
  printf("    ├── Effect (int)\n");
  printf("    ├── SpritePath (str)\n");
  printf("    ├── InitX (int)\n");
  printf("    ├── InitY (int)\n");
  printf("    ├── FrameWidth (int)\n");
  printf("    ├── FrameHeight (int)\n");
  printf("    ├── SpriteOffsetX (int)\n");
  printf("    └── SpriteOffsetY (int)\n");
}

void printSounds() {
  printf("└── Sounds\n");
  printf("       ├── SoundID (int)\n");
  printf("       └── FilePath (str)\n");
}

void printSprites(){
  printf("└── Sprites\n");
  printf("      ├── FrameQuantity (int)\n");
  printf("      ├── Width (int)\n");
  printf("      ├── Height (int)\n");
  printf("      └── FilePath (str)\n");

}

void printLifebar() {
  printf("└── Lifebar :\n");
  printf("       ├── Width (int)\n");
  printf("       ├── Height (int)\n");
  printf("       └── FilePath (str)\n");
}

void printBackground() {
  printf("└── Background :\n");
  printf("       └── FilePath (str)\n");
}

void printPlateforms() {
  printf("└── Plateforms :\n");
  printf("       ├── Quantity (int)\n");
  printf("       ├── FilePath (str)\n");
  printf("       ├── Width (int)\n");
  printf("       ├── Height (int)\n");
  printf("       ├── PosX (int)\n");
  printf("       └── PosY (int)\n");
}

void printBossText() {
  printf("└── Boss :\n");
  printf("    └── Text (str)\n");
}

void printCharacterText() {
  printf("└── Character :\n");
  printf("   └── Text (str)\n");
}
void printKeymap() {
  printf(" (Use 'mouse_left/middle/right' to use mouse click)\n");
  printf("└── Up (str)\n");
  printf("└── Down (str)\n");
  printf("└── Left (str)\n");
  printf("└── Right (str)\n");
  printf("└── DashLeft (str)\n");
  printf("└── DashRight (str)\n");
  printf("└── Shoot (str)\n");
  printf("└── Special (str)\n");
  printf("└── Parry (str)\n");
  printf("└── Menu (str)\n");
}

void printList() {
        printf("Boss\n");
        printAttacks();
        printf(" \n");
        printSounds();
        printf(" \n");
        printSprites();
        printf(" \n");
        printf(" \n");
        printf(" Character\n");
        printAttacks();
        printf(" \n");
        printSounds();
        printf(" \n");
        printSprites();
        printf(" \n");
        printf(" \n");
        printf(" HUD\n");
        printLifebar();
        printf(" \n");
        printf(" \n");
        printf(" Scenery\n");
        printBackground();
        printf(" \n");
        printPlateforms();
        printf(" \n");
        printf(" Text\n");
        printBossText();
        printf(" \n");
        printCharacterText();
        printf(" \n");
        printf(" Keymap\n");
        printKeymap();
}
