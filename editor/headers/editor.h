#ifndef EDITOR_H_INCLUDED
#define EDITOR_H_INCLUDED

#define ERR_INVALOPT "Invalid option. Use \'help\' to get the avalaible options\n"
#define ERR_OPNFILE "Can't open the file\n"
#define ERR_NOFILE "File not found\n"
#define ERR_ARGS_LESS "Too much arguments (%d options needed, %d provided). Use \'help\' to get the avalaible options.\n"
#define ERR_ARGS_MORE "Not enough arguments (%d options needed, %d provided). Use \'help\' to get the avalaible options.\n"

enum Mode{
  EDITOR_EDIT,
  EDITOR_SHOW
};

char *input(char *dataType);
void printWelcome();
void printHelp();
void printList();
char *createDisplayCmd(char *str);
void printAttacks();
void printSounds();
void printSprites();
void printLifebar();
void printBackground();
void printPlateforms();
void printBossText();
void printCharacterText();
void printKeymap();

#endif
