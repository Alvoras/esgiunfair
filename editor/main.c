#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../headers/utility.h"
#include "headers/editor.h"

int main(int argc, char const **argv) {
        int i = 0;
        char *prefix = NULL;
        int loop = 1;
        char *cmd = NULL;
        char *displayCmd = NULL;
        char **cmdList = NULL;

        char displayLevel[10] = {0x00};

        int isKeymapSelected = 0;

        int chosenLevel = -1;

        int crtMode = EDITOR_EDIT;
        int cmdLen = 0;

        char **crtPath = NULL;
        char *crtOption = NULL;

        char displayOptions[10] = {0x00};
        char displayPath[20] = {0x00};

        char *delimiter = " \n";

        crtOption = smalloc(sizeof(char), 20);
        crtOption[0] = 0x00;

        crtPath = smalloc(sizeof(char), 5);

        for (i = 0; i < 5; i++) {
                crtPath[i] = smalloc(sizeof(char), 15);
                crtPath[i] = 0x00;
        }

        crtPath[0] = 0x00;

        prefix = smalloc(sizeof(char), 5);
        strcpy(prefix, "edit");

        switch (argc) {
        case 1:
                printWelcome();
                while ( (loop) ) {
                        displayCmd = createDisplayCmd(prefix);
                        printf("%s", displayCmd);
                        cmd = input("%s");
                        cmdList = splitStr(cmd, delimiter, &cmdLen);

                        if (cmdLen == 1) {
                                if (strcmp(cmdList[0], "exit") == 0 ||strcmp(cmdList[0], "quit") == 0) {
                                        loop = 0;
                                }else if (strcmp(cmdList[0], "list") == 0 ||strcmp(cmdList[0], "show") == 0) {
                                        printList();
                                }else if (strcmp(cmdList[0], "selected") == 0 ||strcmp(cmdList[0], "show") == 0) {
                                        if (chosenLevel == -1) {
                                          strcpy(displayLevel, "needed");
                                        }else{
                                          sprintf(displayLevel, "%d", chosenLevel);
                                        }

                                        if (crtOption[0] == 0x00) {
                                          strcpy(displayOptions, "needed");
                                        }else{
                                          strcpy(displayOptions, crtOption);
                                        }

                                        if (crtPath[0] == 0x00) {
                                          strcpy(displayPath, "needed");
                                        }else{
                                          strcpy(displayPath, joinStr(crtPath, "/"));
                                        }

                                        printf("Level : %s\n", displayLevel);
                                        printf("Path : %s\n", displayPath);
                                        printf("Option : %s\n", displayOptions);
                                }
                                continue;
                        }

                        if (cmdLen == 2) {
                                if (strcmp(cmdList[0], "use") == 0) {
                                        if (strcmp(cmdList[1], "edit") == 0 ||strcmp(cmdList[1], "set") == 0) {
                                                crtMode = EDITOR_EDIT;
                                                strcpy(prefix, "edit");
                                        }else if (strcmp(cmdList[1], "get") == 0 ||strcmp(cmdList[1], "fetch") == 0) {
                                                crtMode = EDITOR_SHOW;
                                                strcpy(prefix, "show");
                                        }
                                } else if (strcmp(cmdList[0], "select") == 0 && strcmp(cmdList[0], "keymap")) {
                                  isKeymapSelected = 1;
                                  printKeymap();
                                }
                                continue;
                        }

                        if (cmdLen == 3) {
                                if (strcmp(cmdList[0], "select") == 0) {
                                        if (strcmp(cmdList[1], "level") == 0) {
                                                chosenLevel = atoi(cmdList[2]);
                                                if (chosenLevel >= 0 && chosenLevel > 6) {
                                                        printf(ERR_INVALOPT);
                                                }
                                        }else if (strcmp(cmdList[1], "boss") == 0||strcmp(cmdList[2], "character") == 0) {
                                                if (strcmp(cmdList[2], "special") == 0||strcmp(cmdList[2], "sprites") == 0||strcmp(cmdList[2], "sounds") == 0) {
                                                        //strcpy(crtPath[1], cmdList[1]);
                                                        //strcpy(crtPath[2], cmdList[2]);
                                                        crtPath[1] = strdup(cmdList[1]);
                                                        crtPath[2] = strdup(cmdList[2]);
                                                        printf("Selected %s/%s\n", cmdList[1], cmdList[2]);
                                                        printAttacks();
                                                        printSounds();
                                                        printSprites();
                                                }else{
                                                        printf(ERR_INVALOPT);
                                                }
                                        }else if (strcmp(cmdList[1], "hud") == 0) {
                                                if (strcmp(cmdList[2], "lifebar") == 0) {
                                                        strcpy(crtPath[1], cmdList[1]);
                                                        strcpy(crtPath[2], cmdList[2]);
                                                        printf("Selected %s/%s", cmdList[1], cmdList[2]);
                                                        printLifebar();
                                                }else{
                                                        printf(ERR_INVALOPT);
                                                }
                                        }else if (strcmp(cmdList[1], "scenery") == 0) {
                                                if (strcmp(cmdList[2], "background") == 0||strcmp(cmdList[2], "plateform") == 0) {
                                                        strcpy(crtPath[1], cmdList[1]);
                                                        strcpy(crtPath[2], cmdList[2]);
                                                        printf("Selected %s/%s", cmdList[1], cmdList[2]);
                                                        printBackground();
                                                        printPlateforms();
                                                }else{
                                                        printf(ERR_INVALOPT);
                                                }
                                        }else if (strcmp(cmdList[1], "text") == 0) {
                                                if (strcmp(cmdList[2], "character") == 0||strcmp(cmdList[2], "boss") == 0) {
                                                        strcpy(crtPath[1], cmdList[1]);
                                                        strcpy(crtPath[2], cmdList[2]);
                                                        printf("Selected %s/%s", cmdList[1], cmdList[2]);
                                                        printBossText();
                                                        printCharacterText();
                                                }else{
                                                        printf(ERR_INVALOPT);
                                                }
                                        }else{
                                                printf(ERR_INVALOPT);
                                        }
                                }

                                if (cmdLen > 3) {
                                        isKeymapSelected = 1;
                                }
                        }

                        if (cmdLen > 4 && cmdLen  < 7) {

                                continue;
                        }
                        cmdList = NULL;
                }
                break;

        case 2:
                if (strcmp(argv[1], "help") == 0) {
                        printHelp();
                }else if(strcmp(argv[1], "list") == 0) {
                        printList();
                }
                break;

        default:
                printf(ERR_ARGS_LESS, 3, argc);
                break;
        }

        free(cmd);
        free(displayCmd);
        free(prefix);

        return 0;
}
