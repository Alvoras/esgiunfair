CC=gcc
CFLAGS= -Wall
EXEC=a
LINKER=-lpthread -lm -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer
OBJ=main.c lib/sdl_tools.c lib/linked_list.c lib/boss.c lib/character.c lib/game.c lib/ability.c lib/utility.c lib/load.c lib/client.c lib/server.c lib/init.c lib/menu.c

all:	$(OBJ)
	clear && $(CC) $(CFLAGS) $(OBJ) $(LINKER) -o $(EXEC)

test:	$(OBJ)
	clear && $(CC) $(CFLAGS) $(OBJ) $(LINKER) -o $(EXEC) && ./${EXEC}
