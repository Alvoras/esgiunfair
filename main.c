#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include "headers/config.h"
#include "headers/utility.h"
#include "headers/menu.h"
#include "headers/init.h"
#include "headers/load.h"
#include "headers/sdl_tools.h"
#include "headers/linked_list.h"
#include "headers/game.h"
#include "headers/socket.h"
#include "headers/client.h"
#include "headers/server.h"

// Switch it to ENV_PROD in order to disable console logging
int _ENV = ENV_DEV;

#ifdef __linux__
    extern int userInterruption;
#endif

SDL_Window *  _mainWindow = NULL;
SDL_Renderer *_renderer   = NULL;

int _blackscreenOpacity = MAX_OPACITY;

int _blackscreenSec = 0;
long _blackscreenMillisec = 0;

int _fadeIn = 0;
int _fadeOut = 0;

SDL_Texture *_textures[TEXTURES_TOTAL];
TTF_Font *   _menuFont;
TTF_Font *   _dialogFont;
TTF_Font *   _announcementFont;

Mix_Chunk *_sounds[SOUND_TOTAL];
int *      _laserChannel;
Mix_Music *_music;
int        _isMusicPlaying = 0;

int _gameParams[CONFIG_TOTAL];

int       tCloseClient = 0;
int       tCloseServer = 0;
SDL_Event tEvent;
char *    tSerialDataToServer   = NULL;
char *    tSerialDataFromServer = NULL;
int       tSendEvent            = 0;
int       tNewGameEvent         = 0;
int       tNewServerEvent       = 0;
char      tGameInfo             = -1;
int       _multiplayer          = 0;
int       _playingBoss          = 0;
int       _playingChar          = 0;
int       _timerStartFight      = 0;

ClientSocketInfo tClientSocketInfo;
pthread_t        clientSocketThread;
pthread_t        serverThread;

Keymap _keymap;

// TODO
// Add gamepad controls
Keymap _keymapControler;

extern int _loops[LOOP_TOTAL];

LifeBar _bossLifeBar;

LifeBar _charLifeBar;

LifeBar _lifeBarHolder;

Platforms *_platforms;

MainCharacter _mainChar;

Boss _boss;

SDL_Rect _shadowRect = {
    0,            // x
    0,            // y
    CHAR_W * 0.8, // w
    15            // h
};

int main(int argc, char **argv) {
    // Main loop quit flag
    unsigned long frameStart   = 0;
    int           loopDuration = 0;

    #ifdef __linux__
        struct sigaction psa;
        psa.sa_handler = signalHandler;

        sigaction(SIGINT, &psa, NULL);
    #endif

    // Server
    FILE *servSharedMem = NULL;

    SDL_Event eventBuffer;

    initGameParams();
    loadGameParams();

    // Start SDL
    if (!(init(_gameParams[CONFIG_FULLSCREEN]))) {
        logger("Init failed\n", LOGGER_FAIL);
        exit(EXIT_FAILURE);
    }else{
        logger("Initialization successful\n", LOGGER_OK);
        logger("Game started\n", LOGGER_INFO);

        _loops[MAIN_MENU_LOOP] = 0;

        while (!(_loops[PROGRAM_LOOP])) {
            playMusic();
            _timerStartFight = 0;

            setBlackscreenTimer(0, 500);
            toggleBlackscreenFade(FADE_OUT);
            mainMenu();
            applySoundParams();

            // After play option fade in
            //setBlackscreenTimer(1, 0);
            //toggleBlackscreenFade(FADE_OUT);
            while ((_loops[GAME_LOOP])) {
                frameStart = SDL_GetTicks();

                if ((tNewServerEvent)) {
                    memcpy(&eventBuffer.type, tSerialDataFromServer, sizeof(int));
                    memcpy(&eventBuffer.key.keysym.sym, tSerialDataFromServer + sizeof(int), sizeof(int));
                    memcpy(&eventBuffer.button.button, tSerialDataFromServer + (sizeof(int) * 2), sizeof(char));

                    printf("Received %x %x %x\n", eventBuffer.type, eventBuffer.key.keysym.sym, eventBuffer.button.button);

                    handleEvents(eventBuffer);
                    tNewServerEvent = 0;
                }
                // Handle events on queue
                handleEventsLoop();

                // Terminate the loop if something in handleEvents need to end the loop
                if (!(_loops[GAME_LOOP])) {
                    break;
                }

                // Update game state
                if (update() == 1) {
                    break;
                }

                // Render each element of the frame
                render();

                // Get the loop duration
                loopDuration = SDL_GetTicks() - frameStart;

                // If the loop duration is lower than our minimum frame delay
                // then delay it for what is left
                if (FRAME_DELAY > loopDuration && !(_fadeIn || _fadeOut)) {
                    SDL_Delay(FRAME_DELAY - loopDuration);
                }
                _loops[MAIN_MENU_LOOP] = 0;
            }
        } // END OF while(!quit)
    }     // END OF if init == OK

    if (servSharedMem != NULL) {
        fclose(servSharedMem);
    }

    // Quit SDL
    closeSDL();

    //kill(serverPID, SIGKILL);

    return 0;
}
