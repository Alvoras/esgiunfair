#!/bin/bash

echo -e 'Installing dependencies...'
echo -e '[\033[33;1m...\033[0m] Installing libsdl2-dev...'
sudo apt-get -qq install -y libsdl2-dev &> /dev/null && tput cuu1 && tput el && echo -e '[\033[32;1mOK\033[0m] Installing libsdl2-dev...' || (tput cuu1 && tput cuu1 && tput el && echo -e '[\033[31;1mFAIL\033[0m] Installing libsdl2-dev...' && tput el)
echo -e '[\033[33;1m...\033[0m] Installing sdl2-image...'
sudo apt-get -qq install -y sdl2-image* &> /dev/null && tput cuu1 && tput el && echo -e '[\033[32;1mOK\033[0m] Installing sdl2-image...' || (tput cuu1 && tput cuu1 && tput el && echo -e '[\033[31;1mFAIL\033[0m] Installing sdl2-image...' && tput el)
echo -e '[\033[33;1m...\033[0m] Installing sdl2-ttf...'
sudo apt-get -qq install -y sdl2-ttf* &> /dev/null && tput cuu1 && tput el && echo -e '[\033[32;1mOK\033[0m] Installing sdl2-ttf...' || (tput cuu1 && tput cuu1 && tput el && echo -e '[\033[31;1mFAIL\033[0m] Installing sdl2-ttf...' && tput el)
echo -e '[\033[33;1m...\033[0m] Installing sdl2-mixer...'
sudo apt-get -qq install -y sdl2-mixer* &> /dev/null && tput cuu1 && tput el && echo -e '[\033[32;1mOK\033[0m] Installing sdl2-mixer...' || (tput cuu1 && tput cuu1 && tput el && echo -e '[\033[31;1mFAIL\033[0m] Installing sdl2-mixer...' && tput el)

echo -e 'Done'
